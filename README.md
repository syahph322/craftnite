# craftnite

## Description
This repository give an example of server implementation for craftnite.io.

## Authors and acknowledgment
Many Thanks to 𝓍𝒾𝓅 who made the original game. He proved this kind of a game is runable in a browser.

[I](https://www.larmoire.info)'m an embedded system software engineer, playing with javascript since a long time.

## License
MIT
