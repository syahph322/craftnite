// Copyright 2022 Thierry LARMOIRE
// SPDX short identifier: MIT
#ifndef __world_h__
#define __world_h__

#include "chunk.h"
#include "player.h"
#include "nothread/list.h"

struct World
{
	int init();
	int exit();
	Chunk *chunkAt(V3 pos);
	Chunk *chunkAtOrCreate(V3 pos);
	
	static const size_t sideLog2=8;
	static const size_t side=1<<sideLog2;
	Chunk ****chunks;
	List<Player> players;
	int remainingTime;
	int load(const char *file,V3 off,V3 size);
	void queue(Player *player);
	void eachSeconds();
	
	inline static int v_i(V3 v)
	{
		return 0
			|(v.x<<0*sideLog2)
			|(v.y<<1*sideLog2)
			|(v.z<<2*sideLog2)
		;
	}
	inline static V3 i_v(int i)
	{
		return V3(
			(i>>0*sideLog2)&(side-1),
			(i>>1*sideLog2)&(side-1),
			(i>>2*sideLog2)&(side-1)
		);
	}
};

#endif /* __world_h__ */
