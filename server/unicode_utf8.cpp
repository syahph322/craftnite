#include "unicode_utf8.h"

int unicode_to_utf8(const U16 *uni_ptr, size_t uni_len,char *dst, size_t len)
{
	int utf_len=0;
	U32 u=0;
	while(uni_len)
	{
		U16 c=*uni_ptr;
		uni_ptr++;
		uni_len--;
		if((c&0xf800)==0xd800)
		{
			if(!(c&0x0400))
			{
				u =((c&0x03f)<<10)|((((c&0x3c0)>>6)+1)<<16);
				continue;
			}
			else
			{
				u|=c&0x03ff;
			}
		}
		else
			u=c;
		if(u < 0x80)
		{
			if(len) { len--; *dst++=(char)u; }
			utf_len++;
		}
		else
		if(u < 0x0800)
		{
			if(len) { len--; *dst++= (char) (((u >> 6) & 0x1f) | 0xc0); }
			if(len) { len--; *dst++= (char) (((u >> 0) & 0x3f) | 0x80); }
			utf_len+=2;
		}
		else
		if(u < 0x10000)
		{
			if(len) { len--; *dst++= (char) (((u >>12) & 0x0f) | 0xe0); }
			if(len) { len--; *dst++= (char) (((u >> 6) & 0x3f) | 0x80); }
			if(len) { len--; *dst++= (char) (((u >> 0) & 0x3f) | 0x80); }
			utf_len+=3;
		}
		else
		{
			if(len) { len--; *dst++= (char) (((u >>18) & 0x07) | 0xf0); }
			if(len) { len--; *dst++= (char) (((u >>12) & 0x3f) | 0x80); }
			if(len) { len--; *dst++= (char) (((u >> 6) & 0x3f) | 0x80); }
			if(len) { len--; *dst++= (char) (((u >> 0) & 0x3f) | 0x80); }
			utf_len+=4;
		}
	}
	return utf_len;
}

int utf8_to_unicode(const char *utf_ptr, size_t utf_len,U16 *dst, size_t len)
{
	size_t uni_len=0;
	U32 u=0;
	while(utf_len)
	{
		U8 b=*utf_ptr;
		utf_ptr++;
		utf_len--;
		u=(u<<8)|b;
		if(0);
		else if((u&0x00000080)==0x00000000)
			u=                                                          (u&0x7f);
		else if((u&0x0000e0c0)==0x0000c080)
			u=                                        ((u&0x1f00)>>2) | (u&0x3f);
		else if((u&0x00f0c0c0)==0x00e08080)
			u=                    ((u&0x0f0000)>>4) | ((u&0x3f00)>>2) | (u&0x3f);
		else if((u&0xf4c0c0c0)==0xf0808080)
			u=((u&0x070000)>>6) | ((u&0x3f0000)>>4) | ((u&0x3f00)>>2) | (u&0x3f);
		else
			continue;
		
		if(u<0x10000)
		{
			if(len) { len--; *dst++=u; }
			uni_len++;
		}
		else
		{
			if(len) { len--; *dst++=0xd800 | ((((u&0x1f0000)>>16)-1)<<6)| ((u&0xfc00)>>10); }
			if(len) { len--; *dst++=0xdc00 | (u&0x3ff); }
			uni_len+=2;
		}
		u=0;
	}
	return uni_len;
}
