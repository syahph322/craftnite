// Copyright 2022 Thierry LARMOIRE
// SPDX short identifier: MIT
#ifndef __chunk_h__
#define __chunk_h__

#ifndef __vector_h__
#include "vector.h"
#endif

#include "nothread/type.h"

struct Player;

struct Chunk
{
	static const size_t sideLog2=5;
	static const size_t side=1<<5;
	static const size_t len=side*side*side;
	V3 pos;
	U16 *data;
	U8 *hard;
	
	Chunk(V3 pos);
	~Chunk();
	void borderStone();
	bool empty();
	void send(Player *player, int kind);
	inline static int v_i(V3 v)
	{
		return 0
			|(v.x<<0*sideLog2)
			|(v.y<<1*sideLog2)
			|(v.z<<2*sideLog2)
		;
	}
	inline static V3 i_v(int i)
	{
		return V3(
			(i>>0*sideLog2)&(side-1),
			(i>>1*sideLog2)&(side-1),
			(i>>2*sideLog2)&(side-1)
		);
	}
};

#endif /* __chunk_h__ */
