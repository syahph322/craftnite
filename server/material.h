// Copyright 2022 Thierry LARMOIRE
// SPDX short identifier: MIT
#ifndef __material_h__
#define __material_h__

#include "nothread/type.h"

struct Material
{
	U16 id;
	U16 hard;
	U16 next;
};

struct Materials
{
	static const size_t lenLog2=16;
	static const size_t len=1<<lenLog2;
	Material *materials;
	int init();
	int exit();
	inline Material *operator[](int i)
	{
		return &materials[i&(len-1)];
	}
};



#endif /* __material_h__ */
