// Copyright 2022 Thierry LARMOIRE
// SPDX short identifier: MIT
#ifndef __player_h__
#define __player_h__

#ifndef __vector_h__
#include "vector.h"
#endif

#include "http/webSocket.h"
struct Server;
struct World;

#define BUFFER_ALLOCED(name,size) \
	U8 name ## _[size]; \
	::Buffer name(name ## _,sizeof(name ## _)); \
/**/

#define CMDS \
	CMD_S_C( 0,PlayerId) \
	CMD_C_S( 1,NewPlayer) \
	CMD_S_C( 2,NewPlayer) \
	CMD_C_S( 3,State) \
	CMD_S_C( 4,State) \
	CMD_C_S( 5,PlayerItem) \
	CMD_S_C( 6,PlayerItem) \
	DIS_C_S( 7,A179) \
	DIS_S_C( 8,Death) \
	CMD_C_S(13,Start) \
	CMD_S_C(14,Config1) \
	CMD_S_C(15,Items) \
	CMD_C_S(16,Touch) \
	DIS_S_C(17,OtherHealth) \
	CMD_S_C(18,Death2) \
	CMD_S_C(19,YouDied) \
	CMD_C_S(20,Respawn) \
	CMD_S_C(21,Respawn) \
	DIS_S_C(22,Alive) \
	DIS_S_C(23,OtherQuit) \
	CMD_C_S(25,Shot) \
	CMD_S_C(26,Shot) \
	CMD_C_S(27,Chat) \
	CMD_S_C(28,Chat) \
	DIS_C_S(29,A203) \
	DIS_C_S(36,A616) \
	DIS_C_S(37,A617) \
	DIS_C_S(38,A210) \
	DIS_C_S(49,A221) \
	DIS_S_C(50,Disconnect) \
	CMD_C_S(53,TriggerOn) \
	DIS_S_C(54,ActingSet) \
	CMD_C_S(55,TriggerOff) \
	DIS_S_C(56,ActingClr) \
	CMD_S_C(64,ChunkWorker) \
	CMD_S_C(65,FirstChunkWorker) \
	CMD_S_C(66,Chunk) \
	DIS_C_S(67,A232) \
	DIS_S_C(68,HideChunkWorker) \
	CMD_S_C(69,ResetChunks) \
	CMD_C_S(70,Change) \
	DIS_S_C(72,Health) \
	DIS_S_C(73,EndMatch) \
	DIS_S_C(74,MatchStartingSoon) \
	DIS_S_C(75,MatchStartWaiting) \
	CMD_S_C(76,MatchStarted) \
	DIS_S_C(77,DisconnectingSoon) \
	DIS_S_C(78,WinPos) \
	CMD_S_C(79,OceanConfig) \
	CMD_S_C(80,Config2) \
	CMD_S_C(81,Creative) \
	CMD_C_S(82,Alter) \
	CMD_C_S(84,ChunkBuffered) \
	DIS_S_C(85,PlayerScore) \
	CMD_S_C(86,MatchRemainingTime) \
	/**/

#define DIS_C_S(i,n) CMD_C_S(i,n)
#define DIS_S_C(i,n) CMD_S_C(i,n)

enum
{
	#define CMD_C_S(i,n) recv ## n=i,
	#define CMD_S_C(i,n) send ## n=i,
	CMDS
	#undef CMD_C_S
	#undef CMD_S_C
	cmdMax
};

typedef float F32;

struct Player: public Node<Player>, WebSocket
{
	struct Buffer : public ::Buffer
	{
		Buffer()
		{
		}
		Buffer(::Buffer b)
		{
			*(::Buffer*)this=b;
		}
		#define GET_PUT(type,type2) \
			Buffer& get ## type (type *); Buffer& put ## type (type ); \
			Buffer& get ## type (type2 *); Buffer& put ## type (type2 ); \
		/**/
		GET_PUT(U8,int)
		GET_PUT(U16,int)
		GET_PUT(U32,int)
		GET_PUT(F32,double)
		#undef GET_PUT
		
		Buffer& putUnicode(const char *s);
	};
	Player();
	~Player();
	int init(Server *server, HttpServer::Client *client, int id);
	int exit();
	void graded(bool up);
	void ondata(::Buffer buffer);
	
	#define CMD_C_S(i,n) static void on ## n ## _(Player *that, Buffer *data); void on ## n(Buffer *data);
	#define CMD_S_C(i,n)
	CMDS
	#undef CMD_C_S
	#undef CMD_S_C
	
	typedef void Cmd(Player *that, Buffer *data) ;
	static Cmd * cmds[cmdMax];
	
	Server *server;
	int id;
	BufferMallocedCString name;
	BufferMallocedCString item;
	int skin;
	int life;
	int score;
	
	const size_t resolution=32;
	const size_t js_resolution=5;
	V3 position;
	float time,rotX,rotY;
	U8 running;
	
	void sendChunk(int kind, V3 v);
	void queue(V3 v);
	void queueAll();
	struct Fifo
	{
		static const size_t sizeLog2=10;
		static const size_t size=1<<sizeLog2;
		size_t i,o;
		int fifo[size];
		inline Fifo()
		{
			i=o=0;
		}
		inline bool empty()
		{
			return i==o;
		}
		inline bool full()
		{
			return i-o==size;
		}
		inline void push(int v)
		{
			fifo[i++&(size-1)]=v;
		}
		inline int pull()
		{
			return fifo[o++&(size-1)];
		}
	}fifo;
	int modifying;
	void modify();
	struct Sight
	{
		V3 pos;
	}sight;
	void queueAround();
	void respawn();
};

#endif /* __player_h__ */
