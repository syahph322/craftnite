// Copyright 2022 Thierry LARMOIRE
// SPDX short identifier: MIT
#ifndef __vector_h__
#define __vector_h__

struct V3
{
	union
	{
		int axes[3];
		struct
		{
			int i,j,k;
		};
		struct
		{
			int x,y,z;
		};
	};
	inline V3()
	{
		x=y=z=0;
	}
	inline V3(int x, int y, int z)
	{
		this->x=x;
		this->y=y;
		this->z=z;
	}
	int operator[](int i)
	{
		return axes[i];
	}
	inline V3 operator*(int v) const
	{
		return V3(x*v,y*v,z*v);
	}
	inline V3 operator/(int v) const
	{
		return V3(x/v,y/v,z/v);
	}
	inline V3 operator+(V3 v) const
	{
		return V3(x+v.x,y+v.y,z+v.z);
	}
	inline V3 operator-(V3 v) const
	{
		return V3(x-v.x,y-v.y,z-v.z);
	}
	inline V3 operator*(V3 v) const
	{
		return V3(x*v.x,y*v.y,z*v.z);
	}
	inline V3& noless(V3 v)
	{
		if(x<v.x) x=v.x;
		if(y<v.y) y=v.y;
		if(z<v.z) z=v.z;
		return *this;
	}
	inline V3& nomore(V3 v)
	{
		if(x>v.x) x=v.x;
		if(y>v.y) y=v.y;
		if(z>v.z) z=v.z;
		return *this;
	}
	inline V3 crop(V3 min,V3 max)
	{
		return this->noless(min).nomore(max);
	}
	struct Border;
};


struct V3::Border
{
	Border(V3 size);
	bool get(V3 *p);
	V3 size;
	struct Face
	{
		V3 o1,ol,v1,vl;
	};
	const Face *f,*e;
	V3 o,v,d;
};

#define V3_FMT(f) "(" f "," f "," f ")"
#define V3_ARG(v)    v.x , v.y , v.z

#endif /* __vector_h__ */
