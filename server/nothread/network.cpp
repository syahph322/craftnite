// Copyright 2022 Thierry LARMOIRE
// SPDX short identifier: MIT
#include "network.h"

#include <arpa/inet.h>
#include <netdb.h>
#include <unistd.h>

unsigned int resolv(const char* host)
{
	unsigned int address;
	
	address = inet_addr(host);
	if(address != INADDR_NONE)
		return address;
	
	{
		hostent he_mem[1],*he;
		char buf[1024];
		int err;
		he=NULL;
		gethostbyname_r(host,he_mem,buf,sizeof(buf),&he,&err);
		if(he == NULL)
			return INADDR_NONE;
		address = *((unsigned int*)he->h_addr_list[0]);
	}
	
	return address;
}

