// Copyright 2022 Thierry LARMOIRE
// SPDX short identifier: MIT
#include "nothread.h"
#include "debug.h"
#include <stdio.h>
#include <stdarg.h>
#include <unistd.h>
#include <sys/epoll.h>
#include <sys/signalfd.h>
#include <sys/timerfd.h>
#include <sys/errno.h>
#include <stdlib.h>
#include <dlfcn.h>

#if 0 /* gdb */

set print static-members off

define dump_timers
 set $a=timers.head.next
 while $n=$a->next
  p $a
  p ((Nothread::Timer*)$a)->handler
  p ((Nothread::Timer*)$a)->timeout.t-now.t
  set $a=$n
 end
end

define dump_listeners
 set $a=listeners.head.next
 while $n=$a->next
  p ((Nothread::Listener*)$a)->handler
  p ((Nothread::Listener*)$a)->fd
  set $a=$n
 end
end
#endif

struct Symbol
{
	void *address;
	char buf[20];
	Dl_info info;
	Symbol(void* address);
	const char *name;
};

Symbol::Symbol(void* address)
{
	memset(&info,0,sizeof(info));
	dladdr(address,&info);
	if(info.dli_sname)
	{
		name=info.dli_sname;
	}
	else
	{
		snprintf(buf,sizeof(buf),"@%p",address);
		name=buf;
	}
}

int Nothread::init(int options)
{
	ldebug("init\n");
	
	debugLevel=0;
	{
		char *v=getenv("nothread_debug");
		if(v)
		{
			debugLevel=atoi(v);
			debug("debugLevel %d\n",debugLevel);
		}
	}
	
	efd=epoll_create1(EPOLL_CLOEXEC);
	if(-1==efd)
		goto err;
	
	if(!(options&dontQuitOnSignal))
	{
		sigset_t set;
		sigemptyset(&set);
		sigaddset(&set,SIGQUIT);
		sigaddset(&set,SIGTERM);
		sigaddset(&set,SIGINT);
		sigprocmask(SIG_BLOCK, &set, NULL);
		sfd=signalfd(-1, &set, SFD_NONBLOCK|SFD_CLOEXEC);
		if(-1==sfd)
			goto err;
		
		listenerAttach(sfd,NOTHREAD_READABLE,onSignal_,this);
	}
	
	tfd=timerfd_create(CLOCK_MONOTONIC,TFD_NONBLOCK|TFD_CLOEXEC);
	if(-1==tfd)
		goto err;
	
	listenerAttach(tfd,NOTHREAD_READABLE,onTimer_,this);
	
	return 0;
err:
	exit();
	return -1;
}

int Nothread::exit()
{
	foreach(Listener,removed,removeds)
	{
		removed->remove();
		delete removed;
	}
	foreach(Timer,timer,timers)
	{
		timer->remove();
		delete timer;
	}
	foreach(Listener,listener,listeners)
	{
		listener->remove();
		delete listener;
	}
	if(tfd!=-1)
	{
		listenerDetach(tfd);
		close(tfd);
		tfd=-1;
	}
	if(sfd!=-1)
	{
		listenerDetach(sfd);
		close(sfd);
		sfd=-1;
	}
	if(efd!=-1)
	{
		close(efd);
		efd=-1;
		ldebug("exit\n");
	}
	return 0;
}

NOTHREAD_HANDLER_BODY(Nothread,onSignal)
{
	struct signalfd_siginfo info;
	ssize_t status=read(sfd, &info, sizeof(info));
	if(status!=sizeof(info))
		ldebug("!read signalfd_siginfo %zd!=%zd on %d %d\n",status,sizeof(info),sfd,errno);
	else
		ldebug("signal %d\n",info.ssi_signo);
	quit=1;
}

NOTHREAD_HANDLER_BODY(Nothread,onTimer)
{
	NtpMonotonic now;
	uint64_t count;
	int status=read(tfd, &count, sizeof(count));
	if(status<0)
		ldebug("out now is %.6lf %d\n", (double)now, status);
	Timers reorders;
	foreach(Timer, timer, timers)
	{
		if(timer->timeout>=now)
			continue;
		if(!timer->handler)
			continue;
		if(debugLevel&0x20)
		{
			Symbol symbol((void*)timer->handler);
			ldebug("invoke for timer %p %.6lf>%.6lf %s(%p)\n", timer, (double)now, (double)timer->timeout, symbol.name,timer->that);
		}
		Ntp t[2],d;
		t[0].setMonotonic();
		timer->handler(timer->that);
		t[1].setMonotonic();
		d=t[1]-t[0];
		if(d.integer()>1)
		{
			Symbol symbol((void*)timer->handler);
			ldebug("%s(%p) block %lf seconds\n", symbol.name, timer->that, (double)d);
		}
		if(!timer->delay.t)
		{
			timer->remove();
			delete timer;
		}
		else
		{
			timer->remove();
			timer->schedule(now);
			reorders.queue(timer);
		}
	}
	foreach(Timer, timer, reorders)
	{
		timers.insert(timer->remove());
	}
}

int Nothread::listenerAttach(int fd, int mask, void (*handler)(void *that, int mask), void*that)
{
	if(debugLevel&0x01)
		ldebug("%d\n",fd);
	Listener *listener=new Listener(fd, handler, that);
	struct epoll_event ev;
	ev.events = mask;
	ev.data.ptr = listener;
	if(-1==epoll_ctl(efd, EPOLL_CTL_ADD, fd, &ev))
	{
		ldebug("!EPOLL_CTL_ADD %d 0x%x %s\n",fd,mask,strerror(errno));
		delete listener;
		return -1;
	}
	listeners.queue(listener);
	return 0;
}

int Nothread::listenerDetach(int fd)
{
	Listener *listener=listeners.findByFd(fd);
	if(debugLevel&0x01)
		ldebug("%d %p\n",fd,listener);
	if(!listener)
		return -1;
	
	// don't delete now, 
	// perhaps we are itering after epoll_wait, 
	// and we try to access this listener in a next ev
	listener->handler=NULL;
	listener->remove();
	removeds.queue(listener);
	
	if(-1==epoll_ctl(efd, EPOLL_CTL_DEL, fd, NULL))
		return -1;
	
	return 0;
}

Nothread::TimerId Nothread::timerSet(int microseconds, void (*handler)(void *that), void*that)
{
	NtpMonotonic now;
	if(microseconds<0)
		microseconds=0;
	
	Timer *timer=new Timer(handler, that);
	timer->timeout.t=now.t+int64_t(microseconds)*NTP_1SEC/1000000;
	timer->align.t=0;
	timer->delay.t=0;
	timer->offset.t=0;
	timers.insert(timer);
	
	if(debugLevel&0x10)
	{
		Symbol symbol((void*)timer->handler);
		ldebug("timerSet %p %lf->%lf %s(%p)\n", (TimerId)timer, (double)now, (double)timer->timeout, symbol.name, timer->that);
	}
	return (TimerId)timer;
}

void Nothread::Timers::insert(Timer *timer)
{
	Node<Timer> *after=NULL;
	foreach(Timer, t, *this)
	{
		if(t->timeout>timer->timeout)
		{
			after=t;
			break;
		}
	}
	if(!after) after=tail;
	
	timer->insert(after->prev, after);
}

int     Nothread::timerCancel(Nothread::TimerId timerId)
{
	Timer *timer=(Timer *)timerId;
	if(timer->alone())
	{
		ldebug("alone!\n");
		return -1;
	}
	if(debugLevel&0x10)
		ldebug("%s %p\n", timer->delay.t?"periodicCancel":"timerCancel", timerId);
	timer->handler=NULL;
	return 0;
}

Nothread::TimerId Nothread::periodicSet(int ualign, int udelay,int uoffset,void (*handler)(void *that),void*that)
{
	NtpMonotonic now;
	
	Timer *timer=new Timer(handler,that);
	timer->align .set(ualign ,1000000);
	timer->delay .set(udelay ,1000000);
	timer->offset.set(uoffset,1000000);
	timer->schedule(now);
	timers.insert(timer);
	return (TimerId)timer;
}

int Nothread::periodicCancel(TimerId timerId)
{
	return timerCancel(timerId);
}

void Nothread::Timer::schedule(const NtpMonotonic &now)
{
	int64_t i,f;
	i=f=now.t;
	f%=align.t;
	i-=f;
	timeout.t=i+f-f%delay.t+delay.t+offset.t;
}

int Nothread::debug(const char *fmt, ...)
{
	size_t len;
	va_list list;
	
	va_start(list, fmt);
	len=_debugv(fmt, list);
	va_end(list);
	return len;
}

int Nothread::doEvent(void)
{
	epoll_event evs[16];
	sigset_t set;
	sigfillset(&set);
	int timeout=-1;
	
	NtpMonotonic now;
	//ldebug("doEvent in  now is %.6lf\n", (double)now);
	//foreach(Timer, timer, timers)
	//{
	//	ldebug("timer  %.6lf\n", (double)timer->timeout);
	//}
	Timer *first=(Timer*)(timers.head->next->next ? timers.head->next : NULL);
	if(first)
	{
		struct itimerspec itimerspec;
		itimerspec.it_interval.tv_sec=0;
		itimerspec.it_interval.tv_nsec=0;
		itimerspec.it_value.tv_sec=first->timeout.integer();
		itimerspec.it_value.tv_nsec=first->timeout.fraction(1000000000);
		//ldebug("%lf %d.%09d\n",(double)now,itimerspec.it_value.tv_sec,itimerspec.it_value.tv_nsec);
		int timer_is_set=timerfd_settime(tfd,TFD_TIMER_ABSTIME,&itimerspec,NULL);
		if(timer_is_set<0)
		{
			ldebug("%lf %lu.%09lu %s\n",(double)now,itimerspec.it_value.tv_sec,itimerspec.it_value.tv_nsec,strerror(errno));
			timeout=0;
		}
	}
	
	int status=epoll_pwait(efd, evs,sizeof(evs)/sizeof(*evs), timeout, &set);
	if(status<0)
	{
		ldebug("!epoll_wait %d\n",errno);
		if(errno==EINTR)
			return 0;
		quit=1;
		return -1;
	}
	for(epoll_event *ev=evs,*end=evs+status; ev<end; ev++)
	{
		Listener *listener=(Listener *)ev->data.ptr;
		if(!listener->handler)
			continue;
		if(debugLevel&0x02 && listener->handler!=onTimer_)
		{
			Symbol symbol((void*)listener->handler);
			ldebug("%s(%p) invoke for fd %d mask %d\n", symbol.name, listener->that, listener->fd, ev->events);
		}
		Ntp t[2],d;
		t[0].setMonotonic();
		listener->handler(listener->that, ev->events);
		t[1].setMonotonic();
		d=t[1]-t[0];
		if(d.integer()>1 && listener->handler!=onTimer_)
		{
			Symbol symbol((void*)listener->handler);
			ldebug("%s(%p) block %lf seconds\n", symbol.name, listener->that, (double)d);
		}
	}
	foreach(Listener,listener,removeds)
	{
		listener->remove();
		delete listener;
	}
	foreach(Timer, timer, timers)
	{
		if(timer->handler)
			continue;
		timer->remove();
		delete timer;
	}
	return 0;
}

Nothread::Listener *Nothread::Listeners::findByFd(int fd)
{
	foreach(Listener, listener, *this)
		if(listener->fd==fd)
			return listener;
	return NULL;
}
