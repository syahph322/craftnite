// Copyright 2022 Thierry LARMOIRE
// SPDX short identifier: MIT
#include "type.h"

#if 0
struct A
{
	typedef RelativeField<u8,3,0,8> a;
	typedef RelativeField<u8,0,0,8> b;
	Cat<u32,a,b > toto;
	u8 buf[4];
};

int rien()
{
	A toto;
	toto.buf[0]=0x12;
	toto.buf[1]=0x34;
	toto.buf[2]=0x56;
	toto.buf[3]=0x78;
	return toto.toto;
}
struct BiccDatation
{
	RelativeField<u8,0x00,0,1> validation;
}datation;

#endif

U8 BitSwap::table[256];

void BitSwap::init()
{
	for(unsigned i=0;i<sizeof(table);i++)
	{
		register U8 k=0;
		for(int j=0;j<8;j++)
		{
			k<<=1;
			k|=(i>>j)&1;
		}
		table[i]=k;
	}
}
