// Copyright 2022 Thierry LARMOIRE
// SPDX short identifier: MIT
#ifndef   __debug_h__
#define   __debug_h__

#include <stdarg.h>

#include <sys/syscall.h>
#include <sys/types.h>
#include <unistd.h>

#if 1
inline int gettid()
{
	return syscall(SYS_gettid);
}
#endif

#define ldebug(fmt, args...) _ldebug(COMPILE_DIR,__FILE__,__LINE__,__PRETTY_FUNCTION__,fmt,##args)

extern int _ldebug (const char*compileDir,const char*file,int line,const char*function,const char *fmt,...) __attribute__ ((__format__ (__printf__, 5, 6)));
extern int _ldebugv(const char*compileDir,const char*file,int line,const char*function,const char *fmt,va_list list);
extern int _debug(const char *fmt,...) __attribute__ ((__format__ (__printf__, 1, 2)));
extern int _debugv(const char *fmt,va_list list);
extern int trace(const char *fmt,...) __attribute__ ((__format__ (__printf__, 1, 2)));

#endif /* __debug_h__ */
