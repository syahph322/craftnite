// Copyright 2022 Thierry LARMOIRE
// SPDX short identifier: MIT
#include "periodic.h"
#include <sys/time.h>


Periodic::Periodic()
{
	timerId=0;
}

Periodic::~Periodic()
{
	exit();
}

int Periodic::init(Nothread *nothread,int delay,int offset,void (*handler)(void *that),void*that)
{
	this->nothread=nothread;
	this->delay=delay;
	this->offset=offset;
	this->handler=handler;
	this->that=that;
	return 0;
}

int Periodic::exit()
{
	stop();
	return 0;
}

int Periodic::start()
{
	stop();
	if(!delay)
		return 0;
	
	struct timeval now;
	gettimeofday(&now, NULL);
	long us=now.tv_sec*1000000+now.tv_usec;
	long delay=this->delay-us%this->delay;
	timerId=nothread->timerSet(delay, onPeriod_, this);
	return 0;
}

int Periodic::stop()
{
	if(timerId)
	{
		nothread->timerCancel(timerId);
		timerId=0;
	}
	return 0;
}

NOTHREAD_TIMEOUT_BODY(Periodic, onPeriod)
{
	timerId=0;
	if(!handler)
		return;
	
	handler(that);
	start();
}

