// Copyright 2022 Thierry LARMOIRE
// SPDX short identifier: MIT
#include "buffer.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

const Buffer Buffer::null=Buffer((U8*)0,0);

#ifdef BUFFER_WITH_CONST

#define BUFFER_CONST(string,name) \
	const Buffer Buffer::name=Buffer(string);
BUFFER_CONST("\r",cr)
BUFFER_CONST("\n",lf)
BUFFER_CONST("\r\n",crlf)
BUFFER_CONST(":",colon)
BUFFER_CONST(",",comma)
BUFFER_CONST(".",dot)
BUFFER_CONST("=",equal)
BUFFER_CONST("-",minus)
BUFFER_CONST(";",semicolon)
BUFFER_CONST("\'",quote)
BUFFER_CONST("\"",doubleQuote)
BUFFER_CONST("?",questionMark)
BUFFER_CONST("/",slash)
BUFFER_CONST("\\",backslash)
BUFFER_CONST("&",ampersand)
BUFFER_CONST("@",atSign)
BUFFER_CONST(" ",space)
BUFFER_CONST("*",asterisk)
BUFFER_CONST("#",hash)
BUFFER_CONST("%",percent)
BUFFER_CONST("~",tilde)
BUFFER_CONST("_",underscore)
BUFFER_CONST("|",pipe)
#undef BUFFER_CONST

#define BUFFER_CONST(openning,closing,name) \
	const Buffer Buffer::name[2]={Buffer(openning),Buffer(closing)};
BUFFER_CONST("(",")",parentheses)
BUFFER_CONST("[","]",squareBrackets)
BUFFER_CONST("{","}",curlyBrackets)
BUFFER_CONST("<",">",chevrons)
#undef BUFFER_CONST
#endif

Size Buffer::append(const char *text)
{
	register U32 l=0;

	while(*text && len)
	{
		*ptr++=*text++;
		l++;
		len--;
	}
	return l;
}

Size Buffer::append(U8 value)
{
	U8 c;

	if(len<2)
		return 0;

	c=value>>4;
	c+= c>=10 ? 'A'-10 : '0';
	*ptr++=c;

	c=value&0xf;
	c+= c>=10 ? 'A'-10 : '0';
	*ptr++=c;

	len-=2;

	return 2;
}

Size Buffer::append(U16 value)
{
	if(len<4)
		return 0;

	append((U8)(value>>8));
	append((U8)value);

	return 4;
}

Size Buffer::append(U32 value)
{
	if(len<8)
		return 0;

	append((U16)(value>>16));
	append((U16)value);

	return 8;
}

Size Buffer::copy(Buffer *b)
{
	U8 *dst=ptr,*src=b->ptr;
	U32 l=b->len;

	while(l--)
	{
		*dst++=*src++;
	}

	len=b->len;

	return b->len;
}

Buffer& Buffer::toLower()
{
	U8 *p=ptr;
	U32 l=len;
	U8 c;
	while(l)
	{
		c=*p;
		if( c>='A' && c<='Z')
		{
			c-='A'-'a';
			*p=c;
		}
		p++;
		l--;
	}
	return *this;
}

Buffer& Buffer::toUpper()
{
	U8 *p=ptr;
	U32 l=len;
	U8 c;
	while(l)
	{
		c=*p;
		if( c>='a' && c<='z')
		{
			c-='a'-'A';
			*p=c;
		}
		p++;
		l--;
	}
	return *this;
}

void Buffer::swapBuffer()
{
	U32 *p;
	U32 l;

	p=(U32*)ptr;
	l=(len+3)/sizeof(U32);

	while(l)
	{
		register unsigned r;
		r=*p;
		r=((r>>24)&0x000000ff)|((r>>8)&0x0000ff00)|((r<<8)&0x00ff0000)|((r<<24)&0xff000000);
		*p=r;
		p++;
		l--;
	}
}

void Buffer::fill(U8 byte)
{
	U32 l=len;
	U32 fill32;
	union 
	{
		U8  *p8;
		U32 *p32;
	};
	fill32 = byte;
	fill32 = (fill32<<24)|(fill32<<16)|(fill32<<8)|(fill32<<0);
	p8=ptr;

	while(l && ( ((Size)p8) & (sizeof(U32)-1) ))
	{
		*p8++=byte;
		l--;
	}

	U32 dcnt=l/sizeof(U32);
	while(dcnt)
	{
		*p32++=fill32;
		dcnt--;
	}

	l&=(sizeof(U32)-1);
	while(l)
	{
		*p8++=byte;
		l--;
	}
}

Size Buffer::search(U8 c) const
{
	U32 l=len;
	U8  *p=ptr;
	U32 i=0;
	while(l)
	{
		if(*p==c)
			return i;
		i++;
		l--;
		p++;
	}
	return notFound;
}

Size Buffer::rsearch(U8 c) const
{
	U32 l=len;
	U8  *p=ptr+len;
	while(l)
	{
		p--;
		if(*p==c)
			return p-ptr;
		l--;
	}
	return notFound;
}

Size Buffer::search(const Buffer s) const
{
	for(Size i=0;i<len;i++)
	{
		if(slice(i,i+s.len)==s)
			return i;
	}
	return notFound;
}

Size Buffer::rsearch(const Buffer s) const
{
	for(Size i=0;i<len;i++)
	{
		if(slice(len-i-s.len,len-i)==s)
			return len-i-s.len;
	}
	return notFound;
}

Size Buffer::split(Buffer separator,Buffer *args,Buffer *argsEnd) const
{
	Size count=0;
	Size l=len;
	U8  *p=ptr;

	while(args<argsEnd)
	{
		// arg new
		args->ptr=p;
		// arg bytes
		while(l)
		{
			if(separator.search(*p)!=notFound)
				break;
			p++;
			l--;
		}
		// arg end
		args->len=p-args->ptr;
		//next arg
		args++;
		count++;

		if(!l)
			break;

		p++;
		l--;
	}

	if(args==argsEnd)
	{
		args[-1].len=(ptr+len)-args[-1].ptr;
	}

	return count;
}

Size Buffer::split(Buffer separator,Size count,...) const
{
	va_list l;
	Buffer consumed(*this);
	va_start(l,count);
	Size done=0;
	while(done<count)
	{
		Buffer *arg;
		arg=va_arg(l,Buffer *);
		if(!consumed.splitNext(separator,arg))
			break;
		done++;
	}
	va_end(l);
	return done;
}

Size Buffer::split(BufferPod separator,va_list l) const
{
	Buffer consumed(*this);
	Size done=0;
	while(1)
	{
		Buffer *arg;
		arg=va_arg(l,Buffer *);
		if(!arg)
			break;
		if(!consumed.splitNext(*(Buffer*)&separator,arg))
			break;
		done++;
	}
	return done;
}

Size Buffer::splitNext(Buffer separator,Buffer *arg)
{
	if(!len)
		return 0;

	arg->ptr=ptr;
	// arg bytes
	while(len)
	{
		if(separator.search(*ptr)!=notFound)
			break;
		ptr++;
		len--;
	}
	// arg end
	arg->len=ptr-arg->ptr;

	if(len)
	{
		ptr++;
		len--;
	}

	return 1;
}

Size Buffer::splitNoEmpty(Buffer separators,Buffer *args,Size count) const
{
	Size dcnt;
	Size l=len;
	U8  *p=ptr;

	dcnt=count;
	while(dcnt && l)
	{
		// skip separators
		while(l)
		{
			if(separators.search(*p)==notFound)
				break;
			p++;
			l--;
		}
		if(!l)
			break;
		// arg new
		args->ptr=p;
		// arg bytes
		while(l)
		{
			if(separators.search(*p)!=notFound)
				break;
			p++;
			l--;
		}
		// arg end
		args->len=p-args->ptr;
		//next arg
		args++;
		dcnt--;
	}

	if(dcnt==0)
	{
		args[-1].len=(ptr+len)-args[-1].ptr;
	}

	return count-dcnt;
}

int Buffer::vformat(const char *fmt,va_list list) const
{
	if(!len)
		return 0;
	
	return vsnprintf((S8*)ptr, len, fmt, list);
}

int Buffer::format(const char *fmt,...) const
{
	int l;
	va_list list;
	
	va_start(list, fmt);
	l=vformat(fmt, list);
	va_end(list);
	return l;
}

int Buffer::vconsume(const char *fmt,va_list list)
{
	int l;
	l=vformat(fmt, list);
	ptr+=l;
	len-=l;
	return l;
}

int Buffer::consume(const char *fmt,...)
{
	va_list list;
	int l;

	va_start(list, fmt);
	l=vconsume(fmt,list);
	va_end(list);

	return l;
}

Size Buffer::cString(char *s,int lenMax) const
{
	int dcnt;
	U8 *p=ptr;
	lenMax--; // de la place pour le '\0'

	dcnt=len;
	if(dcnt>lenMax)
		dcnt=lenMax;

	lenMax=dcnt;
	while(dcnt--)
	{
		*s++=*p++;
	}
	*s='\0';

	return lenMax;
}

U32 Buffer::scanU32(U32 base) const
{
	Size l=len;
	U8  *p=ptr,c;
	U32 value=0;
	
	for(;l;value*=base,value+=c)
	{
		c=*p;
		
		p++;
		l--;
		
		if(c<'0')
			break;
		
		if(c<'0'+10)
		{
			c=c-'0';
			continue;
		}
		
		if(c<'A')
			break;
		if(c<'A'-10+base)
		{
			c=c-'A'+10;
			continue;
		}
		
		if(c<'a')
			break;
		if(c<'a'-10+base)
		{
			c=c-'a'+10;
			continue;
		}
		break;
	}
	return value;
}

S32 Buffer::scanS32(U32 base) const
{
	int neg=0;
	Size l=len;
	U8  *p=ptr;
	if(l && (*p=='-' || *p=='+'))
	{
		if(*p=='-')
			neg=1;
		l--;
		p++;
	}
	S32 value=Buffer(p,l).scanU32(base);
	if(neg)
		value=-value;
	return value;
}

long Buffer::scanLong(U32 base) const
{
	Size l=len;
	U8  *p=ptr,c;
	long value=0;
	
	for(;l;value*=base,value+=c)
	{
		c=*p;
		
		p++;
		l--;
		
		if(c<'0')
			break;
		
		if(c<'0'+10)
		{
			c=c-'0';
			continue;
		}
		
		if(c<'A')
			break;
		if(c<'A'-10+base)
		{
			c=c-'A'+10;
			continue;
		}
		
		if(c<'a')
			break;
		if(c<'a'-10+base)
		{
			c=c-'a'+10;
			continue;
		}
		break;
	}
	return value;
}

long long Buffer::scanLongLong(U32 base) const
{
	Size l=len;
	U8  *p=ptr,c;
	long long value=0;
	
	for(;l;value*=base,value+=c)
	{
		c=*p;
		
		p++;
		l--;
		
		if(c<'0')
			break;
		
		if(c<'0'+10)
		{
			c=c-'0';
			continue;
		}
		
		if(c<'A')
			break;
		if(c<'A'-10+base)
		{
			c=c-'A'+10;
			continue;
		}
		
		if(c<'a')
			break;
		if(c<'a'-10+base)
		{
			c=c-'a'+10;
			continue;
		}
		break;
	}
	return value;
}


U32 Buffer::vscanf(BufferPod fmt,va_list pList) const
{
	// a optimiser ...
	char _fmt[fmt.len+1];
	memcpy(_fmt,fmt.ptr,fmt.len);
	_fmt[fmt.len]='\0';

	char _this[len+1];
	memcpy(_this,ptr,len);
	_this[len]='\0';

	return vsscanf(_this, _fmt, pList);
}

int Buffer::getSign()
{
	int neg=0;
	if(len && (*ptr=='-' || *ptr=='+'))
	{
		if(*ptr=='-')
			neg=1;
		len--;
		ptr++;
	}
	return neg;
}

int Buffer::getBase()
{
	int base=10;
	if(len&&*ptr=='0')
	{
		len--;
		ptr++;
		base=8;
		const char *t=  "b"  "B"  "d"  "D"  "o"  "O"   "x"   "X";
		const char *b="\x2""\x2""\xa""\xa""\x8""\x8""\x10""\x10";
		while(*t)
		{
			if(*t==*ptr)
			{
				len--;
				ptr++;
				base=*b;
				break;
			}
			t++;
			b++;
		}
	}
	return base;
}

U32 Buffer::strtoU32() const
{
	Buffer cpy(*this);
	
	int neg=cpy.getSign();
	U32 base=cpy.getBase();
	S32 value;
	
	value = cpy.scanU32(base);
	if(neg)
		value=-value;
	return value;
}

unsigned long Buffer::strtoul(int base) const
{
	Buffer cpy(*this);
	
	int neg=cpy.getSign();
	if(base==0)
		base=cpy.getBase();
	unsigned long value;
	
	value = cpy.scanLong(base);
	if(neg)
		value=-value;
	return value;
}

unsigned long long Buffer::strtoull(int base) const
{
	Buffer cpy(*this);
	
	int neg=cpy.getSign();
	if(base==0)
		base=cpy.getBase();
	unsigned long long value;
	
	value = cpy.scanLongLong(base);
	if(neg)
		value=-value;
	return value;
}

float Buffer::strtof(int base) const
{
	U32 l=len;
	U8  *p=ptr,c;
	U32 value=0;
	bool fractionPart=false;
	U32 precision=1;

	while(l)
	{
		c=*p;

		p++;
		l--;

		if(c=='.')
		{
			fractionPart=true;
			continue;
		}

		if(c<'0')
			break;

		if(c<'0'+10)
		{
			c=c-'0';
			goto append;
		}

		if(c<'A')
			break;
		if(c<'A'-10+base)
		{
			c=c-'A'+10;
			goto append;
		}

		if(c<'a')
			break;
		if(c<'a'-10+base)
		{
			c=c-'a'+10;
			goto append;
		}

		break;

		append:

		if(fractionPart)
			precision*=base;

		value*=base;
		value+=c;
	}

	return ((float)value)/precision;
}

double Buffer::strtod(int base) const
{
	U32 l=len;
	U8  *p=ptr,c;
	double value=0;
	bool fractionPart=false;
	double precision=1;

	while(l)
	{
		c=*p;

		p++;
		l--;

		if(c=='.')
		{
			fractionPart=true;
			continue;
		}

		if(c<'0')
			break;

		if(c<'0'+10)
		{
			c=c-'0';
			goto append;
		}

		if(c<'A')
			break;
		if(c<'A'-10+base)
		{
			c=c-'A'+10;
			goto append;
		}

		if(c<'a')
			break;
		if(c<'a'-10+base)
		{
			c=c-'a'+10;
			goto append;
		}

		break;

		append:

		if(fractionPart)
			precision*=base;

		value*=base;
		value+=c;
	}

	return ((double)value)/precision;
}

void Buffer::trimBegin(U8 car)
{
	while(len && *ptr==car)
	{
		len--;
		ptr++;
	}
}

void Buffer::trimEnd(U8 car)
{
	U8 *pt=ptr+len;
	while(len && *--pt==car)
	{
		len--;
	}
}

void Buffer::trim(U8 car)
{
	trimBegin(car);
	trimEnd(car);
}

int Buffer::dump(FILE *f,Size pack,Size line) const
{
	int done=0;
	int status;
#define fprintf(fmt,args...) do { status=fprintf(fmt,##args); if(status<0) return -1; done+=status; } while(0)
	for(Size i=0;i<len;i+=line)
	{
		fprintf(f,"%08zx ",i);
		for(Size j=0;j<line;j++)
		{
			if(i+j<len)
				fprintf(f,"%02x",ptr[i+j]);
			else
				fprintf(f,"  ");
			if( pack && (j%pack)==(pack-1))
				fprintf(f," ");
		}
		fprintf(f," ");
		for(Size j=0;j<line;j++)
		{
			if(i+j<len)
			{
				U8 c=ptr[i+j];
				c = c<0x20 || c>=0x7f ? '.':c;
				fprintf(f,"%c",c);
			}
			else
				fprintf(f," ");
		}
		fprintf(f,"\n");
	}
#undef fprintf
	return done;
}

Size Buffer::decode16Max()
{
	return (len-1)/2+1;
}

Size Buffer::decode16(Buffer *dst)
{
	Size got=dst->len;
	U8 c;
	while(len>=2 && dst->len)
	{
		c=*ptr;
		c-='0';
		if(c>=10)
		{
			c-='A'-'0'-10;
			if(c>=16)
			{
				c-='a'-'A';
				if(c>=16)
				{
					break;
				}
			}
		}
		ptr++;len--;
		*dst->ptr=c<<4;
		c=*ptr;
		c-='0';
		if(c>=10)
		{
			c-='A'-'0'-10;
			if(c>=16)
			{
				c-='a'-'A';
				if(c>=16)
				{
					break;
				}
			}
		}
		ptr++;len--;
		*dst->ptr|=c;
		dst->ptr++;
		dst->len--;
	}
	return got-dst->len;
}

Size Buffer::encode16Max()
{
	return len*2;
}

Size Buffer::encode16(Buffer *dst)
{
	static const char* hexs="0123456789abcdef";
	
	while(len && dst->len>=2)
	{
		U8 c=*ptr++;
		len--;
		dst->ptr[0]=hexs[c>>4];
		dst->ptr[1]=hexs[c&0xf];
		dst->ptr+=2;
		dst->len-=2;
	}
	return 0;
}

Size Buffer::decode64Max()
{
	return len/4*3;
}

Size Buffer::decode64(Buffer *dst)
{
	const U8 no=~0;
	static const U8 values[] =
	{
		no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,
		no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,
		no,no,no,no,no,no,no,no,no,no,no,62,no,no,no,63,
		52,53,54,55,56,57,58,59,60,61,no,no,no,no,no,no,
		no, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9,10,11,12,13,14,
		15,16,17,18,19,20,21,22,23,24,25,no,no,no,no,no,
		no,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,
		41,42,43,44,45,46,47,48,49,50,51,no,no,no,no,no
	};
	
	Size got=dst->len;
	U8 value;
	while(len>=4)
	{
		value=values[*ptr];
		if(no==value)
			break;
		ptr++;
		len--;
		
		if(!dst->len)
			break;
		*dst->ptr=value<<2;
		
		value=values[*ptr];
		if(no==value)
			break;
		
		*dst->ptr|=value>>4;
		dst->ptr++;
		dst->len--;
		
		ptr++;
		len--;
		
		if(*ptr!='=')
		{
			if(!dst->len)
				break;
			*dst->ptr=value<<4;
			value=values[*ptr];
			if(no==value)
				break;
			
			*dst->ptr|=value>>2;
			dst->ptr++;
			dst->len--;
		}
		
		ptr++;
		len--;
		
		if(*ptr!='=')
		{
			if(!dst->len)
				break;
			*dst->ptr=value<<6;
			
			value=values[*ptr];
			if(no==value)
				break;
			
			*dst->ptr|=value;
			dst->ptr++;
			dst->len--;
		}
		
		ptr++;
		len--;
	}
	//fprintf(stderr,"got-dst->len %ld\n",got-dst->len);
	return got-dst->len;
}

Size Buffer::encode64Max()
{
	return (len+2)/3*4;
}

Size Buffer::encode64(Buffer *dst)
{
	static const U8 values[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
	Size got=dst->len;
	
	#define do_(i) dst->ptr[i]=values[(bits>>(6*(3-i)))&0x3f]
	
	while(len>=3 && dst->len>=4)
	{
		U32 bits=(ptr[0]<<16)|(ptr[1]<<8)|(ptr[2]<<0);
		do_(0);
		do_(1);
		do_(2);
		do_(3);
		
		ptr+=3;
		len-=3;
		dst->ptr+=4;
		dst->len-=4;
	}
	if(len==2 && dst->len>=4)
	{
		U32 bits=(ptr[0]<<16)|(ptr[1]<<8);
		do_(0);
		do_(1);
		do_(2);
		dst->ptr[3]='=';
		
		ptr+=2;
		len-=2;
		dst->ptr+=4;
		dst->len-=4;
	}
	else
	if(len==1 && dst->len>=4)
	{
		U32 bits=(ptr[0]<<16);
		do_(0);
		do_(1);
		dst->ptr[2]='=';
		dst->ptr[3]='=';
		
		ptr+=1;
		len-=1;
		dst->ptr+=4;
		dst->len-=4;
	}
	#undef do_
	return got-dst->len;
}

BufferMalloced::BufferMalloced()
{
	ptr=NULL;
	len=0;
}

BufferMalloced& BufferMalloced::init(const Buffer *buffer)
{
	resize(buffer->len);
	memcpy(ptr,buffer->ptr,len);
	return *this;
}

BufferMalloced& BufferMalloced::resize(Size l)
{
	if(ptr && len!=l)
	{
		// on desalloue que si la taille change
		free(ptr);
		ptr=NULL;
		len=0;
	}
	if(!ptr)
	{
		ptr=(U8*)malloc(l);
	}
	if(ptr)
		len=l;
	return *this;
}

BufferMalloced::BufferMalloced(const Buffer *buffer)
{
	init(buffer);
}

BufferMalloced::BufferMalloced(const Buffer &buffer)
{
	init(&buffer);
}

BufferMalloced::BufferMalloced(const char*string)
{
	Buffer buffer(string);
	init(&buffer);
}

BufferMalloced::~BufferMalloced()
{
	if(ptr)
		free(ptr);
}

BufferMalloced& BufferMalloced::operator=(const char*string)
{
	Buffer buffer(string);
	return *this=&buffer;
}

BufferMalloced& BufferMalloced::operator=(const Buffer *buffer)
{
	init(buffer);
	return *this;
}

BufferMalloced& BufferMalloced::operator=(const Buffer &buffer)
{
	init(&buffer);
	return *this;
}

BufferMalloced& BufferMalloced::operator+=(const Buffer &buffer)
{
	U8 *p=(U8*)malloc(len+buffer.len);
	if(ptr)
	{
		U8 *d=p,*s=ptr;
		Size l=len;
		while(l--)
			*d++=*s++;
		free(ptr);
	}
	ptr=p;
	
	U8 *d=ptr+len,*s=buffer.ptr;
	Size l=buffer.len;
	while(l--)
		*d++=*s++;
	len+=buffer.len;
	
	return *this;
}

BufferMallocedCString& BufferMallocedCString::init(const Buffer *buffer)
{
	resize(buffer->len);
	memcpy(ptr,buffer->ptr,len);
	ptr[len]='\0';
	return *this;
}

inline BufferMallocedCString& BufferMallocedCString::resize(Size l)
{
	Size need=l+1;
	//malloc alloue par bloc de 8 octets
	if(ptr && len!=l && need>8)
	{
		// on desalloue que si la taille change
		free(ptr);
		ptr=NULL;
		len=0;
	}
	if(!ptr)
	{
		if(need<8)
			need=8;
		ptr=(U8*)malloc(need);
	}
	if(ptr)
		len=l;
	return *this;
}

BufferMallocedCString::BufferMallocedCString()
{
	Buffer buffer("");
	init(&buffer);
}

BufferMallocedCString::BufferMallocedCString(const char*string)
{
	Buffer buffer(string);
	init(&buffer);
}

BufferMallocedCString::~BufferMallocedCString()
{
	if(ptr)
	{
		free(ptr);
	}
}

BufferMallocedCString& BufferMallocedCString::operator=(const char*string)
{
	Buffer buffer(string);
	init(&buffer);
	return *this;
}

BufferMallocedCString& BufferMallocedCString::operator=(const Buffer *buffer)
{
	init(buffer);
	return *this;
}
BufferMallocedCString& BufferMallocedCString::operator=(const Buffer &buffer)
{
	init(&buffer);
	return *this;
}

BufferMallocedCString& BufferMallocedCString::operator+=(const Buffer &buffer)
{
	U8 *p=(U8*)malloc(len+buffer.len+1);
	if(ptr)
	{
		U8 *d=p,*s=ptr;
		Size l=len;
		while(l--)
			*d++=*s++;
		free(ptr);
	}
	ptr=p;
	
	U8 *d=ptr+len,*s=buffer.ptr;
	Size l=buffer.len;
	while(l--)
		*d++=*s++;
	len+=buffer.len;
	
	terminate();
	
	return *this;
}

int BufferMallocedCString::vappendf(Size here,const char *fmt,va_list list)
{
	int l;
	char no[0];
	va_list copy;
	
	va_copy(copy,list);
	l=vsnprintf(no, 0, fmt, copy);
	va_end(copy);
	
	Buffer prev=*this;
	ptr=(U8*)malloc(here+l+1);
	if(ptr)
	{
		len=here+l;
		memcpy(ptr,prev.ptr,here);
		va_copy(copy,list);
		vsnprintf((char*)ptr+here, l+1, fmt, copy);
		va_end(copy);
	}
	else
		len=0;
	if(prev.ptr)
	{
		free(prev.ptr);
	}
	return l;
}

int BufferMallocedCString::appendf(const char *fmt,...)
{
	int l;
	va_list list;
	
	va_start(list, fmt);
	l=vappendf(fmt, list);
	va_end(list);
	return l;
}

int BufferMallocedCString::format(const char *fmt,...)
{
	int l;
	va_list list;
	
	va_start(list, fmt);
	l=vformat(fmt, list);
	va_end(list);
	return l;
}

char *string_dup(const char *string)
{
	char *copy;
	int len=1+strlen(string);
	copy=(char*)malloc(len);
	if(!copy)
		return 0;
	strcpy(copy,string);
	return copy;
}

#ifdef BUFFER_TEST

static void test_decode16()
{
	const char *tests[]={
		"30313233,4161"
	};
	for(unsigned i=0;i<sizeof(tests)/sizeof(*tests);i++)
	{
		Buffer src(tests[i]);
		fprintf(stderr,"%s\n",tests[i]);
		src.dump(stderr);
		U8 buf[8+src.decode16Max()];
		Buffer dst(buf,0),consumed(buf,sizeof(buf));
		while(src.len)
		{
			dst.len+=consumed.consume(Buffer("\0\0\0\1",4));
			dst.len+=src.decode16(&consumed);
			if(!src.len)
				break;
			fprintf(stderr,"skip 0x%02x\n",*src.ptr);
			src.ptr++;
			src.len--;
		}
		dst.dump(stderr);
		fprintf(stderr,"\n");
	}
}
static void test_decode64()
{
	const char *tests[]={
		"YQ==",
		"YWI=",
		"YWJj",
		"dG90bw==,dGF0YQ==",
		"Z01AFtsCwJF/4AGAAWIAABwgAAV+QQg=,aM4zyA==",
		"Z0KAHvUFgJNgSRAAAD6AAAYahgYABt3QAJJ+974HiBFQ,aM40IA=="
	};
	for(unsigned i=0;i<sizeof(tests)/sizeof(*tests);i++)
	{
		const Buffer sync("\0\0\0\1",4);
		Buffer src(tests[i]);
		fprintf(stderr,"%s\n",tests[i]);
		src.dump(stderr);
		U8 buf[2*sync.len+src.decode64Max()];
		Buffer dst(buf,0),consumed(buf,sizeof(buf));
		while(src.len)
		{
			dst.len+=consumed.consume(sync);
			dst.len+=src.decode64(&consumed);
			if(!src.len)
				break;
			fprintf(stderr,"skip 0x%02x remain %zd\n",*src.ptr,src.len);
			src.ptr++;
			src.len--;
		}
		dst.dump(stderr);
		fprintf(stderr,"\n");
	}
}

#include <time.h>
#include <unistd.h>
#include <sys/wait.h>
#include "popen.h"

static void test_encode64()
{
	time_t now,delay;;
	time(&now);
	delay=now;
	for(int l=1;l<=2;l++)
	{
		U64 count=1<<(8*l);
		for(U32 i=0;i<count;i++)
		{
			Popen popen;
			
			#if ENDIANITY!=ENDIANITY_LITTLE
			#error little only
			#endif
			Buffer src((U8*)&i,l);
			U8 buffer[src.encode64Max()];
			Buffer dst1(buffer,0),consumed(buffer,sizeof(buffer));
			dst1.len+=Buffer(src).encode64(&consumed);
			
			char b[512];
			int len;
			int status;
			const char *argv[]={"/usr/bin/openssl","enc","-base64",NULL};
			popen.init((char**)argv);
			write(popen.i,src.ptr,src.len);
			close(popen.i);
			wait(&status);
			len=read(popen.o,b,sizeof(b));
			close(popen.o);
			Buffer dst2(b,len);
			dst2.trim_right(Buffer::lf);
			time(&now);
			if(now>=delay)
			{
				fprintf(stderr,"%d/%lld\n",i,count);
				fprintf(stderr,"src:\n");
				src.dump(stderr);
				fprintf(stderr,"dst1:\n");
				dst1.dump(stderr);
				fprintf(stderr,"dst2:\n");
				dst2.dump(stderr);
				delay+=5;
			}
			if(dst1!=dst2)
			{
				fprintf(stderr,"src:\n");
				src.dump(stderr);
				fprintf(stderr,"dst1:\n");
				dst1.dump(stderr);
				fprintf(stderr,"dst2:\n");
				dst2.dump(stderr);
				return;
			}
		}
	}
}

int main(int argc,char **argv)
{
	//test_decode16();
	//test_decode64();
	test_encode64();
}

#endif

