// Copyright 2022 Thierry LARMOIRE
// SPDX short identifier: MIT
#include "debug.h"
#include "buffer.h"
#include "ntp.h"
#include <stdio.h>

int _ldebugv(const char*compileDir,const char*file,int line,const char*function,const char *fmt,va_list list)
{
	Buffer f(function);
	Size b,e;
	int len=0;
	
	e=f.search('(');
	if(e!=Buffer::notFound)
	{
		f=f.slice(0,e);
		b=f.search(' ');
		if(b!=Buffer::notFound)
		{
			f=f.slice(b+1);
		}
	}
	flockfile(stderr);
	fprintf(stderr,"%s/%s:%d: %d " BUFFER_FMT " ",compileDir,file,line,gettid(),BUFFER_ARG(f));
	vfprintf(stderr,fmt,list);
	funlockfile(stderr);
	return len;
}

int _debugv(const char *fmt,va_list list)
{
	return vfprintf(stderr,fmt,list);
}

int _ldebug(const char*compileDir,const char*file,int line,const char*function,const char *fmt,...)
{
	int len=0;
	va_list list;
	
	va_start(list, fmt);
	len=_ldebugv(compileDir,file,line,function,fmt,list);
	va_end(list);
	return len;
}

int _debug(const char *fmt,...)
{
	int len=0;
	va_list list;
	
	va_start(list, fmt);
	len=_debugv(fmt,list);
	va_end(list);
	return len;
}

int trace(const char *fmt,...)
{
	int len=0;
	va_list list;
	NtpNow now;
	
	len+=fprintf(stderr,"%.09lf ",(double)now);
	va_start(list, fmt);
	len+=vfprintf(stderr,fmt,list);
	va_end(list);
	return len;
}
