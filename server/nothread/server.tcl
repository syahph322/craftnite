# Copyright 2022 Thierry LARMOIRE
# SPDX short identifier: MIT
namespace eval command_server {}

proc command_server::prompt { fd } {
	puts $fd [clock format [clock seconds] -format "prompt %Y%m%d-%H%M%S >"]
	flush $fd
}

proc command_server::on_data { fd } {
	variable cmd
	while { -1!=[gets $fd line] } {
		if { -1!=[lsearch {quit exit} $line] } {
			close $fd
			return
		}
		append cmd $line\n
		if { [info complete $cmd] } {
			if { [catch {
				puts $fd [uplevel #0 $cmd]
			} ] } {
				puts $fd $::errorInfo
			}
			prompt $fd
			set cmd ""
		}
	}
	if { [eof $fd] } {
		close $fd
	}
}
proc command_server::on_client { fd ip port } {
	prompt $fd
	fconfigure $fd -blocking 0
	fileevent $fd readable [list command_server::on_data $fd]
}


proc command_server::init { port } {
	set fd [socket -server command_server::on_client $port]
}

command_server::init 8023
