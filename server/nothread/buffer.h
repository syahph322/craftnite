// Copyright 2022 Thierry LARMOIRE
// SPDX short identifier: MIT
#ifndef __buffer_h__
#define __buffer_h__

#ifndef __type_h__
#include "type.h"
#endif

#include <string.h>
#include <stdio.h>
#include <stdarg.h>

#define BUFFER_FMT "%.*s"
#define BUFFER_ARG(buffer) (int)(buffer).len,(const char*)(buffer).ptr

struct BufferPod
{
	// order match struct iovec
	U8 *ptr;
	Size len;
};

struct Buffer : public BufferPod
{
	static const Size notFound=~(Size)0;
	static const Buffer null;
#ifdef BUFFER_WITH_CONST
	static const Buffer cr,lf,crlf;
	static const Buffer colon,comma,dot,equal,minus,semicolon,quote,doubleQuote,questionMark,slash,backslash;
	static const Buffer ampersand,atSign,space,asterisk,hash,percent,tilde,underscore,pipe,caret;
	static const Buffer parentheses[2],squareBrackets[2],curlyBrackets[2],chevrons[2];
#endif
	
	inline U8* end()
	{
		return ptr+len;
	}
	
	inline bool isNull()
	{
		return ptr==null.ptr;
	}
	
	inline Buffer()
	{
		ptr=NULL;
		len=0;
	}

	inline Buffer(void*p,Size l)
	{
		ptr=(U8*)p;
		len=l;
	}

	inline Buffer(U8*p,Size l)
	{
		ptr=p;
		len=l;
	}

	inline Buffer(const U8*p,Size l)
	{
		ptr=(U8*)p;
		len=l;
	}

	inline Buffer(S8*p,Size l)
	{
		ptr=(U8*)p;
		len=l;
	}

	inline Buffer(const S8*p,Size l)
	{
		ptr=(U8*)p;
		len=l;
	}

	inline Buffer(const char *string)
	{
		ptr=(U8*)string;
		len=0;
		if(string)
		while(*string)
		{
			string++;
			len++;
		}
	}

	inline Buffer(U32*p,Size l)
	{
		ptr=(U8*)p;
		len=l;
	}

	inline Buffer(const Buffer &b)
	{
		ptr=b.ptr;
		len=b.len;
	}

	inline Size consume(Size l)
	{
		if(l>=len)
			l=len;
		ptr+=l;
		len-=l;
		return l;
	}
	inline Size consume(const Buffer &b)
	{
		Size l=b.len;
		if(b.len>len)
			l=len;
		memcpy(ptr,b.ptr,l);
		ptr+=l;
		len-=l;
		return l;
	}

	inline Size trim_left(const Buffer &sep)
	{
		while(len && sep.search(*ptr)!=notFound)
		{
			ptr++;
			len--;
		}
		return len;
	}

	inline Size trim_right(const Buffer &sep)
	{
		U8* end=ptr+len;
		while(len && sep.search(*--end)!=notFound)
		{
			len--;
		}
		return len;
	}

	inline Size trim(const Buffer &sep)
	{
		trim_left(sep);
		trim_right(sep);
		return len;
	}
	
	inline Size trim_left(char c)
	{
		return trim_left(Buffer(&c,1));
	}
	inline Size trim_right(char c)
	{
		return trim_right(Buffer(&c,1));
	}
	inline Size trim(char c)
	{
		return trim(Buffer(&c,1));
	}
	
	inline void replace(U8 from,U8 to)
	{
		U32 l=len;
		U8  *p=ptr;
		while(l)
		{
			if(*p==from)
				*p=to;
			p++;
			l--;
		}
	}

	inline bool operator==(const Buffer& right) const
	{
		return len==right.len && 0==memcmp(ptr,right.ptr,len);
	}
	inline bool operator!=(const Buffer& right) const
	{
		return !(*this==right);
	}
	bool operator==(const char *right) const
	{
		return 0==strncmp((char*)ptr,(char*)right,len) && right[len]=='\0';
	}
	inline bool operator!=(const char *right) const
	{
		return !(*this==right);
	}
	inline operator bool() const
	{
		return ptr ? true:false;
	}

	inline Buffer &operator=(const Buffer &b)
	{
		ptr=b.ptr;
		len=b.len;
		return *this;
	}
	inline Buffer &operator=(const char *string)
	{
		ptr=(U8*)string;
		len=0;
		if(string)
		while(*string)
		{
			string++;
			len++;
		}
		return *this;
	}
	Size append(const char *text);
	Size append(U8 value);
	Size append(U16 value);
	Size append(U32 value);
	Size copy(Buffer *b);
	inline Buffer slice(SSize beg,SSize end=~0) const
	{
		Buffer b;
		if(beg<0) beg=len-~beg;
		if(end<0) end=len-~end;
		if((Size)beg>len)
			beg=len;
		if((Size)end>len)
			end=len;
		b.ptr=ptr+beg;
		b.len=end>beg ? end-beg:0;
		return b;
	}
	inline bool beginBy(const Buffer &buf,Buffer *end=NULL) const
	{
		if(slice(0,buf.len)!=buf)
			return false;
		if(end)
			*end=slice(buf.len);
		return true;
	}

	inline bool endBy(const Buffer &buf,Buffer *begin=NULL) const
	{
		if(slice(~buf.len,~0)!=buf)
			return false;
		if(begin)
			*begin=slice(0,~buf.len);
		return true;
	}

	inline bool beginBy(const char *str,Buffer *end=NULL) const
	{
		Buffer buf(str);
		return beginBy(buf,end);
	}
	inline bool endBy(const char *str,Buffer *begin=NULL) const
	{
		Buffer buf(str);
		return endBy(buf,begin);
	}

	Buffer& toLower();
	Buffer& toUpper();
	void swapBuffer();
	void fill(U8 byte);
	inline void clear()
	{
		fill(0);
	}
	Size  search(U8 c) const;
	Size rsearch(U8 c) const;
	Size  search(const Buffer s) const;
	Size rsearch(const Buffer s) const;
	
	Size split(Buffer separators,Buffer *argsBegin,Buffer *argsEnd) const;
	inline Size split(Buffer separators,Buffer *args,Size count) const
	{
		return split(separators,args,args+count);
	}
	Size split(Buffer separators,Size count,...) const;
	inline Size split(char c,...) const
	{
		va_list pList;
		
		int l;
		
		va_start(pList, c);
		l=split(Buffer(&c,1),pList);
		va_end(pList);
		
		return l;
	}
	
	Size split(BufferPod separators,va_list list) const;
	inline Size split(Buffer separators,va_list list) const
	{
		return split((BufferPod)separators,list);
	}
	inline Size split(BufferPod separators,...) const
	{
		va_list pList;
		
		int l;
		
		va_start(pList, separators);
		l=split(separators,pList);
		va_end(pList);
		
		return l;
	}
	
	Size splitNext(Buffer separator,Buffer *arg);
	inline Size splitNext(char c,Buffer *arg)
	{
		return splitNext(Buffer(&c,1),arg);
	}
	Size splitNoEmpty(Buffer separators,Buffer *args,Size count) const;
	int vformat(const char *fmt,va_list list) const;
	int format(const char *fmt,...) const;
	int vconsume(const char *fmt,va_list list);
	int consume(const char *fmt,...);
	Size cString(char *s,int lenMax) const;
	inline U32 cString(Buffer buf) const
	{
		return cString((char *)buf.ptr,(int)buf.len);
	}
	U32 scanU32(U32 base=10) const;
	S32 scanS32(U32 base=10) const;
	long scanLong(U32 base=10) const;
	long long scanLongLong(U32 base=10) const;
	
	inline U32 vscanf(Buffer fmt,va_list pList) const
	{
		return vscanf((BufferPod)fmt,pList);
	}
	U32 vscanf(BufferPod fmt,va_list pList) const;
	inline U32 scanf(const char *fmt,...) const
	{
		va_list pList;
	
		int l;
	
		va_start(pList, fmt);
		l=vscanf(fmt,pList);
		va_end(pList);

		return l;
	}
	inline U32 scanf(BufferPod fmt,...) const
	{
		va_list pList;
		
		int l;
		
		va_start(pList, fmt);
		l=vscanf(fmt,pList);
		va_end(pList);

		return l;
	}
	int getSign();
	int getBase();
	U32 strtoU32() const;
	unsigned long strtoul(int base=0) const;
	unsigned long long strtoull(int base=0) const;
	float strtof(int base=10) const;
	double strtod(int base=10) const;
	void trim(U8 car=' ');
	void trimBegin(U8 car=' ');
	void trimEnd(U8 car=' ');
	
	inline Buffer operator-=(const Buffer &b)
	{
		this->len-=b.len;
		return *this;
	}
	inline Buffer operator-(const Buffer &b)
	{
		Buffer r(*this);
		r-=b;
		return r;
	}
	int dump(FILE *,Size pack=1,Size line=16) const;
	Size decode16Max();
	Size decode16(Buffer *dst);
	Size encode16Max();
	Size encode16(Buffer *dst);
	Size decode64Max();
	Size decode64(Buffer *dst);
	Size encode64Max();
	Size encode64(Buffer *dst);
};

template <int _size>
struct BufferAlloced : public Buffer
{
	static const Size size=_size;
	U8 buf[size];
	inline BufferAlloced(): Buffer(buf,size)
	{
	}
	inline void raz()
	{
		ptr=buf;
		len=size;
	}
	inline void consumed()
	{
		len=size-len;
		ptr=buf;
	}
};


struct BufferMalloced : public Buffer
{
	BufferMalloced();
	~BufferMalloced();
	BufferMalloced(const Buffer*buffer);
	BufferMalloced(const Buffer&buffer);
	BufferMalloced(const char*string);
	BufferMalloced& operator=(const char*string);
	BufferMalloced& operator=(const Buffer *);
	BufferMalloced& operator=(const Buffer &);
	BufferMalloced& operator+=(const Buffer &);
	BufferMalloced& resize(Size l);
	private :
		BufferMalloced& init(const Buffer*buffer);
};

struct BufferCString : public Buffer
{
	inline BufferCString() : Buffer() {}
	inline BufferCString(Buffer b) : Buffer(b) {}
	inline BufferCString(const U8*p,Size l) : Buffer(p,l) {}
	inline BufferCString(const S8*p,Size l) : Buffer(p,l) {}
	inline BufferCString(const char *s) : Buffer(s) {}
	inline char*cString() const { return (char*)ptr; }
	inline void terminate(char c) { ptr[len]=c; }
	inline void terminate() { terminate('\0'); }
};

struct BufferMallocedCString : public BufferCString
{
	BufferMallocedCString();
	~BufferMallocedCString();
	BufferMallocedCString(const BufferMallocedCString*buffer);
	BufferMallocedCString(const BufferMallocedCString&buffer);
	BufferMallocedCString(const char*string);
	BufferMallocedCString& operator=(const char*string);
	BufferMallocedCString& operator=(const Buffer *);
	BufferMallocedCString& operator=(const Buffer &);
	BufferMallocedCString& operator+=(const Buffer &);
	inline BufferMallocedCString& operator=(const BufferMallocedCString &b)
	{
		return *this=(Buffer)b;
	};
	
	BufferMallocedCString& resize(Size l);
	
	int vappendf(Size here,const char *fmt,va_list list);
	inline int vappendf(const char *fmt,va_list list)
	{
		return vappendf(len,fmt,list);
	}
	
	inline int vformat(const char *fmt,va_list list)
	{
		return vappendf(0,fmt,list);
	}
	
	int appendf(const char *fmt,...);
	int format(const char *fmt,...);
	
	private :
		BufferMallocedCString& init(const Buffer*buffer);
};

char *string_dup(const char *string);

#endif /* __buffer_h__ */
