// Copyright 2022 Thierry LARMOIRE
// SPDX short identifier: MIT
#include "script.h"

Script::Script()
{
	i=-1;
	o=-1;
}

Script::~Script()
{
	exit();
}

int Script::init(Nothread *nothread,const char **args)
{
	this->nothread=nothread;
	Popen::init(args,O_CLOEXEC);
	fcntl(o,F_SETFL,O_NONBLOCK);
	File::init(*this);
	nothread->listenerAttach(o,NOTHREAD_READABLE,onScript_,this);
	return 0;
}

int Script::exit()
{
	if(o!=-1)
	{
		nothread->listenerDetach(o);
		o=-1;
	}
	File::exit();
	Popen::exit();
	return 0;
}

int Script::printf(const char*format,...)
{
	int len;
	va_list list;
	
	va_start(list, format);
	len=vdprintf(i,format,list);
	va_end(list);
	return len;
}

NOTHREAD_HANDLER_BODY(Script,onScript)
{
	if(!readable(o))
	{
		ldebug("died!\n");
		exit();
		return;
	}
	while(1)
	{
		Buffer line;
		if(!getLine(&line))
			break;
		Buffer words[16];
		int count=line.split(Buffer(" "),words,N_ELEMENT(words));
		onLine(line,words,count);
		forget();
	}
}

int Script::onLine(Buffer line,Buffer *words,int count)
{
	ldebug("" BUFFER_FMT "\n",BUFFER_ARG(line));
	return 0;
}
