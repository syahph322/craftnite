// Copyright 2022 Thierry LARMOIRE
// SPDX short identifier: MIT
#include "popen.h"
#include "debug.h"

#include <unistd.h>
#include <sys/wait.h>
#include <string.h>
#include <fcntl.h>

int Popen::init(const char **argv,int flags)
{
	return init(argv,flags,flags);
}

int Popen::init(const char **argv,int flags_i,int flags_o)
{
	memset(fds,-1,sizeof(*fds)*4);
	
	pipe2(wpipes,flags_i);
	pipe2(rpipes,flags_o);
	
	pid=fork();
	if(0==pid)
	{
		close(wpipes[1]);
		close(rpipes[0]);
		dup2(wpipes[0], 0);
		dup2(rpipes[1], 1);
		close(wpipes[0]);
		close(rpipes[1]);
		// about the cast https://stackoverflow.com/questions/190184/execv-and-const-ness
		execv(argv[0],(char**)argv);
		ldebug("!execv %s\n",argv[0]);
		exit();
	}
	if(pid<0)
	{
		close(i);i=-1;
		close(o);o=-1;
	}
	
	close(wpipes[0]);wpipes[0]=-1;
	close(rpipes[1]);rpipes[1]=-1;
	return pid;
}

int Popen::exit()
{
	if(i!=1)
	{
		close(i);i=-1;
	}
	if(o!=1)
	{
		close(o);o=-1;
	}
	int status=-1;
	if(pid>0)
	{
		waitpid(pid,&status,0);
		pid=-1;
	}
	return status;
}
