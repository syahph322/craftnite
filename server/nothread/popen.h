// Copyright 2022 Thierry LARMOIRE
// SPDX short identifier: MIT
#ifndef __popen_h__
#define __popen_h__

struct Popen
{
	int fds[0];
	int rpipes[0];
	int o;
	int child_o;
	int wpipes[0];
	int child_i;
	int i;
	int pid;
	int init(const char **argv,int flags=0);
	int init(const char **argv,int flags_i,int flags_o);
	int exit();
};

#endif /* __popen_h__ */
