// Copyright 2022 Thierry LARMOIRE
// SPDX short identifier: MIT
#ifndef   __file_h__
#define   __file_h__

#include "buffer.h"

struct File
{
	Buffer space;
	Buffer remain;
	Buffer data;
	int init(Buffer space);
	int exit();
	bool readable(int fd);
	bool getLine(Buffer *line);
	bool getFullLine(Buffer *line);
	void forget();
};

#endif /* __file_h__ */
