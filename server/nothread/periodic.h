// Copyright 2022 Thierry LARMOIRE
// SPDX short identifier: MIT
#ifndef __periodic_h__
#define __periodic_h__

#include "nothread.h"

struct Periodic
{
	Nothread *nothread;
	Nothread::TimerId timerId;
	int delay;
	int offset;
	void (*handler)(void *that);
	void*that;
	
	Periodic();
	~Periodic();
	int init(Nothread *nothread,int delay,int offset,void (*handler)(void *that),void*that);
	int exit();
	int start();
	int stop();
	NOTHREAD_TIMEOUT_DECL(onPeriod);
};

#endif /* __periodic_h__ */
