// Copyright 2022 Thierry LARMOIRE
// SPDX short identifier: MIT
#include "ntp.h"
#include "debug.h"

const Ntp Ntp::zero={{0}};
const Ntp Ntp::min={{int64_t(~((~(uint64_t)0)>>1U))}};
const Ntp Ntp::max={{int64_t(  (~(uint64_t)0)>>1U )}};

void Ntp::String::init(const Ntp &ntp)
{
	struct tm tm;
	time_t t=ntp.integer();
	if(!localtime_r(&t,&tm))
	{
		len=snprintf(text,sizeof(text),"%ld.%09d",ntp.integer(),ntp.fraction(1000000000));
		return;
	}
	
	ptr=(U8*)text;
	len=strftime(text, sizeof(text), "%Y/%m/%d-%H:%M:%S", &tm);
}

