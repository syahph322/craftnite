// Copyright 2022 Thierry LARMOIRE
// SPDX short identifier: MIT
#ifndef __script_h__
#define __script_h__

#include "nothread.h"
#include "buffer.h"
#include "file.h"
#include "popen.h"
#include <fcntl.h>

struct Script : public Popen,File,BufferAlloced<4096>
{
	Nothread *nothread;
	Script();
	virtual ~Script();
	int init(Nothread *nothread,const char **args);
	int exit();
	int printf(const char*format,...);
	NOTHREAD_HANDLER_DECL(onScript);
	virtual int onLine(Buffer line,Buffer *words,int count);
};

#endif /* __script_h__ */
