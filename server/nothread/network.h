// Copyright 2022 Thierry LARMOIRE
// SPDX short identifier: MIT
#ifndef __network_h__
#define __network_h__

unsigned int resolv(const char* host);

#endif /* __network_h__ */
