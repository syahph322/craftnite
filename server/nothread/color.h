// Copyright 2022 Thierry LARMOIRE
// SPDX short identifier: MIT
#ifndef   __color_h__
#define   __color_h__

#include "type.h"

struct Yuva;

struct Rgba
{
	union
	{
		U32 all[1];
		struct
		{
			U8 r;
			U8 g;
			U8 b;
			U8 a;
		};
	};
	inline Rgba operator=(Yuva yuva);
	inline Rgba operator=(U32 v) { *all=v; return *this; }
	inline operator U32 () { return *all; }
} __attribute__((aligned(4),packed));

struct Yuva
{
	union
	{
		U32 all[1];
		struct
		{
			U8 y;
			U8 u;
			U8 v;
			U8 a;
		};
	};
	inline Yuva operator=(Rgba rgba);
	inline Yuva operator=(U32 v) { *all=v; return *this; }
	inline operator U32 () { return *all; }
} __attribute__((aligned(4)));


#if 0
// CCIR
#define s_y 219.0
#define s_u 224.0
#define s_v 224.0
#else
#define s_y 255.0
#define s_u 255.0
#define s_v 255.0
#endif

// ffmpeg/libavcodec/imgconvert.c
inline Yuva Yuva::operator=(Rgba rgba)
{
	int z;
//#define m(a,b) ((a/255.0)*b)
//#define j(a) (int)(a)
#define m(a,b) ((int((a/255.0)*1024))*b)
#define j(a) (a)>>10
	z=j(m( 0.29900*s_y,rgba.r)+m( 0.58700*s_y,rgba.g)+m( 0.11400*s_y,rgba.b)+m(1*s_y, 16)); if(z<0) z=0; if(z>255) z=255; y=z;
	z=j(m( 0.50000*s_v,rgba.r)+m(-0.41869*s_v,rgba.g)+m(-0.08131*s_v,rgba.b)+m(1*s_v,128)); if(z<0) z=0; if(z>255) z=255; v=z;
	z=j(m(-0.16874*s_u,rgba.r)+m(-0.33126*s_u,rgba.g)+m( 0.50000*s_u,rgba.b)+m(1*s_u,128)); if(z<0) z=0; if(z>255) z=255; u=z;
#undef m
#undef j
	a=rgba.a;
	return *this;
}

inline Rgba Rgba::operator=(Yuva yuva)
{
	int z;
//#define m(a,b,c) (a*255.0*(b-c))
//#define j(a) (int)(a)
#define m(a,b,c) ((int(a*255.0*1024))*(((int)b)-c))
#define j(a) (a)>>10
	z=j(m(1/s_y,yuva.y,16)+m( 0      /s_u,yuva.u,128)+m( 1.40200/s_v,yuva.v,128)); if(z<0) z=0; if(z>255) z=255; r=z;
	z=j(m(1/s_y,yuva.y,16)+m(-0.34414/s_u,yuva.u,128)+m(-0.71414/s_v,yuva.v,128)); if(z<0) z=0; if(z>255) z=255; g=z;
	z=j(m(1/s_y,yuva.y,16)+m( 1.77200/s_u,yuva.u,128)+m( 0      /s_v,yuva.v,128)); if(z<0) z=0; if(z>255) z=255; b=z;
#undef m
#undef j
	a=yuva.a;
	return *this;
}
#endif /* __color_h__ */
