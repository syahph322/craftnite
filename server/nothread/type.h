// Copyright 2022 Thierry LARMOIRE
// SPDX short identifier: MIT
#ifndef __type_h__
#define __type_h__

#include <sys/types.h>

#define ENDIANITY_BIG 0
#define ENDIANITY_LITTLE 1

#if defined(__i386__) || defined(__i486__) || defined(__x86_64__) || defined(__arm__) || defined(__cris__) || defined(__aarch64__)
	#define ENDIANITY ENDIANITY_LITTLE
#endif
#if defined(__powerpc__) || defined(__sparc__)
	#define ENDIANITY ENDIANITY_BIG
#endif

#ifndef ENDIANITY
#error ENDIANITY echo "" | gcc -E -dM -x c - | sort | less
#endif

#define N_ELEMENT(tab) (sizeof(tab)/sizeof(tab[0]))
#define N_INDEX(tab,element) (((long)element-(long)tab)/sizeof(tab[0]))
#define ARRAY_BEGIN(tab) &tab[0]
#define ARRAY_END(tab) &tab[N_ELEMENT(tab)]
#define ARRAY_START(tab) &tab[0]
#define ARRAY_STOP(tab) &tab[N_ELEMENT(tab)]
#define ARRAY_COUNT(tab) (sizeof(tab)/sizeof(tab[0]))
#define ARRAY_INDEX(tab,element) (((long)element-(long)tab)/sizeof(tab[0]))

const int kilo=1024;
const int mega=kilo*kilo;
const int giga=kilo*mega;

typedef          char      S8;
typedef unsigned char      U8;
typedef          short     S16;
typedef unsigned short     U16;
typedef          int       S32;
typedef unsigned int       U32;
typedef          long long S64;
typedef unsigned long long U64;

typedef ssize_t SSize;
typedef  size_t USize;
typedef USize Size;
const Size SizeMax=~(USize)0;

inline U8 *align(U8 *ptr,USize a)
{
	return (U8*) ((((unsigned long)ptr)+(a-1))&~(a-1));
}

template <int first,int len>
struct MaskMaker
{
	static const int len_ones= ~ ((~0)<<(U32)len);
	static const int mask = len_ones<<first;
};

template <>
struct MaskMaker<0,32>
{
	static const int mask=~0;
};

template <class type,int first,int len>
struct Field
{
	U32 ici[0];
	static const unsigned bitLen=len;
	inline static const U32 mask()
	{
		return MaskMaker<first,len>::mask;
	}
	inline Field & operator=(type v)
	{
		*ici= (*ici&~mask())|(((U32)v<<first)&mask());
		return *this;
	};
	inline operator type()
	{
		return (type)((*ici&mask())>>first);
	}
	inline Field & operator|=(type v)
	{
		return *this=*this|v;
	};
	inline Field & operator&=(type v)
	{
		return *this=*this&v;
	};
};

template <class type,int offset,int first,int len,class accesseur=U32>
struct RelativeField
{
	U8 ici[0];
	static const unsigned bitLen=len;
	typedef type baseType;
	inline accesseur * const address() const
	{
		return (accesseur*)(ici+offset);
	}
	inline static const accesseur mask()
	{
		return MaskMaker<first,len>::mask;
	}
	inline RelativeField & operator=(type v)
	{
		*address()= (*address()&~mask())|(((accesseur)v<<first)&mask());
		return *this;
	};
	inline operator type()
	{
		return (type)((*address()&mask())>>first);
	}
	inline RelativeField & operator|=(type v)
	{
		return *this=*this|v;
	};
	inline RelativeField & operator&=(type v)
	{
		return *this=*this&v;
	};
};

template <int first,int len>
struct Field32
{
	U32 ici[0];
	inline static const U32 mask()
	{
		return ~(((U32)~0)<<(U32)len)<<((U32)first);
	}
	inline U32 & operator=(U32 v)
	{
		*ici= (*ici&~mask())|((v<<first)&mask());
		return *ici;
	};
	inline operator U32()
	{
		return ((*ici&mask())>>first);
	}
};

template <class type,int _offset,class accesseur=type>
struct Relative
{
	U8 base[0];
	inline accesseur* address()
	{
		return (accesseur*)(base+_offset);
	}
	inline const U32 offset() const
	{
		return _offset;
	}
	inline accesseur * operator->()
	{
		return address();
	}
	inline Relative & operator=(type v)
	{
		*address()=v;
		return *this;
	};
	inline Relative & operator|=(type v)
	{
		return (*this)=v|((type)(*this));
	}
	inline Relative & operator&=(type v)
	{
		return (*this)=v&((type)(*this));
	}
	inline operator type()
	{
		return *address();
	}
	inline accesseur * operator&()
	{
		return address();
	};
};

template <class type,int offset>
struct RelativeRef
{
	U8 base[0];
	typedef type *typeRef;
	inline operator typeRef()
	{
		return (typeRef)(base+offset);
	}
	inline type& operator*()
	{
		return *(typeRef)(base+offset);
	}
	inline type * operator->()
	{
		return (typeRef)(base+offset);
	}
};

/* ca concataine les bits de deux types en un */
/* permet de creer un champ a partir de deux disjoints */
template <class type,class mstype,class lstype>
struct Cat
{
	U8 base[0];
	typedef type baseType;
	static const unsigned bitLen=mstype::bitLen+lstype::bitLen;
	inline Cat & operator=(type v)
	{
		(*(mstype*)(base))=(v>>lstype::bitLen)&~((~((type)0))<<mstype::bitLen);
		(*(lstype*)(base))=(v                )&~((~((type)0))<<lstype::bitLen);
		return *this;
	}
	inline operator type()
	{
		type v=0;
		v<<=mstype::bitLen;  // sans cette ligne ca marche pas, 
		v  =(type)(*((mstype*)(base)));
		v<<=lstype::bitLen;
		v |=(type)(*((lstype*)(base)));
		return v;
	}
};
template <class type>
struct Swap
{
	type value;
	inline static type swap(type);

	inline Swap & operator=(type v)
	{
		value=swap(v);
		return *this;
	};
	inline operator type()
	{
		return swap(value);
	}
	inline Swap & operator|=(type v)
	{
		return value= swap(v)| value;
	}
	inline Swap & operator&=(type v)
	{
		return value= swap(v)& value;
	}
}__attribute__((packed));

#if 0
#elif defined(__i386__) || defined(__i486__) || defined(__x86_64__)
template <>
inline U32 Swap<U32>::swap(U32 v)
{
	register U32 reg=v;
	asm("bswap %0": "=r" (reg): "0" (reg) );
	return reg;
}

template <>
inline U16 Swap<U16>::swap(U16 v)
{
	register U16 reg=v;
	v<<=4;
	asm("rolw $8,%0": "=r" (reg): "0" (reg) );
	return reg;
}
#elif defined(__powerpc__)
template <>
inline U32 Swap<U32>::swap(U32 v)
{
	U32 result;
	asm("rlwimi %0,%2,24,16,23" : "=&r" (result) : "0" (v>>24),  "r" (v));
	asm("rlwimi %0,%2,8,8,15"   : "=&r" (result) : "0" (result), "r" (v));
	asm("rlwimi %0,%2,24,0,7"   : "=&r" (result) : "0" (result), "r" (v));
	return result;
}

template <>
inline U16 Swap<U16>::swap(U16 v)
{
	U16 result;
	asm("rlwimi %0,%2,8,16,23" : "=&r" (result) : "0" (v >> 8), "r" (v));
	return result;
}
#elif defined(__arm__) || defined(__cris__)  || defined(__aarch64__)
#include <arpa/inet.h>
template <>
inline U32 Swap<U32>::swap(U32 v)
{
	return ntohl(v);
}

template <>
inline U16 Swap<U16>::swap(U16 v)
{
	return ntohs(v);
}
#elif defined(__sparc__)
template <>
inline U32 Swap<U32>::swap(U32 v)
{
	return ((v&0x000000ff)<<24)|((v&0x0000ff00)<<8)|((v&0x00ff0000)>>8)|((v&0xff000000)>>24);
}

template <>
inline U16 Swap<U16>::swap(U16 v)
{
	return ((v&0x00ff)<<8)|((v&0xff00)>>8);
}

#else
#error Swap
#endif

template <>
inline U64 Swap<U64>::swap(U64 v)
{
	U32 h,l;
	l=Swap<U32>::swap(v);
	h=Swap<U32>::swap(v>>32);
	return (((U64)l)<<32)|h;
}

#if ENDIANITY==ENDIANITY_LITTLE
typedef      S16  Sle16;
typedef      U16  Ule16;
typedef      S32  Sle32;
typedef      U32  Ule32;
typedef      U64  Ule64;
typedef Swap<S16> Sbe16;
typedef Swap<U16> Ube16;
typedef Swap<S32> Sbe32;
typedef Swap<U32> Ube32;
typedef Swap<U64> Ube64;
#endif
#if ENDIANITY==ENDIANITY_BIG
typedef      S16  Sbe16;
typedef      U16  Ube16;
typedef      S32  Sbe32;
typedef      U32  Ube32;
typedef      U64  Ube64;
typedef Swap<S16> Sle16;
typedef Swap<U16> Ule16;
typedef Swap<S32> Sle32;
typedef Swap<U32> Ule32;
typedef Swap<U64> Ule64;
#endif

struct BitSwap
{
	U8 value;
	operator U8() const
	{
		return table[value];
	}
	BitSwap& operator =(U8 v)
	{
		value=table[v];
		return *this;
	}
	static U8 table[256];
	static void init();
};

#endif
