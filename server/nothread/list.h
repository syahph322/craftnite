// Copyright 2022 Thierry LARMOIRE
// SPDX short identifier: MIT
#ifndef __list_h__
#define __list_h__

/*
	recupere l'adresse de la structure linkedType doublement chaine
	par le noeud nodeName a l'adresse node.
	exemple :
	
	typedef struct
	{
		char *string;
		node node[1];
	}linkedString;
	
	list stringList[1];
	node *cur;
	
	while(1)
	{
		cur=listGet(stringList);
		if(!cur)
			break;
		if(strcmp(linkedFromNode(linkedString,node,node)->string,"thierry",)==0)
			break;
	}
#define linkedFromNode(linkedType,nodeName,node) ((linkedType*)(((char *)node)-((int)((linkedType*)0)->nodeName)))
*/

#define foreach(Type,var,list) \
	for(Type* var ## _next, *var = (Type*)(list).head->::Node<Type>::next; (var ## _next = (Type*)var->::Node<Type>::next); var=(Type*)var ## _next)

#define hcaerof(Type,var,list) \
	for(Type* var ## _prev, *var = (Type*)(list).tail->::Node<Type>::tail; (var ## _prev = (Type*)var->::Node<Type>::prev); var=(Type*)var ## _prev)

#define foreach_(Type,var,list) \
	for(Type* var = (Type*)(list).head->::Node<Type>::next; ((Type*)var->::Node<Type>::next); var=(Type*)var->::Node<Type>::next)

#define foreach_find(var,list) \
	var = (typeof(var))(list).head->::Node<typeof(*var)>::next; \
	for(typeof(var) var ## _next; (var ## _next = (typeof(var))var->::Node<typeof(*var)>::next) ? true : ((var=0),false) ; var=(typeof(var))var ## _next)

template <class type>
struct Node
{
	Node<type> * volatile prev;
	Node<type> * volatile next;
	inline Node()
	{
		prev=this;
		next=this;
	}
	inline bool alone()
	{
		return prev==this /*|| next==this*/ ;
	}
	inline type *insert(Node<type> *first,Node<type> *second)
	{
		prev=first;
		next=second;
		first->next=this;
		second->prev=this;
		return (type*)this;
	};
	inline type *remove()
	{
		prev->next=next;
		next->prev=prev;
		next=this;
		prev=this;
		return (type*)this;
	};
	inline type *move_after(Node<type> *node)
	{
		return remove()->insert(node,node->next);
	};
	inline type *move_before(Node<type> *node)
	{
		return remove()->insert(node->prev,node);
	};
};

template <class type>
struct List
{
	typedef type type__;
	Node<type> head[1];
	Node<type> tail[1];
	
	inline List()
	{
		init();
	}
	inline ~List()
	{
		exit();
	}
	inline void init()
	{
		head->prev=0;
		head->next=tail;
		tail->prev=head;
		tail->next=0;
	};
	inline void exit()
	{
		foreach(type,i,*this)
		{
			i->Node<type>::remove();
		}
	};
	
	inline type* add(type *n)
	{
		return ((Node<type>*)n)->insert(head,head->next);
	}
	inline type* queue(type *n)
	{
		return ((Node<type>*)n)->insert(tail->prev,tail);
	};
	
	inline void add(List<type> *l)
	{
		register Node<type> *first;
		register Node<type> *last;
		
		first=l->head->next;
		if(first->next)
		{
			last=l->tail->prev;
			// on vide l
			l->head->next=l->tail;
			l->tail->prev=l->head;
			// on insere les anciens noeuds de l au debut de this
			first->prev=head;
			last->next=head->next;
			head->next->prev=last;
			head->next=first;
		}
	}
	inline void queue(List<type> *l)
	{
		register Node<type> *first;
		register Node<type> *last;

		first=l->head->next;
		if(first->next)
		{
			last=l->tail->prev;
			// on vide l
			l->head->next=l->tail;
			l->tail->prev=l->head;
			// on insere les anciens noeuds de l a la fin de this
			first->prev=tail->prev;
			last->next=tail;
			tail->prev->next=first;
			tail->prev=last;
		}
	};
	
	inline type *get()
	{
		register Node<type> *first;
		first=head->next;
		return first->next ? first->remove() : 0;
	};
	
	inline type *getLast()
	{
		register Node<type> *last;
		last=tail->prev;
		return last->prev ? last->remove() : 0;
	};
	
	inline int count()
	{
		register Node<type> *first, *next;
		register int count=0;

		first=head->next;
		while((next=first->next))
		{
			count++;
			first=next;
		}
		
		return count;
	};

	inline bool isEmpty()
	{
		return !head->next->next;
	}

	/*
		parcourt la liste a partir du noeud first inclu,
		pour chaque noeud, applique la fonction fnc.
		note : le noeud courant peut etre enleve de la liste et detruit car
		une copie de la reference sur le prochain noeud a ete faite
	*/

	typedef bool (*eachFunc)(type *n,void *param);
	bool each(eachFunc func,void *param)
	{
		Node<type> *cur, *next;

		cur=(Node<type> *)head->next;
		while(1)
		{
			next=cur->next;
			if(!next)
				return false;
			if((*func)((type *)cur,param))
				return true;
			cur=next;
		}
	}

};

	
template <class Type,class List=List<Type> >
struct ListIterator
{
	Type *cur,*next;
	inline ListIterator(List *list)
	{
		begin(list);
	}
	inline void begin(List *list)
	{
		cur=(Type*)list->head->next;
	}
	inline bool end()
	{
		return (next=(Type*)cur->next)==0;
	}
	inline operator bool ()
	{
		return !end();
	}
	inline ListIterator &operator++() // pre incrementation
	{
		cur=next;
		return *this;
	}
	inline ListIterator &operator++(int ) // post incrementation
	{
		cur=next;
		return *this;
	}
	inline Type* operator ->()
	{
		return (Type*)cur;
	}
	inline operator Type*()
	{
		return (Type*)cur;
	}
};

template <class Type,int count>
struct Pool : public List<Type>
{
	Type array[count];
	inline Pool()
	{
		for(int i=0;i<count;i++)
			add(array+i);
	}
};

#endif /* __list_h__ */
