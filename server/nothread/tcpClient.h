// Copyright 2022 Thierry LARMOIRE
// SPDX short identifier: MIT
#ifndef __tcpClient_hh__
#define __tcpClient_hh__

#include <stdio.h>
#include "nothread.h"
#include "buffer.h"

struct TcpClient
{
	TcpClient();
	virtual ~TcpClient();
	int init(Nothread *nothread,const Buffer hostname,int port);
	int init(Nothread *nothread,const char*hostname,int port);
	int init(Nothread *nothread,U32 ip_n,int port);
	void exit();
	
	int setBlocking(bool yes);
	int setLinger(bool onOff,int linger);
	int setKeepAlive(bool yes);
	int connect();
	int disconnect();
	int failure();
	virtual void onConnect();
	virtual void onDisconnect();
	
	Nothread::TimerId timeout;
	NOTHREAD_HANDLER_DECL(connecting);
	NOTHREAD_TIMEOUT_DECL(connectingFail);
	
	ssize_t read(void *buf, size_t count);
	inline Size read(Buffer b)
	{
		return read(b.ptr,b.len);
	}
	ssize_t write(const void *buf, size_t count);
	inline Size write(Buffer b)
	{
		return write(b.ptr,b.len);
	}
	
	Nothread *nothread;
	BufferMallocedCString hostname;
	int port;
	
	int ok;
	int ko;
	int fd;
};

#endif /* __tcpClient_hh__ */
