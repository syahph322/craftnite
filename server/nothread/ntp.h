// Copyright 2022 Thierry LARMOIRE
// SPDX short identifier: MIT
#ifndef   __ntp_h__
#define   __ntp_h__

#include <stdint.h>
#include <time.h>
#include "buffer.h"
#include "type.h"
#include "debug.h"

/*

RFC 958 say :

4. State Variables and Formats

   NTP timestamps are represented as a 64-bit fixed-point number, in
   seconds relative to 0000 UT on 1 January 1900.  The integer part is
   in the first 32 bits and the fraction part in the last 32 bits, as
   shown in the following diagram.

but :
too keep a signed value, 0 is reference 0 UT on 1 January 1970

but :
to handle after 2038
integer  part is 33 bits
fraction part is 31 bits

*/

#define NTP_1SEC_LOG2 31
#define NTP_1SEC (1LL<<NTP_1SEC_LOG2)
#define NTP_INTEGER_MASK ((~0LL)<<NTP_1SEC_LOG2)
#define NTP_FRACTION_MASK (~NTP_INTEGER_MASK)
#define NTP_Unix (3600LL*24*(365*70+70/4) <<NTP_1SEC_LOG2)
#define NTP_FROM_FLOAT(f) {{int64_t(f*NTP_1SEC)}}

struct Ntp
{
	struct Perfometer;
	struct PerfometerIgnore;
	struct Statistics;

	static const Ntp zero,min,max;
	int64_t t;
	inline Ntp &setClock(int kind)
	{
		struct timespec tp[1];
		clock_gettime(kind, tp);
		*this=*tp;
		return *this;
	}
	inline Ntp &setNow()
	{
		return setClock(CLOCK_REALTIME);
	}
	inline Ntp &setMonotonic()
	{
		return setClock(CLOCK_MONOTONIC);
	}
	inline Ntp &getResolution(int kind=CLOCK_REALTIME)
	{
		struct timespec tp[1];
		clock_getres(kind, tp);
		*this=*tp;
		return *this;
	}
	inline operator double() const
	{
		return ((double)t)/(double)(NTP_1SEC);
	}
	inline operator int64_t() const
	{
		return t;
	}
	inline Ntp &operator =(int64_t v)
	{
		t=v;
		return *this;
	}
	inline Ntp &operator =(U64 v)
	{
		t=v;
		return *this;
	}
	inline uint32_t fraction(uint32_t max) const
	{
		return ((t&NTP_FRACTION_MASK)*max)>>NTP_1SEC_LOG2;
	}
	inline uint32_t fraction() const
	{
		return t&NTP_FRACTION_MASK;
	}
	inline int64_t integer() const
	{
		return t>>NTP_1SEC_LOG2;
	}
	inline Ntp abs() const
	{
		Ntp ntp;
		ntp=(t<0 ? -t:t);
		return ntp;
	}
	inline Ntp &operator=(const timeval &tv)
	{
		t=(uint64_t(tv.tv_sec)<<NTP_1SEC_LOG2)+(uint64_t(tv.tv_usec)<<NTP_1SEC_LOG2)/1000000;
		return *this;
	}
	inline Ntp &operator=(const timespec &ts)
	{
		t=(uint64_t(ts.tv_sec)<<NTP_1SEC_LOG2)+(uint64_t(ts.tv_nsec)<<NTP_1SEC_LOG2)/1000000000;
		return *this;
	}

	inline Ntp &operator=(double seconds)
	{
		t=int64_t(seconds*NTP_1SEC);
		return *this;
	}
	inline Ntp &set(int64_t p, int64_t q)
	{
		t=((p/q)<<NTP_1SEC_LOG2)+((p%q)<<NTP_1SEC_LOG2)/q;
		return *this;
	}
	inline int64_t sub(int64_t v) const
	{
		return v*integer()+fraction(v);
	}
	inline int64_t s() const
	{
		return integer();
	}
	inline int64_t ms() const
	{
		return sub(1000);
	}
	inline int64_t us() const
	{
		return sub(1000*1000);
	}
	inline int64_t ns() const
	{
		return sub(1000*1000*1000);
	}
	struct String;
};

struct NtpNow : public Ntp
{
	inline NtpNow()
	{
		setNow();
	}
};

struct NtpMonotonic : public Ntp
{
	inline NtpMonotonic()
	{
		setMonotonic();
	}
};

#define NTP_OP(op) \
inline static Ntp operator op(const Ntp &l,const Ntp &r) \
{ \
	Ntp n; \
	n.t=l.t op r.t; \
	return n; \
} \
inline static Ntp operator op(const Ntp &l,int r) \
{ \
	Ntp n; \
	n.t=l.t op r*NTP_1SEC; \
	return n; \
} \
inline static Ntp operator op(const Ntp &l,double r) \
{ \
	Ntp n; \
	n.t=l.t op int64_t(r*NTP_1SEC); \
	return n; \
} \

NTP_OP(+)
NTP_OP(-)
NTP_OP(%)
#undef NTP_OP

#define NTP_OP(op) \
inline static bool operator op(const Ntp &l,const Ntp &r) \
{ \
	return l.t op r.t; \
} \

NTP_OP(<)
NTP_OP(>)
NTP_OP(<=)
NTP_OP(>=)
NTP_OP(==)
NTP_OP(!=)
#undef NTP_OP

inline static Ntp operator /(const Ntp &n,unsigned d)
{
	Ntp r;
	r.t=n.t/d;
	return r;
}

inline static Ntp operator /(const Ntp &n,unsigned long d)
{
	Ntp r;
	r.t=n.t/d;
	return r;
}

struct Ntp::Perfometer
{
	Ntp ntps[2];
	inline void start() { ntps[0].setMonotonic(); }
	inline void stop () { ntps[1].setMonotonic(); }
	inline operator Ntp() { return ntps[1]-ntps[0]; }
};

struct Ntp::PerfometerIgnore
{
	inline void start() {}
	inline void stop () {}
};

struct Ntp::Statistics
{
	Ntp values[128];
	size_t index;
	inline void operator=(Ntp ntp)
	{
		values[index++]=ntp;
		if(index>=N_ELEMENT(values))
			index=0;
	}
	Statistics()
	{
		index=0;
		for(size_t i=0;i<N_ELEMENT(values);i++)
		{
			values[i]=zero;
		}
	}
	struct Result
	{
		Ntp min,max,mean;
	};
	inline operator Result()
	{
		Result r;

		r.min=Ntp::min;
		r.max=Ntp::max;
		r.mean=Ntp::zero;
		for(size_t i=0;i<N_ELEMENT(values);i++)
		{
			if(r.min>values[i])
				r.min=values[i];
			if(r.max<values[i])
				r.max=values[i];
			r.mean = r.mean + values[i]/N_ELEMENT(values);
		}
		return r;
	}
};

struct Ntp::String : public BufferCString
{
	char text[64];
	void init(const Ntp &ntp);
	inline String(const Ntp &ntp)
	{
		init(ntp);
	}
	inline String()
	{
		NtpNow now;
		init(now);
	}
};

#endif /* __ntp_h__ */
