// Copyright 2022 Thierry LARMOIRE
// SPDX short identifier: MIT
#ifndef   __var_h__
#define   __var_h__

#include "type.h"
#include "ntp.h"
#include "buffer.h"
#include "list.h"
#include "color.h"
#include <time.h>

struct delay_t
{
	time_t t;
	inline delay_t& operator=(const delay_t&d)
	{
		t=d.t;
		return *this;
	}
	inline delay_t& operator=(time_t v)
	{
		t=v;
		return *this;
	}
};

#define VAR_TYPES \
VAR_TYPE(string,BufferMallocedCString, \
{ \
	*value=string; \
}, \
{ \
	string=*value; \
	return string; \
}) \
VAR_TYPE(const_string,const char *, \
{ \
}, \
{ \
	string=*value; \
	return string; \
}) \
VAR_TYPE(integer,int, \
{ \
	*value=string.strtoU32(); \
}, \
{ \
	string.format("%d",*value); \
	return string; \
}) \
VAR_TYPE(time,time_t, \
{ \
	*value=string.strtoul(); \
}, \
{ \
	string.format("%d",(int)*value); \
	return string; \
}) \
VAR_TYPE(delay,delay_t, \
{ \
	*value=string.strtoul(); \
}, \
{ \
	string.format("%d",value->t); \
	return string; \
}) \
VAR_TYPE(double,double, \
{ \
	*value=string.strtod(10); \
}, \
{ \
	string.format("%lf",*value); \
	return string; \
}) \
VAR_TYPE(U16,U16, \
{ \
	*value=string.strtoU32(); \
}, \
{ \
	string.format("%u",(U32)*value); \
	return string; \
}) \
VAR_TYPE(U32,U32, \
{ \
	*value=string.strtoU32(); \
}, \
{ \
	string.format("%u",*value); \
	return string; \
}) \
VAR_TYPE(U64,U64, \
{ \
	*value=string.strtoul(); \
}, \
{ \
	string.format("%llu",*value); \
	return string; \
}) \
VAR_TYPE(rgba,Rgba, \
{ \
	*(U32*)value->all=string.strtoU32(); \
}, \
{ \
	string.format("0x%08x",*(U32*)value->all); \
	return string; \
}) \
VAR_TYPE(boolean,bool, \
{ \
	*value=string.strtoU32(); \
}, \
{ \
	string.format("%u",(unsigned int)*value); \
	return string; \
}) \

struct Var : public Node<Var>
{
	typedef void Value;
	struct Type : public Node<Type>
	{
		BufferMallocedCString name;
		inline Type(const char*name)
		{
			this->name=name;
		}
		virtual ~Type();
		virtual void set(Value *value,const Buffer &string)=0;
		virtual const BufferCString &get(Value *value,BufferMallocedCString &string)=0;
	};
	BufferMallocedCString name;
	const char*man;
	BufferMallocedCString asString;
	int readOnly;
	Type *type;
	Value *value;
	void *that;
	int (*onSet)(void *that,Var*var);
	int (*onGet)(void *that,Var*var);
	struct Listener : public Node<Listener>
	{
		void *that;
		int (*onEvent)(void *that,Listener *listener,Var*var);
		#define VAR_LISTENER_DECL(name) \
			static int name ## _(void *that,Var::Listener *listener,Var *var); \
			int name(Var::Listener *listener,Var *var);
		
		#define VAR_LISTENER_BODY(klass,name) \
			int klass::name ## _(void *that,Var::Listener *listener,Var *var) \
			{ \
				return ((klass*)that)->name(listener,var); \
			} \
			 \
			int klass::name(Var::Listener * __attribute__((unused)) listener,Var * __attribute__((unused)) var)
	};
	List<Listener> listeners;
	Ntp ntp;
	inline void signal(Ntp ntp)
	{
		this->ntp=ntp;
		foreach(Listener,listener,listeners)
		{
			listener->onEvent(listener->that,listener,this);
		}
	}
	inline void signal()
	{
		NtpNow ntp;
		signal(ntp);
	}
	inline Var()
	{
		that=NULL;
		ntp=Ntp::zero;
	}
	inline ~Var()
	{
		while(Listener*listener=listeners.get())
		{
			listener->onEvent(listener->that,listener,NULL);
		}
	}
	inline void set()
	{
		if(that && onSet)
			onSet(that,this);
	}
	inline void get()
	{
		if(that && onGet)
			onGet(that,this);
	}
	inline Var& operator=(Buffer &string)
	{
		type->set(value,string);
		set();
		return *this;
	}
	inline operator const BufferCString&()
	{
		get();
		return type->get(value,asString);
	}
	int load(const char*filename);
	
	#define VAR_TYPE(name_,base_,set_,get_) \
		struct name_ ## _ : public Type \
		{ \
			inline name_ ## _(const char*name) : Type(name) {}; \
			void set(Value *value,const Buffer &string); \
			const BufferCString &get(Value *value,BufferMallocedCString &string); \
		}; \
		static name_ ## _ name_ ## __; \
	/**/
	VAR_TYPES
	#undef VAR_TYPE
	
};

struct Vars : public List<Var>
{
	inline Var* find(Buffer &name)
	{
		foreach(Var,var,*this)
		{
			if(var->name==name)
				return var;
		}
		return NULL;
	}
	inline Var* find(const char *cString)
	{
		Buffer name(cString);
		return find(name);
	}
	inline Var* find(void *value)
	{
		foreach(Var,var,*this)
		{
			if(var->value==value)
				return var;
		}
		return NULL;
	}
	#define VAR_PROC_DECL(name) \
		static int name ## _(void *that,Var *var); \
		int name(Var * var=NULL);

	#define VAR_PROC_BODY(klass,name) \
		int klass::name ## _(void *that,Var *var) \
		{ \
			return ((klass*)that)->name(var); \
		} \
		 \
		int klass::name(Var * __attribute__((unused)) var)
	typedef int (*Callback)(void *that,Var *var);

	#define VAR_TYPE(name_,base_,set_,get_) \
		inline Var *addRo(base_ *base,const char*man,const char*fmt,...) \
		{ \
			Var *var; \
			va_list l; \
			va_start(l,fmt); \
			var=add(1,base,NULL,NULL,NULL,man,fmt,l); \
			va_end(l); \
			return var; \
		} \
		inline Var *add(base_ *base,const char*man,const char*fmt,...) \
		{ \
			Var *var; \
			va_list l; \
			va_start(l,fmt); \
			var=add(0,base,NULL,NULL,NULL,man,fmt,l); \
			va_end(l); \
			return var; \
		} \
		inline Var *add(base_ *base,void *that, Callback onSet,Callback onGet,const char*man,const char*fmt,...) \
		{ \
			Var *var; \
			va_list l; \
			va_start(l,fmt); \
			var=add(0,base,that,onSet,onGet,man,fmt,l); \
			va_end(l); \
			return var; \
		} \
		inline Var *add(int readOnly,base_ *value,void *that, Callback onSet,Callback onGet,const char*man,const char*fmt,va_list l) \
		{ \
			return add(readOnly,(void*)value,&Var:: name_ ## __,that,onSet,onGet,man,fmt,l); \
		} \
	/**/
	VAR_TYPES
	#undef VAR_TYPE
	Var *add(int readOnly,void *,Var::Type *type,void *that, Callback onSet,Callback onGet,const char*man,const char*fmt,va_list l);
	inline void del(void *value)
	{
		Var *var=find(value);
		if(!var)
			return;
		var->remove();
		delete var;
	}
	int init();
	int exit();
	int load(const char*filename);
	int save(const char*filename);
};

#define VAR

#endif /* __var_h__ */
