// Copyright 2022 Thierry LARMOIRE
// SPDX short identifier: MIT
#include "debug.h"
#include "file.h"
#include <unistd.h>
#include <errno.h>

int File::init(Buffer space)
{
	this->space=space;
	remain=space;
	data.ptr=space.ptr;
	data.len=0;
	return 0;
}

int File::exit()
{
	space=Buffer::null;
	remain=Buffer::null;
	data=Buffer::null;
	return 0;
}

bool File::readable(int fd)
{
	ssize_t status;
	if(remain.len==0)
	{
		ldebug("no space left -- space %p:%zd data %p:%zd remain %p:%zd\n",space.ptr,space.len,data.ptr,data.len,remain.ptr,remain.len);
		return false;
	}
	status=::read(fd,remain.ptr,remain.len);
	//ldebug("status %d %d\n",status,errno);
	if(status<0)
	{
		if(errno==EAGAIN)
			return true;
		else
			return false;
	}
	
	if(status==0)
	{
		//ldebug("%d %d\n",data.len,remain.len);
		return false;
	}
	remain.ptr+=status;
	remain.len-=status;
	data.len+=status;
	//ldebug("{{" BUFFER_FMT "}}\n",BUFFER_ARG(data));
	return true;
}

bool File::getLine(Buffer *line)
{
	if(!getFullLine(line))
		return false;
	
	U8 *end=line->end(),c;
	while(line->len && ((c=*--end), (c=='\r' || c=='\n')))
		line->len--;
	
	return true;
}

bool File::getFullLine(Buffer *line)
{
	for(Size i=0;i<data.len;i++)
	{
		if(data.ptr[i]=='\n')
		{
			line->ptr=data.ptr;
			line->len=i+1;
			data.ptr+=line->len;
			data.len-=line->len;
			
			return true;
		}
	}
	return false;
}

void File::forget()
{
	Size len=data.len;
	if(len)
	{
		U8 *dst=space.ptr;
		U8 *src=data.ptr;
		while(len)
		{
			*dst++=*src++;
			len--;
		}
	}
	remain.ptr=space.ptr+data.len;
	remain.len=space.len-data.len;
	data.ptr=space.ptr;
	//ldebug("space %p:%d data %p:%d remain %p:%d\n",space.ptr,space.len,data.ptr,data.len,remain.ptr,remain.len);
}

