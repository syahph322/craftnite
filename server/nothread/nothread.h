// Copyright 2022 Thierry LARMOIRE
// SPDX short identifier: MIT
#ifndef __nothread_hh__
#define __nothread_hh__

#include "list.h"
#include "ntp.h"
#include <signal.h>
#include <time.h>
#include <stdio.h>
#include <stdarg.h>
#include <sys/epoll.h>

struct Nothread
{
	typedef void *TimerId;
	#define NOTHREAD_HANDLER_DECL(name) \
		static void name##_(void *that,int mask); \
		void name(int mask=0); \
	/**/
	#define NOTHREAD_HANDLER_BODY(klass,name) \
		void klass::name##_(void *that,int mask) \
		{ \
			((klass*)that)->name(mask); \
		} \
		void klass::name(int mask) \
	/**/
	#define NOTHREAD_TIMEOUT_DECL(name) \
		static void name##_(void *that); \
		void name(); \
	/**/
	#define NOTHREAD_TIMEOUT_BODY(klass,name) \
		void klass::name##_(void *that) \
		{ \
			((klass*)that)->name(); \
		} \
		void klass::name() \
	/**/
	inline Nothread()
	{
		efd=-1;
		sfd=-1;
		tfd=-1;
	}
	inline ~Nothread()
	{
		exit();
	}
	static const int dontQuitOnSignal=1<<0;
	int init(int options=0);
	int exit();
	int listenerAttach(int fd,int mask,void (*handler)(void *that,int mask),void*that);
	int listenerDetach(int fd);
	TimerId timerSet(int microseconds,void (*handler)(void *that),void*that);
	int     timerCancel(TimerId timerId);
	TimerId periodicSet(int ualign,int udelay,int uoffset,void (*handler)(void *that),void*that);
	int     periodicCancel(TimerId timerId);
	inline int setBackgroundHandling(int fd,int mask,void (*handler)(void *that,int mask),void*that)
	{
		return listenerAttach(fd,mask,handler,that);
	}
	inline int disableBackgroundHandling(int fd)
	{
		return listenerDetach(fd);
	}
	inline TimerId scheduleDelayedTask(int microseconds,void (*handler)(void *that),void*that)
	{
		return timerSet(microseconds,handler,that);
	}
	inline int unscheduleDelayedTask(TimerId timerId)
	{
		return timerCancel(timerId);
	}

	#define NOTHREAD_READABLE EPOLLIN
	#define NOTHREAD_WRITABLE EPOLLOUT
	#define NOTHREAD_ERROR    EPOLLERR

	int debugLevel;
	int debug(const char *fmt,...);
	
	NOTHREAD_HANDLER_DECL(onSignal);
	NOTHREAD_HANDLER_DECL(onTimer);
	int efd;
	int sfd;
	int tfd;
	int quit;
	int doEvent();
	inline int doEventLoop()
	{
		quit=0;
		while(!quit)
		{
			doEvent();
		}
		return 0;
	};
	
	// Listener internal
	struct Listener : public Node<Listener>
	{
		inline Listener(int fd, void (*handler)(void *that,int mask), void*that)
		{
			this->fd=fd;
			this->handler=handler;
			this->that=that;
		}
		int fd;
		void (*handler)(void *that, int mask);
		void *that;
	};
	struct Listeners : public List<Listener>
	{
		Listener *findByFd(int fd);
	};
	Listeners listeners;
	Listeners removeds;
	
	// Timer internal
	struct Timer : public Node<Timer>
	{
		inline Timer(void (*handler)(void *that),void*that)
		{
			this->handler=handler;
			this->that=that;
		}
		Ntp timeout;
		void (*handler)(void *that);
		void*that;
		Ntp align;
		Ntp delay;
		Ntp offset;
		void schedule(const NtpMonotonic &now);
	};
	struct Timers : public List<Timer>
	{
		void insert(Timer *t);
	};
	Timers timers;
};

#endif/* __nothread_hh__ */
