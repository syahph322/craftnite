// Copyright 2022 Thierry LARMOIRE
// SPDX short identifier: MIT
#include "var.h"
#include "debug.h"
#include <stdarg.h>

Var::Type::~Type()
{
}

#define VAR_TYPE(name_,base_,set_,get_) \
Var::name_ ## _ Var::name_ ## __(# name_); \
void Var::name_ ## _::set(Value *value_,const Buffer & __attribute__((unused)) string ) \
{ \
	base_ * __attribute__((unused)) value=(base_*)value_; \
	set_ \
} \
const BufferCString &Var::name_ ## _::get(Value *value_,BufferMallocedCString &string) \
{ \
	base_ * __attribute__((unused)) value=(base_*)value_; \
	get_ \
} \
/**/
VAR_TYPES
#undef VAR_TYPE

int Var::load(const char*filename)
{
	Buffer crlf("\r\n");
	int i=0;
	BufferAlloced<1024> line;
	FILE *file=fopen(filename,"r");
	if(!file)
		return -1;
	while(fgets((char*)line.buf,line.size,file))
	{
		i++;
		line.len=strlen((char*)line.buf);
		line.trim_right(crlf);
		if(line.beginBy("#"))
			continue;
		Size p;
		p=line.search('=');
		if(p==Buffer::notFound)
			continue;
		Buffer name,value;
		name=line.slice(0,p);
		value=line.slice(p+1);
		if(this->name==name)
		{
			//ldebug("init " BUFFER_FMT " " BUFFER_FMT "\n",BUFFER_ARG(name),BUFFER_ARG(value));
			*this=value;
			break;
		}
	}
	fclose(file);
	return 0;
}


int Vars::init()
{
	return 0;
}

int Vars::exit()
{
	foreach(Var,var,*this)
	{
		var->remove();
		delete var;
	}
	return 0;
}

int Vars::load(const char*filename)
{
	Buffer crlf("\r\n");
	//ldebug("loading %s\n",filename);
	int i=0;
	BufferAlloced<1024> line;
	FILE *file=fopen(filename,"r");
	if(!file)
	{
		ldebug("!fopen %s\n",filename);
		return -1;
	}
	while(fgets((char*)line.buf,line.size,file))
	{
		i++;
		line.len=strlen((char*)line.buf);
		line.trim_right(crlf);
		if(line.beginBy("#"))
			continue;
		Size p;
		p=line.search('=');
		if(p==Buffer::notFound)
			continue;
		Buffer name,value;
		name=line.slice(0,p);
		value=line.slice(p+1);
		Var *var;
		var=find(name);
		if(var)
		{
			if(!var->readOnly)
				*var=value;
		}
		else
		{
			//ldebug("%s:%d:var " BUFFER_FMT " not found\n",filename,i,BUFFER_ARG(name));
			var = new Var();
			var->readOnly=0;
			var->man="";
			var->that=NULL;
			var->onSet=NULL;
			var->onGet=NULL;
			var->name=name;
			var->asString=value;
			var->value=NULL;
			var->type=NULL;
			queue(var);
		}
	}
	fclose(file);
	return 0;
}

int Vars::save(const char*filename)
{
	FILE *file=fopen(filename,"w");
	if(!file)
	{
		ldebug("!open %s\n",filename);
		return -1;
	}
	
	foreach(Var,var,*this)
	{
		if(var->readOnly)
			continue;
		if(var->onGet)
			continue;
		if(!var->type)
			continue;
		
		BufferCString value=(BufferCString)*var;
		fprintf(file,BUFFER_FMT "=" BUFFER_FMT "\n",BUFFER_ARG(var->name),BUFFER_ARG(value));
	}
	fclose(file);
	return 0;
}

Var *Vars::add(int readOnly, void *value,Var::Type *type,void *that, Callback onSet, Callback onGet, const char*man,const char*fmt,va_list l)
{
	Var *var=new Var();
	var->readOnly=readOnly;
	var->man=man;
	var->that=that;
	var->onSet=onSet;
	var->onGet=onGet;
	var->name.vformat(fmt,l);
	var->value=value;
	var->type=type;
	Var *found=find(var->name);
	queue(var);
	//ldebug("%s %s\n",var->name.cString(),!found?"<nil>":found->asString.cString());
	if(found)
	{
		found->remove();
		*var=found->asString;
		delete found;
	}
	//var->load(filename.cString());
	return var;
}

