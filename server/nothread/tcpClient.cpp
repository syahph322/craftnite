// Copyright 2022 Thierry LARMOIRE
// SPDX short identifier: MIT
#include "tcpClient.h"
#include "network.h"
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <sys/fcntl.h>
#include <sys/socket.h>
#include <net/if.h>
#include <arpa/inet.h>

TcpClient::TcpClient()
{
	fd=-1;
	timeout=0;
}

TcpClient::~TcpClient()
{
}

int TcpClient::init(Nothread * nothread,const Buffer hostname,int port)
{
	this->nothread=nothread;
	this->hostname=hostname;
	this->port=port;
	ok=0;
	ko=0;
	fd=-1;
	return 0;
}

int TcpClient::init(Nothread * nothread,const char*hostname,int port)
{
	return init(nothread,Buffer(hostname),port);
}

void TcpClient::exit()
{
	disconnect();
}

int TcpClient::setBlocking(bool yes)
{
	int flags = fcntl (fd, F_GETFL);
	if (flags < 0)
		return -1;
	if(yes)
		flags&=~O_NONBLOCK;
	else
		flags|= O_NONBLOCK;
	flags = fcntl (fd, F_SETFL, flags);
	if (flags < 0)
		return -1;
	return 0;
}

int TcpClient::setLinger(bool o,int l)
{
	int result;
	struct linger linger;
	linger.l_onoff  = o  ? 1:0;
	linger.l_linger = l;
	result = setsockopt(fd, SOL_SOCKET, SO_LINGER, &linger, sizeof(linger));
	return result;
}

int TcpClient::setKeepAlive(bool yes)
{
	int result;
	int value=yes?1:0;
	result = setsockopt(fd, SOL_SOCKET, SO_KEEPALIVE, &value, sizeof(value));
	return result;
}

int TcpClient::connect()
{
	if(fd!=-1)
		return 0;

	int status=-1;
	struct sockaddr_in serv_addr;
	
	fd = socket (AF_INET, SOCK_STREAM|SOCK_CLOEXEC, 0);
	if(fd<0)
		goto error;
	
	memset (&serv_addr, 0x0, sizeof(serv_addr));
	
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = resolv (hostname.cString());
	serv_addr.sin_port = htons (port);
	
	if(0!=setBlocking(false))
	{
		ldebug("!getsockopt %20s %d %d\n",hostname.cString(),port,errno);
		goto error;
	}
	
	status = ::connect (fd, (sockaddr *) &serv_addr, sizeof(serv_addr));
	//ldebug("%s %d connect %d %d\n",hostname,port,status,errno);
	if(status==0)
	{
		ok++;
		ko=0;
		onConnect();
		return 0;
	}
	if(errno==EINPROGRESS)
	{
		timeout=nothread->scheduleDelayedTask(1000*1000,connectingFail_,this);
		nothread->setBackgroundHandling(fd,NOTHREAD_WRITABLE,connecting_,this);
		return 0;
	}
	
error:
	ldebug("\n");
	failure();
	return status;
}

int TcpClient::disconnect()
{
	if(fd>=0)
	{
		nothread->disableBackgroundHandling(fd);
		shutdown(fd,SHUT_RDWR);
		close(fd);
		fd=-1;
	}
	
	onDisconnect();
	
	return 0;
}

int TcpClient::failure()
{
	if(!ko)
		ldebug("\n");
	ko++;
	ok=0;
	return disconnect();
}

void TcpClient::onConnect()
{
	ldebug("%20s %d\n",hostname.cString(),port);
}

void TcpClient::onDisconnect()
{
	ldebug("%20s %d\n",hostname.cString(),port);
}

NOTHREAD_HANDLER_BODY(TcpClient,connecting)
{
	int error = 0;
	socklen_t len = sizeof(error);
	if (getsockopt(fd, SOL_SOCKET, SO_ERROR, &error, &len) < 0)
	{
		if(ko==0)
			ldebug("getsockopt %20s %d %d %d\n",hostname.cString(),port,error,errno);
		nothread->unscheduleDelayedTask(timeout);
		timeout=0;
		nothread->disableBackgroundHandling(fd);
		failure();
		return;
	}
	
	if(error==EINPROGRESS)
		return;
	
	if(error)
	{
		if(ko==0)
			ldebug("getsockopt %20s %d %d %d\n",hostname.cString(),port,error,errno);
		nothread->unscheduleDelayedTask(timeout);
		timeout=0;
		nothread->disableBackgroundHandling(fd);
		failure();
		return;
	}
	nothread->unscheduleDelayedTask(timeout);
	timeout=0;
	nothread->disableBackgroundHandling(fd);
	ok++;
	ko=0;
	onConnect();
}

NOTHREAD_TIMEOUT_BODY(TcpClient,connectingFail)
{
	if(ko==0)
		ldebug("\n");
	timeout=0;
	nothread->disableBackgroundHandling(fd);
	failure();
}

ssize_t TcpClient::read(void *buf, size_t count)
{
	ssize_t status;
	if(fd<0)
		return -1;
	status=::read(fd,buf,count);
	if(status<0 && errno!=EAGAIN)
	{
		failure();
		ldebug("%20s %d %zd %d\n",hostname.cString(),port,status,errno);
	}
	return status;
}

ssize_t TcpClient::write(const void *buf, size_t count)
{
	ssize_t status;
	if(fd<0)
		return -1;
	status=::write(fd,buf,count);
	if(status<0 && errno!=EAGAIN)
	{
		ldebug("%20s %d %zd %d\n",hostname.cString(),port,status,errno);
		failure();
	}
	return status;
}
