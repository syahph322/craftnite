// Copyright 2022 Thierry LARMOIRE
// SPDX short identifier: MIT
#include "chunk.h"

#include <stdlib.h>
#include "nothread/buffer.h"
#include "player.h"

Chunk::Chunk(V3 pos)
{
	this->pos=pos;
	data=(U16*)calloc(sizeof(U16),len);
	hard=(U8*)calloc(sizeof(U8),len);
	//ldebug("(%d,%d,%d)\n",pos.i,pos.j,pos.k);
}

Chunk::~Chunk()
{
	//ldebug("(%d,%d,%d)\n",pos.i,pos.j,pos.k);
	free(hard);
	free(data);
}

void Chunk::borderStone()
{
	U16 *p=data;
	for(int k=0;k<side;k++)
	for(int j=0;j<side;j++)
	for(int i=0;i<side;i++)
	{
		*p++ = false
			|| ((i+1)&(side-2))==0
			|| ((j+1)&(side-2))==0
			|| ((k+1)&(side-2))==0
		? 1024:0;
	}
}

bool Chunk::empty()
{
	for(U16 *p=data,*e=p+len;p<e;p++)
		if(*p)
			return false;
	return true;
}

void Chunk::send(Player *player, int kind)
{
	BufferMalloced b;
	b.resize(1+4*sizeof(U32)+len*2*sizeof(U16));
	Player::Buffer x=b,c;
	x.putU8(kind);
	x.putU32(pos.i).putU32(pos.j).putU32(pos.k);
	c=x;
	x.putU32(0);
	
	int check=0;
	U16 *p=data,*e=p+len;
	int q=~0,v,rl=0;
	while(1)
	{
		v= p<e ? *p : ~0;
		if(q!=v)
		{
			if(rl)
			{
				x.putU16(q);
				x.putU16(rl);
				check+=rl;
				rl=0;
			}
			if(v==~0)
				break;
			q=v;
		}
		rl++;
		p++;
	}
	int k=(x.ptr-(c.ptr+sizeof(U32)))/(2*sizeof(U16));
	c.putU32(k);
	b.len=x.ptr-b.ptr;
	player->send(b);
	if(k==1)
		ldebug("rl %d %d %d\n",k,q,check);
}
