#ifndef __unicode_utf8_h__
#define __unicode_utf8_h__

#include "nothread/type.h"

int unicode_to_utf8(const U16  *uni_ptr, size_t uni_len,char *utf_ptr, size_t len);
int utf8_to_unicode(const char *utf_ptr, size_t utf_len,U16  *uni_ptr, size_t len);

#endif /* __unicode_utf8_h__ */
