// Copyright 2022 Thierry LARMOIRE
// SPDX short identifier: MIT
#ifndef __pam_h__
#define __pam_h__

#include <security/pam_appl.h>

struct Pam
{
	int init();
	int exit();
	int check(const char*user, const char*pass, int *checked);
	int change(const char*user, const char*curr, const char*next);
	
	struct pam_conv conv;
	const char*pass;
	const char*next;
	static int func_(int num_msg, const struct pam_message **msg, struct pam_response **resp, void *appdata_ptr);
	int func(int num_msg, const struct pam_message **msg, struct pam_response **resp);
	static void delay_(int status, int delay, void *appdata_ptr);
	void delay(int status, int delay);

};

#endif /*  __pam_h__ */
