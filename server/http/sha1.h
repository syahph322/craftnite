// Copyright 2022 Thierry LARMOIRE
// SPDX short identifier: MIT
#ifndef __sha1_h__
#define __sha1_h__

#include "nothread/buffer.h"
#ifdef HTTP_WITH_CRYPTO
#include <openssl/sha.h>
#endif

struct Sha1
{
#ifdef HTTP_WITH_CRYPTO
	static const size_t len=SHA_DIGEST_LENGTH;
	SHA_CTX ctx[1];
#else
	// from rfc3174
	static const size_t len=160/8;
	U32 hash[len/4];
	U64 dataLength;
	
	int index;
	U8 block[64];
	
	void pad();
	void process();
#endif
	int init();
	int update(Buffer b);
	int exit(U8 result[len]);
};

#endif /* __sha1_h__ */
