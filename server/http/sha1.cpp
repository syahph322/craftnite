// Copyright 2022 Thierry LARMOIRE
// SPDX short identifier: MIT
#include "sha1.h"

#ifdef HTTP_WITH_CRYPTO

int Sha1::init()
{
	return SHA1_Init(ctx);
}
int Sha1::update(Buffer b)
{
	return SHA1_Update(ctx, b.ptr, b.len);
}

int Sha1::exit(U8 result[len])
{
	return SHA1_Final(result, ctx);
}

#else

// from rfc3174

int Sha1::init()
{
	dataLength=0;
	index=0;
	
	hash[0]   = 0x67452301;
	hash[1]   = 0xEFCDAB89;
	hash[2]   = 0x98BADCFE;
	hash[3]   = 0x10325476;
	hash[4]   = 0xC3D2E1F0;
	
	return 0;
}

int Sha1::exit(U8 result[len])
{
	pad();
	/* message may be sensitive, clear it out */
	memset(block,0,sizeof(block));
	dataLength=0;
	
	for(int i=0; i<len; i++)
	{
		result[i] = hash[i>>2]>> (8*(3-(i&0x03)));
	}
	
	return 0;
}

int Sha1::update(Buffer b)
{
	while(b.len--)
	{
		block[index++]=*b.ptr++;
		
		dataLength+=8;
		if(index==64)
		{
			process();
		}
	}
	return 0;
}

#define SHA1CircularShift(bits,word) \
	(((word) << (bits)) | ((word) >> (32-(bits))))

void Sha1::process()
{
	const U32 K[] =
	{
		0x5A827999,
		0x6ED9EBA1,
		0x8F1BBCDC,
		0xCA62C1D6
	};
	U32 W[80];
	U32 A, B, C, D, E;
	
	for(int t = 0; t < 16; t++)
	{
		W[t]  = block[t * 4 + 0] << 24;
		W[t] |= block[t * 4 + 1] << 16;
		W[t] |= block[t * 4 + 2] <<  8;
		W[t] |= block[t * 4 + 3] <<  0;
	}
	
	for(int t = 16; t < 80; t++)
	{
		W[t] = SHA1CircularShift(1,W[t-3] ^ W[t-8] ^ W[t-14] ^ W[t-16]);
	}
	
	A = hash[0];
	B = hash[1];
	C = hash[2];
	D = hash[3];
	E = hash[4];
	
	for(int t = 0; t < 20; t++)
	{
		U32 temp =  SHA1CircularShift(5,A) +
				((B & C) | ((~B) & D)) + E + W[t] + K[0];
		E = D;
		D = C;
		C = SHA1CircularShift(30,B);
		B = A;
		A = temp;
	}

	for(int t = 20; t < 40; t++)
	{
		U32 temp = SHA1CircularShift(5,A) + (B ^ C ^ D) + E + W[t] + K[1];
		E = D;
		D = C;
		C = SHA1CircularShift(30,B);
		B = A;
		A = temp;
	}
	
	for(int t = 40; t < 60; t++)
	{
		U32 temp = SHA1CircularShift(5,A) +
			   ((B & C) | (B & D) | (C & D)) + E + W[t] + K[2];
		E = D;
		D = C;
		C = SHA1CircularShift(30,B);
		B = A;
		A = temp;
	}
	
	for(int t = 60; t < 80; t++)
	{
		U32 temp = SHA1CircularShift(5,A) + (B ^ C ^ D) + E + W[t] + K[3];
		E = D;
		D = C;
		C = SHA1CircularShift(30,B);
		B = A;
		A = temp;
	}
	
	hash[0] += A;
	hash[1] += B;
	hash[2] += C;
	hash[3] += D;
	hash[4] += E;
	
	index = 0;
}

void Sha1::pad()
{
	block[index++] = 0x80;
	if(index > 56)
	{
		while(index < 64)
		{
			block[index++] = 0;
		}
		
		process();
	}
	while(index < 56)
	{
		block[index++] = 0;
	}
	
	block[56] = dataLength>> 56;
	block[57] = dataLength>> 48;
	block[58] = dataLength>> 40;
	block[59] = dataLength>> 32;
	block[60] = dataLength>> 24;
	block[61] = dataLength>> 16;
	block[62] = dataLength>>  8;
	block[63] = dataLength>>  0;
	
	process();
}

#endif
