// Copyright 2022 Thierry LARMOIRE
// SPDX short identifier: MIT
#include "httpClient.h"
#include <unistd.h>
#include <errno.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>

void HttpClient::onConnect()
{
	headWait();
}

void HttpClient::headWait()
{
	response.init(responseSpace);
	got=0;
	contentLength=~0U;
	timeout=nothread->scheduleDelayedTask(5*1000*1000,headFail_,this);
	nothread->setBackgroundHandling(fd,NOTHREAD_READABLE,head_,this);
}

void HttpClient::onDisconnect()
{
	//ldebug("\n");
	timeout=nothread->scheduleDelayedTask(10*1000*1000,retry_,this);
}

void HttpClient::onHeader()
{
}

void HttpClient::onData(Buffer line)
{
}

NOTHREAD_TIMEOUT_BODY(HttpClient,retry)
{
	connect();
}

NOTHREAD_HANDLER_BODY(HttpClient,head)
{
	if(!response.readable(fd))
	{
		nothread->unscheduleDelayedTask(timeout);
		timeout=0;
		nothread->disableBackgroundHandling(fd);
		failure();
		return;
	}
	if(response.findHead())
	{
		nothread->unscheduleDelayedTask(timeout);
		timeout=0;
		nothread->disableBackgroundHandling(fd);
		for(Http::Header::Arg *a=response.options.begin();a<response.options.end();a++)
		{
			//ldebug("o " BUFFER_FMT "=" BUFFER_FMT "\n",BUFFER_ARG(a->name),BUFFER_ARG(a->value));
			if(a->name=="Content-Length")
			{
				contentLength=a->value.scanLong();
				//ldebug("%d\n",poll.contentLength);
			}
		}
		onHeader();
		dataWait();
		return;
	}
}

void HttpClient::dataWait()
{
	//timeout=nothread->scheduleDelayedTask(10*1000*1000,dataFail_,this);
	nothread->setBackgroundHandling(fd,NOTHREAD_READABLE,data_,this);
	content.init(Buffer(response.data.ptr,response.data.len+response.remain.len));
	content.data.len=response.data.len;
	data();
}

NOTHREAD_TIMEOUT_BODY(HttpClient,headFail)
{
	ldebug("\n");
	timeout=0;
	nothread->disableBackgroundHandling(fd);
	failure();
}

void HttpClient::dataEnd()
{
	if(timeout)
	{
		nothread->unscheduleDelayedTask(timeout);
		timeout=0;
	}
	nothread->disableBackgroundHandling(fd);
}

NOTHREAD_HANDLER_BODY(HttpClient,data)
{
	if(!content.readable(fd))
	{
		dataEnd();
		failure();
		return;
	}
	while(1)
	{
		Buffer line;
		if(!content.getFullLine(&line))
			break;
		
		got+=line.len;
		onData(line);
	}
	content.forget();
	if(got+content.data.len>=contentLength)
	{
		got+=content.data.len;
		onData(content.data);
	}
	if(got>=contentLength)
	{
		dataEnd();
		onData(Buffer::null);
	}
}


NOTHREAD_TIMEOUT_BODY(HttpClient,dataFail)
{
	ldebug("\n");
	timeout=0;
	nothread->disableBackgroundHandling(fd);
	failure();
}

