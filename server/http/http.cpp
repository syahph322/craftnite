// Copyright 2022 Thierry LARMOIRE
// SPDX short identifier: MIT
#include "http.h"
#include "unistd.h"
#include "errno.h"
#include <stdlib.h>

void Http::unpercent(Buffer &buffer)
{
	Buffer src=buffer;
	Buffer dst=buffer;
	
	while(src.len && dst.len)
	{
		if(*src.ptr=='%' && src.len>=3)
		{
			*dst.ptr=src.slice(1,3).scanU32(16);
			src.ptr+=3;
			src.len-=3;
		}
		else
		{
			*dst.ptr=*src.ptr;
			src.ptr++;
			src.len--;
		}
		dst.ptr++;
		dst.len--;
	}
	buffer.len-=dst.len;
}

int Http::encode(Buffer *to,Buffer from,U32 *escapeds)
{
	static const char*hexs="0123456789abcdef";
	int size=0;
	while(from.len)
	{
		U8 c;
		c=*from.ptr++;
		from.len--;
		if(escapeds[c>>5]&(1<<(c&31)))
		{
			if(to->len>=3)
			{
				*to->ptr++='%';
				*to->ptr++=(U8)hexs[(c>>4)&0xf];
				*to->ptr++=(U8)hexs[(c>>0)&0xf];
				to->len-=3;
			}
			size+=3;
		}
		else
		{
			if(to->len>=1)
			{
				*to->ptr++=c;
				to->len--;
			}
			size+=1;
		}
	}
	return size;
}

int Http::escape(Buffer *to,Buffer from)
{
	U32 escapeds[8]=
	{
		0xffffffff ,
		0xfc0013ff ,
		0x78000000 ,
		0xf8000001 ,
		0xffffffff ,
		0xffffffff ,
		0xffffffff ,
		0xffffffff ,
	};
	return encode(to,from,escapeds);
}

int Http::encodeUri(Buffer *to,Buffer from)
{
	U32 escapeds[8]=
	{
		0xffffffff ,
		0x50000025 ,
		0x78000000 ,
		0xb8000001 ,
		0xffffffff ,
		0xffffffff ,
		0xffffffff ,
		0xffffffff ,
	};
	return encode(to,from,escapeds);
}

int Http::encodeUriComponent(Buffer *to,Buffer from)
{
	U32 escapeds[8]=
	{
		0xffffffff ,
		0xfc00987d ,
		0x78000001 ,
		0xb8000001 ,
		0xffffffff ,
		0xffffffff ,
		0xffffffff ,
		0xffffffff ,
	};
	return encode(to,from,escapeds);
}

Http::Uri& Http::Uri::kv(const char *k,const char *v)
{
	static const char*hexs="0123456789abcdef";
	U32 escapeds[8]=
	{
		0xffffffff ,
		0xfc00987d ,
		0x78000001 ,
		0xb8000001 ,
		0xffffffff ,
		0xffffffff ,
		0xffffffff ,
		0xffffffff ,
	};
	BufferCString h(head),e("=");
	Size l=0;
	l+=h.len;
	for(const char *s=k;char c=*s;s++)
		l+= (escapeds[c>>5]&(1<<(c&31))) ? 3:1;
	l+=e.len;
	for(const char *s=v;char c=*s;s++)
		l+= (escapeds[c>>5]&(1<<(c&31))) ? 3:1;
	
	U8* p=(U8*)malloc(len+l+1);
	if(!p)
		return *this;
	
	memcpy(p,ptr,len);
	
	U8 *d=p+len;
	for(U8 *s=h.ptr,c;(c=*s);s++)
		*d++=c;
	for(U8 *s=(U8*)k,c;(c=*s);s++)
	{
		if(escapeds[c>>5]&(1<<(c&31)))
		{
			*d++='%';
			*d++=(U8)hexs[(c>>4)&0xf];
			*d++=(U8)hexs[(c>>0)&0xf];
		}
		else
			*d++=c;
	}
	for(U8 *s=e.ptr,c;(c=*s);s++)
		*d++=c;
	for(U8 *s=(U8*)v,c;(c=*s);s++)
	{
		if(escapeds[c>>5]&(1<<(c&31)))
		{
			*d++='%';
			*d++=(U8)hexs[(c>>4)&0xf];
			*d++=(U8)hexs[(c>>0)&0xf];
		}
		else
			*d++=c;
	}
	*d++='\0';

	free(ptr);
	ptr=p;
	len+=l;
	if(h.len==0)
		head="&";
	return *this;
}

Http::Uri& Http::Uri::kv(const char *k,long v)
{
	const int len=64;
	char string[len+1];
	snprintf(string,len,"%ld",v);
	string[len]='\0';
	
	return kv(k,string);
}

void Http::Header::init(Buffer space)
{
	File::init(space);
	first=Buffer::null;
	options.init();
}


bool Http::Header::findHead()
{
	Buffer line;
	while(getLine(&line))
	{
		//debug("%3d " BUFFER_FMT "\n",line.len,BUFFER_ARG(line));
		if(line.len==0)
		{
			return true;
		}
		if(!first.ptr)
		{
			first=line;
		}
		else
		{
			Arg *option=options.alloc();
			if(option)
			{
				Buffer n,v;
				v=line;
				v.splitNext(':',&n);
				option->value=v;
				option->name=n;
				option->name.trim();
				option->value.trim();
				option->name.terminate();
				option->value.terminate();
			}
		}
	}
	return false;
}

void Http::Request::init(Buffer space)
{
	Header::init(space);
	method=path=version=Buffer::null;
	args.init();
}

bool Http::Request::findHead()
{
	if(!Header::findHead())
		return false;
	
	version=first;
	version.splitNext(' ',&method);
	version.splitNext(' ',&query);
	query.splitNext('?',&path);
	Buffer qs=query,q;
	while(qs.splitNext('&',&q))
	{
		Arg *arg=args.alloc();
		if(arg)
		{
			Size p;
			p=q.search('=');
			if(p==Buffer::notFound)
			{
				arg->name=q;
				arg->value=Buffer::null;
			}
			else
			{
				arg->name=q.slice(0,p);
				arg->value=q.slice(p+1);
				unpercent(arg->value);
				arg->value.terminate();
			}
			unpercent(arg->name);
			arg->name.terminate();
			//debug("%.*s,%.*s\n",BUFFER_ARG(arg->name),BUFFER_ARG(arg->value));
		}
	}
	//debug(BUFFER_FMT "," BUFFER_FMT "," BUFFER_FMT "," BUFFER_FMT "\n",
	//	BUFFER_ARG(method),BUFFER_ARG(path),BUFFER_ARG(query),BUFFER_ARG(version));
	return true;
}

void Http::Response::init(Buffer space)
{
	Header::init(space);
	code=status=Buffer::null;
}

bool Http::Response::findHead()
{
	if(!Header::findHead())
		return false;
	
	status=first;
	status.splitNext(' ',&code);
	//debug(BUFFER_FMT "," BUFFER_FMT "\n",
	//	BUFFER_ARG(code),BUFFER_ARG(status));
	return true;
}
