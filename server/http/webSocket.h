// Copyright 2022 Thierry LARMOIRE
// SPDX short identifier: MIT
#ifndef __webSocket_h__
#define __webSocket_h__

#ifndef __httpServer_h__
#include "httpServer.h"
#endif

struct WebSocket
{
	struct Header
	{
		U8 opcode:4;
		U8 rsvd:3;
		U8 fin:1;
		U8 len:7;
		U8 mask:1;
		U8 lens[8];
	};
	
	WebSocket();
	virtual ~WebSocket();
	int init(HttpServer::Client *client);
	int exit();
	int unmask(Buffer data);
	
	HttpServer::Client *client;
	Header *header;
	U32 mask;
	Size got;
	BufferMalloced buffer;
	Buffer data;
	
	NOTHREAD_HANDLER_DECL(ondata);
	void onframe(Buffer buffer);
	virtual void ondata(Buffer buffer)=0;
	int send(Buffer buffer);
	
	typedef HttpServer::Client Client;
	THEN_DECL(graded);
	virtual void graded(bool up);
};


#endif /* __webSocket_h__ */
