// Copyright 2022 Thierry LARMOIRE
// SPDX short identifier: MIT
#ifndef   __http_h__
#define   __http_h__

#include "nothread/buffer.h"
#include "nothread/file.h"

struct Http
{
	static void unpercent(Buffer &buffer);
	static int encode(Buffer *to,Buffer from,U32 *escapeds);
	static int escape(Buffer *to,Buffer from);
	static int encodeUri(Buffer *to,Buffer from);
	static int encodeUriComponent(Buffer *to,Buffer from);

	struct Header : public File
	{
		Buffer first;
		struct Arg
		{
			BufferCString name;
			BufferCString value;
			operator BufferCString*()
			{
				return &value;
			}
		};
		struct Args
		{
			Arg array[512];
			size_t count;
			inline void init()
			{
				count=0;
			}
			inline Arg *alloc()
			{
				if(count<N_ELEMENT(array))
					return &array[count++];
				return NULL;
			}
			inline Arg *find(const Buffer &name)
			{
				for(size_t i=0;i<count;i++)
				{
					if(array[i].name==name)
						return &array[i];
				}
				return NULL;
			}
			inline Arg *find(const char*name)
			{
				return find(Buffer(name));
			}
			inline Buffer value(const char*name)
			{
				Arg *arg;
				arg=find(name);
				return arg ? arg->value : Buffer::null;
			}
			inline Arg*begin()
			{
				return &array[0];
			}
			inline Arg*end()
			{
				return &array[count];
			}
		};
		Args options;
		void init(Buffer space);
		bool findHead();
	};
	struct Request : public Header
	{
		Buffer method,path,query,version;
		Args args;
		void init(Buffer space);
		bool findHead();
	};
	struct Response : public Header
	{
		Buffer code,status;
		void init(Buffer space);
		bool findHead();
	};
	struct Uri : public BufferMallocedCString
	{
		const char *head;
		inline Uri(const char *head="")
		{
			this->head=head;
		}
		Uri& kv(const char *k,const char *v);
		Uri& kv(const char *k,long v);
	};
	
};

#endif /* __http_h__ */
