// Copyright 2022 Thierry LARMOIRE
// SPDX short identifier: MIT
#ifndef __httpServer_h__
#define __httpServer_h__

#include "nothread/debug.h"
#include "nothread/nothread.h"
#include "nothread/list.h"
#include "nothread/buffer.h"
#include "nothread/ntp.h"
#include <stddef.h>

#define CRLF "\r\n"

struct Config;

struct HttpServer
{
#ifdef HTTP_WITH_LOGIN
	struct Login : public Node<Login>
	{
		BufferMallocedCString user;
		BufferMallocedCString pass;
		int rights;
	};
	List<Login> logins;
	struct Session : public Node<Session>
	{
		struct
		{
			inline operator size_t()
			{
				return (size_t)this;
			}
		}id;
		time_t expiration;
		BufferMallocedCString user;
		BufferMallocedCString pass;
	};
	List<Session> sessions;
#endif
	struct Client : public Node<Client>
	{
		virtual ~Client(){};
		virtual int init(HttpServer *server,int fd);
		int init();
		virtual int exit();
		static int sockaddrToString(BufferMallocedCString *ip_port,const struct sockaddr *addr, socklen_t addrlen);
		int getxxxxname(BufferMallocedCString *ip_port,int (fn)(int sockfd, struct sockaddr *addr, socklen_t *addrlen));
		int getpeername(BufferMallocedCString *ip_port);
		int getsockname(BufferMallocedCString *ip_port);
		struct Request
		{
			BufferAlloced<16*1024> buffer;
			Buffer header;
			BufferCString method,path,query,version;
			struct Arg
			{
				BufferCString name;
				BufferCString value;
				operator BufferCString*()
				{
					return &value;
				}
			};
			struct Args
			{
				Arg array[512];
				size_t count;
				inline void init()
				{
					count=0;
				}
				inline Arg *alloc()
				{
					if(count<N_ELEMENT(array))
						return &array[count++];
					return NULL;
				}
				inline Arg *find(const Buffer &name)
				{
					for(size_t i=0;i<count;i++)
					{
						if(array[i].name==name)
							return &array[i];
					}
					return NULL;
				}
				inline Arg *find(const char*name)
				{
					return find(Buffer(name));
				}
				inline Buffer value(const char*name)
				{
					Arg *arg;
					arg=find(name);
					return arg ? arg->value : Buffer::null;
				}
			};
			Args options;
			Args args;
			BufferMalloced data;
			Buffer remain;
			void init();
			int findHead();
			int parse();
		}request;
		struct Response
		{
			int fd;
			size_t todo;
			struct Then
			{
				void *that;
				int (*function)(void *that,Client *client,int closed);
			}then;
			#define THEN_DECL(name) \
				static int name ## _(void *that,HttpServer::Client *client,int closed); \
				int name (Client *client,int closed); \
				
			#define THEN_BODY(klass,name) \
				int klass :: name ## _ (void *that,HttpServer::Client *client,int closed) \
				{ \
					return ((klass *)that)->name ((klass :: Client*)client,closed); \
				} \
				int klass :: name  (Client *client,int closed) \
			
			BufferAlloced<1024*1024> buffer;
			Buffer head;
			void init();
		}response;
		
		int  handle();
		int  handleError(int error,const char*string);
		int  handleFile(Buffer root);
		int  handleFile(Buffer root, Buffer dirroot);
		int  handleFile(BufferCString filename,struct stat *stat);
		int  handleDir(Buffer root,BufferCString filename,struct stat *stat);
		int  handleList();
		
		void waitRequest();
		void bye();
		
#ifdef HTTP_WITH_FILE_INSTALL
		int  handleFileGet(BufferMallocedCString &intern_name,BufferMallocedCString &extern_name);
		int  handleFileSet(BufferMallocedCString &intern_name);
#endif
		
#ifdef HTTP_WITH_LOGIN
		int basicAuthorization(BufferMallocedCString *user,BufferMallocedCString *pass);
		HttpServer::Session* session();
		int session(BufferMallocedCString *user,BufferMallocedCString *pass);
		int check(const char*prompt,int rights);
		int authorizationRequired(const char *prompt);
#endif
		
		BufferMallocedCString peer;
		int fd;
		int inData;
#ifdef HTTP_WITH_SSL
		void *ssl, *io;
#endif
		void *spare[2];
		
		HttpServer *server;
		//int write(Buffer buffer,void (*after)(void *),void that);
		NOTHREAD_HANDLER_DECL(onRead);
		NOTHREAD_HANDLER_DECL(onReadData);
		NOTHREAD_HANDLER_DECL(onWrite);
		NOTHREAD_HANDLER_DECL(onFileRead);
		ssize_t read(void *data, size_t len);
		ssize_t write(const void *data, size_t len);
		inline void *getCtx(size_t size)
		{
			Size align=request.remain.len&(sizeof(long)-1);
			if(align)
				request.remain.consume(sizeof(long)-align);
			if(size>request.remain.len)
				return NULL;
			void *ptr=request.remain.ptr;
			request.remain.consume(size);
			return ptr;
		}
		
		FILE *fopenChunked();
		FILE *fopenChunked(const char*mime);
		
		struct // nothread is in server->nothread
		{
			char empty[0];
			inline Nothread* operator->()
			{
				HttpServer::Client c[0];
				return ((Client*)(empty-((long)&c->nothread-(long)&c)))->server->nothread;
			}
		}nothread;
	};
	
	struct Url : public Node<Url>
	{
		const char *method;
		const char *path;
		void *that;
		int (*onUrl)(void *that,Client *client);
		inline Url(const char *method, const char*path, void *that, int (*onUrl)(void *that,Client *client))
		{
			this->method=method;
			this->path=path;
			this->that=that;
			this->onUrl=onUrl;
		}
	};
	
	int init(Nothread* nothread, Config *config, int port, const char *name, const char* pem=NULL);
	int init(Nothread* nothread, Config *config, const char *ip, int port, const char *name, const char* pem=NULL);
	int exit();
	virtual Client *createClient();
	
	Nothread* nothread;
	int port;
	int debugLevel;
	int fd;
#ifdef HTTP_WITH_SSL
	BufferMallocedCString pem;
	void *sslCtx;
#endif
	List<Client> clients;
	List<Url> urls;
	
	NOTHREAD_HANDLER_DECL(onClient);
	
#define URL_DECL(name) \
	static int name ## ___(void *that,HttpServer::Client *client); \
	int name ## __(Client *client);
	
#define URL_BODY(klass,name) \
	int klass :: name ## ___ (void *that,HttpServer::Client *client) \
	{ \
		return ((klass *)that)->name ## __((klass :: Client*)client); \
	} \
	int klass :: name ## __ (Client *client) \
	
#define URL_INIT(server,that_,name,method_,path_) \
	(server)->urls.queue(new HttpServer::Url(method_, path_, that_, &name ## ___))
	
	URL_DECL(login);
	URL_DECL(logout);
	URL_DECL(testArgs);
#ifdef HTTP_WITH_PAM
	URL_DECL(pamCheck);
	URL_DECL(pamChange);
#endif
};

#endif /* __httpServer_h__ */
