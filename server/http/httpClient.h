// Copyright 2022 Thierry LARMOIRE
// SPDX short identifier: MIT
#ifndef __httpClient_h__
#define __httpClient_h__

#include "nothread/tcpClient.h"
#include "nothread/file.h"
#include "http.h"

struct HttpClient : public TcpClient
{
	virtual void onConnect();
	virtual void onDisconnect();
	virtual void onHeader();
	virtual void onData(Buffer line);
	void headWait();
	void dataWait();
	void dataEnd();
	NOTHREAD_HANDLER_DECL(head);
	NOTHREAD_TIMEOUT_DECL(headFail);
	NOTHREAD_HANDLER_DECL(data);
	NOTHREAD_TIMEOUT_DECL(dataFail);
	NOTHREAD_TIMEOUT_DECL(retry);
	
	BufferAlloced<4*1024> responseSpace;
	USize got,contentLength;
	Buffer dataSpace;
	Http::Response response;
	File content;
};

#endif /* __httpClient_h__ */
