// Copyright 2022 Thierry LARMOIRE
// SPDX short identifier: MIT
#include "config.h"
#include <sys/stat.h>
#include <utime.h>
#include <regex.h>

int Config::init(Nothread *nothread,HttpServer *httpServer,const char*filename)
{
	this->nothread=nothread;
	this->httpServer=httpServer;
	this->filename=filename;
	
	Vars::init();
	
	load();
	
	if(httpServer)
	{
		URL_INIT(httpServer,this,values,"GET",NULL);
		URL_INIT(httpServer,this,types,"GET","/types");
		URL_INIT(httpServer,this,events,"GET","/events");
	}
	
	save_on_flash=1;
	add(&save_on_flash,"enum:[0,1],man:\"if not set,values will be losts\"","save_on_flash");
	return 0;
}

int Config::exit()
{
	return Vars::exit();
}

int Config::load(const char*filename)
{
	if(!filename)
		filename=this->filename.cString();
	return Vars::load(filename);
}

int Config::save(const char*filename)
{
	if(!filename)
		filename=this->filename.cString();
	return Vars::save(filename);
}

time_t Config::touch(const char*filename)
{
	if(!filename)
		filename=this->filename.cString();
	
	struct stat s;
	lstat(filename,&s);
	utime(filename,NULL);
	return s.st_mtime;
}

URL_BODY(Config,values)
{
	const char *req=NULL;
	const char *get="/params/get.cgi";
	const char *set="/params/set.cgi";
	const char *values="/values";
	const char *config="/config";
	const char *status="/status";
	
	//ldebug(BUFFER_FMT ".\n",BUFFER_ARG(client->request.path));
	if(client->request.path==values) req=values; else
	if(client->request.path==config) req=config; else
	if(client->request.path==status) req=status; else
	if(client->request.path==get   ) req=values; else
	if(client->request.path==set   ) req=values; else
		return -1;
	
	typeof(client->response.buffer) *buffer=&client->response.buffer;
	buffer->raz();
	Buffer head(buffer->ptr,256);
	buffer->consume(head.len);
	
	int len=0;
	int modified=0;
	for(size_t i=0;i<client->request.args.count;i++)
	{
		HttpServer::Client::Request::Arg *arg=client->request.args.array+i;
		regex_t re[1];
		char re_string[128];
		//ldebug(BUFFER_FMT "\n",BUFFER_ARG(arg->name));
		snprintf(re_string,sizeof(re_string),"^" BUFFER_FMT "$",BUFFER_ARG(arg->name));
		regcomp(re,re_string,REG_EXTENDED|REG_NOSUB);
		foreach_(Var,var,*this)
		{
			if(!var->type)
				continue;
			if( var->readOnly && req==config)
				continue;
			if(!var->readOnly && req==status)
				continue;
			if(arg->name.len!=0 && 0!=regexec(re,var->name.cString(),0,NULL,0))
				continue;
			if(!arg->value.isNull())
			{
				*var=arg->value;
				modified++;
			}
			Buffer value=(Buffer)*var;
			len+=buffer->consume(BUFFER_FMT "=" BUFFER_FMT "\n",BUFFER_ARG(var->name),BUFFER_ARG(value));
		}
		regfree(re);
	}
	if(modified && save_on_flash)
		save();
	
	head.consume(
		"HTTP/1.0 200 OK" CRLF
		"Cache-control: no-cache" CRLF
		"Pragma: no-cache" CRLF
		"Content-Type: text/plain; charset=utf-8" CRLF
		"Content-Length: %d" CRLF
		,len
	);
	head.consume("Padding: ");
	while(head.len>4)
	{
		head.consume("-");
	}
	memcpy(head.ptr,CRLF CRLF,4);
	head.consume(4);
	
	buffer->consumed();
	
	client->onWrite();
	return 0;
}

URL_BODY(Config,types)
{
	typeof(client->response.buffer) *buffer=&client->response.buffer;
	buffer->raz();
	Buffer head(buffer->ptr,128);
	buffer->consume(head.len);
	
	int len=0;
	for(size_t i=0;i<client->request.args.count;i++)
	{
		HttpServer::Client::Request::Arg *arg=client->request.args.array+i;
		regex_t re[1];
		char re_string[128];
		//ldebug(BUFFER_FMT "\n",BUFFER_ARG(arg->name));
		snprintf(re_string,sizeof(re_string),"^" BUFFER_FMT "$",BUFFER_ARG(arg->name));
		regcomp(re,re_string,REG_EXTENDED|REG_NOSUB);
		len+=buffer->consume("{\n");
		foreach_(Var,var,*this)
		{
			if(!var->type)
				continue;
			if(arg->name.len!=0 && 0!=regexec(re,var->name.cString(),0,NULL,0))
				continue;
			Buffer value=(Buffer)*var;
			len+=buffer->consume("\"" BUFFER_FMT "\":{type:type_" BUFFER_FMT ",readOnly:%d,value:\"" BUFFER_FMT "\"%s%s},\n"
				,BUFFER_ARG(var->name),BUFFER_ARG(var->type->name),var->readOnly,BUFFER_ARG(value)
				,var->man[0]?",":"",var->man
			);
		}
		len+=buffer->consume("}\n");
		regfree(re);
	}
	
	head.consume(
		"HTTP/1.0 200 OK" CRLF
		"Cache-control: no-cache" CRLF
		"Pragma: no-cache" CRLF
		"Content-Type: text/plain" CRLF
		"Content-Length: %d" CRLF
		,len
	);
	head.consume("Padding: ");
	while(head.len>4)
	{
		head.consume("-");
	}
	memcpy(head.ptr,CRLF CRLF,4);
	head.consume(4);
	
	buffer->consumed();
	
	client->onWrite();
	return 0;
}

struct EventsContext
{
	typedef HttpServer::Client Client;
	Client *client;
	int listenersCount;
	struct Listener : public Var::Listener
	{
		Ntp seen;
		Var *var;
	};
	Listener listeners[0];
	VAR_LISTENER_DECL(onChange);
	THEN_DECL(httpClientWait);
	inline void init(Client *client)
	{
		this->client=client;
		listenersCount=0;
	}
};

VAR_LISTENER_BODY(EventsContext,onChange)
{
	//Buffer value=*var;
	//ldebug(BUFFER_FMT "=" BUFFER_FMT "\n",BUFFER_ARG(var->name),BUFFER_ARG(value));
	if(!var)
	{
		((Listener*)listener)->var=NULL;
		return 0;
	}
	if(client)
		httpClientWait(client,0);
	return 0;
}

THEN_BODY(EventsContext,httpClientWait)
{
	if(closed)
	{
		for(int i=0;i<listenersCount;i++)
		{
			listeners[i].remove();
		}
		return 0;
	}
	
	client->response.buffer.raz();
	for(int i=0;i<listenersCount;i++)
	{
		if(!listeners[i].var)
			continue;
		if(listeners[i].var->ntp<=listeners[i].seen)
			continue;
		listeners[i].seen=listeners[i].var->ntp;
		Buffer value=*listeners[i].var;
		client->response.buffer.consume(
			"ntp=%lf\n"
			BUFFER_FMT "=" BUFFER_FMT "\n"
			,(double)listeners[i].var->ntp,BUFFER_ARG(listeners[i].var->name),BUFFER_ARG(value)
		);
	}
	client->response.buffer.consumed();
	
	if(!client->response.buffer.len)
	{
		this->client=client;
		return 0;
	}
	
	this->client=NULL;
	client->onWrite();
	return 0;
}

URL_BODY(Config,events)
{
	EventsContext *context=(EventsContext*)align(client->request.header.end(),sizeof(U32));
	if((U8*)(context+1)>&client->request.buffer.buf[client->request.buffer.size])
	{
		ldebug("no place for context\n");
		client->bye();
		return 0;
	}
	context->init(client);
	
	for(size_t i=0;i<client->request.args.count;i++)
	{
		HttpServer::Client::Request::Arg *arg=client->request.args.array+i;
		regex_t re[1];
		char re_string[128];
		//ldebug(BUFFER_FMT "\n",BUFFER_ARG(arg->name));
		snprintf(re_string,sizeof(re_string),"^" BUFFER_FMT "$",BUFFER_ARG(arg->name));
		regcomp(re,re_string,REG_EXTENDED|REG_NOSUB);
		foreach(Var,var,*this)
		{
			if(!var->type)
				continue;
			if(arg->name.len!=0 && 0!=regexec(re,var->name.cString(),0,NULL,0))
				continue;
			
			EventsContext::Listener *listener=&context->listeners[context->listenersCount++];
			if((U8*)(listener+1)>&client->request.buffer.buf[client->request.buffer.size])
			{
				ldebug("no more place for listener\n");
				break;
			}
			listener->seen=Ntp::zero;
			listener->var=var;
			listener->that=context;
			listener->onEvent=EventsContext::onChange_;
			var->listeners.add(listener);
		}
		regfree(re);
	}
	
	client->response.buffer.consume(
		"HTTP/1.0 200 OK" CRLF
		"Cache-control: no-cache" CRLF
		"Pragma: no-cache" CRLF
		"Connection: close" CRLF
		"Content-Type: text/plain" CRLF
		CRLF
	);
	client->response.buffer.consumed();
	client->response.then.that=context;
	client->response.then.function=&EventsContext::httpClientWait_;
	client->onWrite();
	client=NULL;
	return 0;
}
