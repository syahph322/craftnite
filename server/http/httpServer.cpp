// Copyright 2022 Thierry LARMOIRE
// SPDX short identifier: MIT
#include "httpServer.h"
#include "config.h"
#ifdef HTTP_WITH_PAM
#include "pam.h"
#endif

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <dirent.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <sys/fcntl.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <net/if.h>
#include <arpa/inet.h>
#include <netinet/tcp.h>
#include <sys/stat.h>
#include <zlib.h>
#ifdef HTTP_WITH_SSL
#include <openssl/ssl.h>
#endif
#include <syslog.h>


int HttpServer::init(Nothread* nothread, Config *config, int port, const char *name, const char* pem)
{
	return init(nothread,config,NULL,port,name,pem);
}

int HttpServer::init(Nothread* nothread, Config *config, const char *ip, int port, const char *name, const char* pem)
{
	this->nothread=nothread;
	
	this->port=port;
#ifdef HTTP_WITH_SSL
	this->pem=pem?pem:"";
	sslCtx=NULL;
#endif
	
	debugLevel=0;
	if(char*s=getenv("nothread_http_debug"))
		debugLevel=atoi(s);
	
	if(config)
	{
		config->add(&this->port,"min:0,max:65535","%s.port",name);
#ifdef HTTP_WITH_SSL
		config->add(&this->pem,"man:\"path to server-chain.pem\"","%s.pem",name);
#endif
		config->add(&this->debugLevel,"min:0,man:\"debug level\"","%s.debugLevel",name);
	}
	
	struct sockaddr_in sockaddr_in ;
	fd = socket (AF_INET, SOCK_STREAM|SOCK_NONBLOCK|SOCK_CLOEXEC, 0);
	if (fd == -1)
	{
		ldebug("!socket\n");
		return -1;
	}
	
	int opt = 1 ;
	if (setsockopt (fd, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof (opt)) < 0)
	{
		ldebug("!setsockopt\n");
		close (fd);
		return -1;
	}
	
	memset (&sockaddr_in, 0x0, sizeof(sockaddr_in));
	sockaddr_in.sin_family = AF_INET;
	sockaddr_in.sin_port = htons(this->port);
	if(!ip || 0!=inet_pton(AF_INET, ip, &sockaddr_in.sin_addr))
		sockaddr_in.sin_addr.s_addr = htonl(INADDR_ANY);
	
	if (bind (fd, (struct sockaddr *)&sockaddr_in, sizeof(sockaddr_in)) == -1)
	{
		ldebug("!bind %d\n",this->port);
		close (fd);
		return -1;
	}
		
	if (listen (fd, 64) == -1)
	{
		ldebug("!listen\n");
		close (fd);
		return -1;
	}
	
#ifdef HTTP_WITH_SSL
	if(this->pem!="")
	{
		static int ssl_init=0;
		if(!ssl_init)
			SSL_library_init();
		ssl_init++;
		
		sslCtx=SSL_CTX_new(TLS_server_method());
		
		SSL_CTX_set_min_proto_version((SSL_CTX*)sslCtx,TLS1_2_VERSION);
		
		if(!SSL_CTX_use_certificate_chain_file((SSL_CTX*)sslCtx,this->pem.cString()))
		{
			ldebug("!SSL_CTX_use_certificate_chain_file %s %d\n",this->pem.cString(),errno);
		}
		if(!SSL_CTX_use_PrivateKey_file((SSL_CTX*)sslCtx,this->pem.cString(),SSL_FILETYPE_PEM))
		{
			ldebug("!SSL_CTX_use_PrivateKey_file %s %d\n",this->pem.cString(),errno);
		}
	}
#endif
	
	URL_INIT(this,this,testArgs,"GET","/testArgs");
	
	nothread->setBackgroundHandling(fd,NOTHREAD_READABLE,onClient_,this);
	
	{
		sigset_t set;
		int ret;
		ret=sigemptyset(&set);
		if (ret < 0)
		{
			ldebug("!sigemptyset\n");
			close (fd);
			return -1;
		}
		ret=sigaddset(&set,SIGPIPE);
		if (ret < 0)
		{
			ldebug("!sigaddset SIGPIPE\n");
			close (fd);
			return -1;
		}
		ret=pthread_sigmask(SIG_BLOCK, &set, NULL);
		if (ret < 0)
		{
			ldebug("!pthread_sigmask SIGPIPE\n");
			close (fd);
			return -1;
		}
	}
	return 0;
}

int HttpServer::exit()
{
	foreach(Client,client,clients)
	{
		client->bye();
	}
#ifdef HTTP_WITH_SSL
	if(sslCtx)
	{
		SSL_CTX_free((SSL_CTX*)sslCtx);
		sslCtx=NULL;
	}
#endif
	if(fd!=-1)
	{
		nothread->disableBackgroundHandling(fd);
		close(fd);
		fd=-1;
	}
	
	foreach(Url,url,urls)
	{
		url->remove();
		delete url;
	}
	return 0;
}

HttpServer::Client *HttpServer::createClient()
{
	return new Client();
}

NOTHREAD_HANDLER_BODY(HttpServer,onClient)
{
	struct sockaddr_in sockaddr_in ;
	
	memset (&sockaddr_in, 0x0, sizeof(sockaddr_in));
	socklen_t sockaddr_in_len = sizeof (sockaddr_in) ;
	
	int clientFd = accept (fd, (struct sockaddr *) &sockaddr_in, &sockaddr_in_len);
	if(clientFd == -1)
	{
		if(errno==EAGAIN)
			return;
		BufferMallocedCString ip_port;
		HttpServer::Client::sockaddrToString(&ip_port,(struct sockaddr *)&sockaddr_in,sockaddr_in_len);
		ldebug("!accept %d %s\n",errno,ip_port.cString());
		return;
	}
	
	Client *client=createClient();
	clients.queue(client);
	client->init(this,clientFd);
}

int HttpServer::Client::init(HttpServer *server,int fd)
{
	int status;
	
	this->server=server;
	this->fd=fd;
	
	getpeername(&peer);
	syslog(LOG_AUTH|LOG_NOTICE, "connection from \"%s\"", peer.cString());
#ifdef HTTP_WITH_SSL
	if(server->sslCtx)
	{
		void *sbio, *ssl_bio;
		sbio=BIO_new_socket(fd,BIO_NOCLOSE);
		ssl=SSL_new((SSL_CTX*)server->sslCtx);
		SSL_set_bio((SSL*)ssl,(BIO*)sbio,(BIO*)sbio);
		
		status=SSL_accept((SSL*)ssl);
		if(status<=0)
		{
			ldebug("!SSL_accept %d %d %s\n",status,errno,peer.cString());
			io=NULL;
			response.fd=-1;
			response.then.function=NULL;
			bye();
			return -1;
		}
		
		io=BIO_new(BIO_f_buffer());
		ssl_bio=BIO_new(BIO_f_ssl());
		BIO_set_ssl((BIO*)ssl_bio,(SSL*)ssl,BIO_CLOSE);
		BIO_push((BIO*)io,(BIO*)ssl_bio);
	}
	else
	{
		ssl=NULL;
		io=NULL;
	}
#endif
	
	return init();
}

int HttpServer::Client::init()
{
	{
		int flags = O_NONBLOCK;
		int ret;
		ret = fcntl (fd, F_SETFL, flags);
		if (ret < 0)
		{
			ldebug("!fcntl F_SETFL O_NONBLOCK\n");
			close (fd);
			return -1;
		}
	}
	
	{
		int flags = 1;
		int ret;
		ret = setsockopt(fd, IPPROTO_TCP, TCP_NODELAY, &flags, sizeof(flags));
		if (ret < 0)
		{
			ldebug("!setsockopt TCP_NODELAY\n");
			close (fd);
			return -1;
		}
	}
	
	{
		struct linger l;
		l.l_onoff=1;
		l.l_linger=1;
		int ret;
		ret = setsockopt(fd, SOL_SOCKET, SO_LINGER, &l, sizeof(l));
		if (ret < 0)
		{
			ldebug("!setsockopt SO_LINGER\n");
			close (fd);
			return -1;
		}
	}
	
	waitRequest();
	
	return 0;
}

int HttpServer::Client::exit()
{
	syslog(LOG_AUTH|LOG_NOTICE, "disconnection from \"%s\"", peer.cString());
	if(fd!=-1)
	{
		server->nothread->disableBackgroundHandling(fd);
#ifdef HTTP_WITH_SSL
		if(ssl)
		{
			SSL_shutdown((SSL*)ssl);
			if(io)
			{
				BIO_free_all(BIO_pop((BIO*)io));
				BIO_free_all((BIO*)io);
				io=NULL;
			}
			else
				SSL_free((SSL*)ssl);
			ssl=NULL;
		}
#endif
		close(fd);
		fd=-1;
	}
	if(response.fd!=-1)
	{
		close(response.fd);
		response.fd=-1;
	}
	if(response.then.function)
	{
		response.then.function(response.then.that,this,1);
		response.then.function=NULL;
	}
	
	return 0;
}

int HttpServer::Client::sockaddrToString(BufferMallocedCString *ip_port,const struct sockaddr *addr, socklen_t addrlen)
{
	if(!ip_port)
		return 0;
	
	if(0) ;
	else if(addr->sa_family==AF_INET)
	{
		ip_port->format("%d.%d.%d.%d:%d"
			,((uint8_t*)&(*(sockaddr_in*)addr).sin_addr.s_addr)[0]
			,((uint8_t*)&(*(sockaddr_in*)addr).sin_addr.s_addr)[1]
			,((uint8_t*)&(*(sockaddr_in*)addr).sin_addr.s_addr)[2]
			,((uint8_t*)&(*(sockaddr_in*)addr).sin_addr.s_addr)[3]
			,htons((*(sockaddr_in*)addr).sin_port)
		);
	}
	else
		ip_port->format("sa_family=%d",addr->sa_family);
	return 0;
}

int HttpServer::Client::getxxxxname(BufferMallocedCString *ip_port,int (fn)(int sockfd, struct sockaddr *addr, socklen_t *addrlen))
{
	struct sockaddr recvaddr;
	socklen_t recvaddr_size;
	recvaddr_size=sizeof(recvaddr);
	if(0!=fn(fd, &recvaddr, &recvaddr_size))
		return -1;
	
	return sockaddrToString(ip_port,&recvaddr,recvaddr_size);
}

int HttpServer::Client::getpeername(BufferMallocedCString *ip_port)
{
	return getxxxxname(ip_port,::getpeername);
}

int HttpServer::Client::getsockname(BufferMallocedCString *ip_port)
{
	return getxxxxname(ip_port,::getsockname);
}

void HttpServer::Client::waitRequest()
{
	inData=0;
	request.init();
	response.init();
	server->nothread->setBackgroundHandling(fd,NOTHREAD_READABLE,onRead_,this);
}

void HttpServer::Client::bye()
{
	//ldebug("bye %p\n",this);
	exit();
	remove();
	delete(this);
}

inline ssize_t HttpServer::Client::read(void *data, size_t len)
{
	ssize_t done;
#ifdef HTTP_WITH_SSL
	if(io)
		done=BIO_read((BIO*)io,data,len);
	else
#endif
		done=::read(fd,data,len);
	
	if( inData && (server->debugLevel&2) )
	{
		Buffer d(data,done);
		if(d.len)
		{
			ldebug("<< D %zd %s\n", d.len, peer.cString());
			d.slice(0,1024).dump(stderr,1,32);
		}
	}
	return done;
}

ssize_t HttpServer::Client::write(const void *data, size_t len)
{
	ssize_t done;
#ifdef HTTP_WITH_SSL
	if(io)
	{
		done=BIO_write((BIO*)io,data,len);
		if(1!=BIO_flush((BIO*)io))
			done=-1;
	}
	else
#endif
		done=::write(fd,data,len);
	
	if(server->debugLevel&3)
	{
		Buffer h,d((U8*)data,len);
		if(!inData)
		{
			Buffer e(CRLF CRLF);
			Size i=d.search(e);
			if(i!=Buffer::notFound)
			{
				i+=e.len;
				h=d.slice(0,i);
				d=d.slice(i);
				inData=1;
			}
		}
		if(h.len && (server->debugLevel&1))
		{
			_debug(">> H %zd %s\n", h.len, peer.cString());
			Buffer e(CRLF),d(h);
			for(Size i;Buffer::notFound!=(i=d.search(e));)
			{
				_debug("| " BUFFER_FMT "\n", BUFFER_ARG(d.slice(0,i)));
				d=d.slice(i+e.len);
			}
		}
		if(h.len && (server->debugLevel&2))
		{
			_debug(">> H %zd %s\n", h.len, peer.cString());
			h.dump(stderr,1,32);
		}
		if(d.len && (server->debugLevel&4))
		{
			_debug(">> D %zd %s\n", d.len, peer.cString());
			d.slice(0,1024).dump(stderr,1,32);
		}
	}
	return done;
}

struct ChunkedCtx
{
	HttpServer::Client *client;
	FILE *mem;
	Buffer buffer;
	bool chunked;
	
	static cookie_io_functions_t functions;
	static ssize_t _read (void *c, char *buf, size_t size);
	static ssize_t _write(void *c, const char *buf, size_t size);
	static int     _seek (void *c, off64_t *offset, int whence);
	static int     _close(void *c);
	ssize_t         read (char *buf, size_t size);
	ssize_t         write(const char *buf, size_t size);
	int             seek (off64_t *offset, int whence);
	int             close();

	static int done_(void *that,HttpServer::Client *client,int closed);
	int done(HttpServer::Client *client,int closed);
};

ssize_t ChunkedCtx::_read (void *c, char *buf, size_t size)
{
	return ((ChunkedCtx*)c)->read(buf,size);
}

ssize_t ChunkedCtx::_write(void *c, const char *buf, size_t size)
{
	return ((ChunkedCtx*)c)->write(buf,size);
}

int     ChunkedCtx::_seek (void *c, off64_t *offset, int whence)
{
	return ((ChunkedCtx*)c)->seek(offset,whence);
}

int     ChunkedCtx::_close(void *c)
{
	return ((ChunkedCtx*)c)->close();
}

inline ssize_t         ChunkedCtx::read (char *buf, size_t size)
{
	//ldebug("%d %.*s\n",chunked,size,buf);
	return -1;
}

inline ssize_t         ChunkedCtx::write(const char *buf, size_t size)
{
	//ldebug("%d %.*s\n",chunked,size,buf);
	if(chunked)
	{
		fprintf(mem,"%zx" CRLF,size);
	}
	size_t len=fwrite(buf,1,size,mem);
	if(chunked)
	{
		fprintf(mem,CRLF);
	}
	return len;
}

inline int             ChunkedCtx::seek (off64_t *offset, int whence)
{
	//ldebug("%ld %d\n",offset?*offset:0,whence);
	int status=0;
	switch(whence)
	{
		case SEEK_SET:
			chunked=0;
		break;
		case SEEK_END:
			chunked=1;
		break;
		default :
			status=-1;
	}
	return status;
}

inline int             ChunkedCtx::close()
{
	if(chunked)
	{
		fprintf(mem,"0" CRLF CRLF);
	}
	fclose(mem);
	//ldebug("%d %.*s\n",buffer.len,buffer.len,buffer.ptr);
	client->response.buffer.ptr=buffer.ptr;
	client->response.buffer.len=buffer.len;
	client->response.then.that=this;
	client->response.then.function=done_;
	client->onWrite();
	return 0;
}

int ChunkedCtx::done_(void *that,HttpServer::Client *client,int closed)
{
	return ((ChunkedCtx*)that)->done(client,closed);
}

int ChunkedCtx::done(HttpServer::Client *client,int closed)
{
	free(buffer.ptr);
	client->response.buffer.len=0;
	client->waitRequest();
	return 0;
}

cookie_io_functions_t ChunkedCtx::functions=
{
	ChunkedCtx::_read,
	ChunkedCtx::_write,
	ChunkedCtx::_seek,
	ChunkedCtx::_close,
};

FILE* HttpServer::Client::fopenChunked()
{
	ChunkedCtx *ctx=(ChunkedCtx*)getCtx(sizeof(ChunkedCtx));
	if(!ctx)
		return NULL;
	
	ctx->client=this;
	ctx->chunked=false;
	ctx->buffer=Buffer::null;
	ctx->mem=open_memstream ((char**)&ctx->buffer.ptr,&ctx->buffer.len);
	FILE *file=fopencookie(ctx, "w+", ChunkedCtx::functions);
	return file;
}

FILE* HttpServer::Client::fopenChunked(const char*mime)
{
	FILE *file=fopenChunked();
	if(!file)
		return file;
	
	fprintf(file,
		"HTTP/1.1 200 Ok" CRLF
		"Content-Type: %s" CRLF
		"Transfer-Encoding: chunked" CRLF
		CRLF
		,mime
	);
	fseek(file, 0L, SEEK_END);
	return file;
}

NOTHREAD_HANDLER_BODY(HttpServer::Client,onRead)
{
	int status;
	
	status=read(request.buffer.buf+request.buffer.len,request.buffer.size-request.buffer.len);
	//ldebug("read %d/%zd %d\n",status,request.buffer.size-request.buffer.len,errno);
	//if(status>=0) ldebug("%.*s\n",status,request.buffer.buf+request.buffer.len);
	if(status==0)
	{
		bye();
		return;
	}
	if(status<0)
	{
		if(errno==EAGAIN)
		{
			return;
		}
		ldebug("error %d %s\n",errno,strerror(errno));
		server->nothread->disableBackgroundHandling(fd);
		bye();
		return;
	}
	request.buffer.len+=status;
	if(request.findHead())
	{
		if(server->debugLevel&7)
		{
			Buffer h(request.header),d(request.data);
			if(h.len && (server->debugLevel&1))
			{
				_debug("<< H %zd %s\n", h.len, peer.cString());
				_debug("| " BUFFER_FMT "\n",BUFFER_ARG(request.version));
				for(Request::Arg *i=request.options.array,*e=i+request.options.count;i<e;i++)
				_debug("| " BUFFER_FMT "\n",BUFFER_ARG(i->value));
			}
			if(h.len && (server->debugLevel&2))
			{
				_debug("<< H %zd %s\n", h.len, peer.cString());
				h.dump(stderr,1,32);
			}
			if(d.len && (server->debugLevel&4))
			{
				_debug("<< D %zd %s\n", d.len, peer.cString());
				d.slice(0,1024).dump(stderr,1,32);
			}
		}
		request.parse();
		inData=1;
		server->nothread->disableBackgroundHandling(fd);
		if(request.remain.len)
		{
			server->nothread->setBackgroundHandling(fd,NOTHREAD_READABLE,onReadData_,this);
		}
		else
			handle();
		return;
	}

	if(request.buffer.len>=request.buffer.size)
	{
		ldebug("check size\n");
		bye();
		return;
	}
}

NOTHREAD_HANDLER_BODY(HttpServer::Client,onReadData)
{
	int status;
	
	status=read(request.remain.ptr,request.remain.len);
	//ldebug("read %d/%zd %d\n",status,request.buffer.size-request.buffer.len,errno);
	if(status==0)
	{
		server->nothread->disableBackgroundHandling(fd);
		bye();
		return;
	}
	if(status<0)
	{
		if(errno==EAGAIN)
		{
			return;
		}
		ldebug("error %d %s\n",errno,strerror(errno));
		server->nothread->disableBackgroundHandling(fd);
		bye();
		return;
	}
	request.remain.consume(status);
	if(!request.remain.len)
	{
		server->nothread->disableBackgroundHandling(fd);
		handle();
	}
}

NOTHREAD_HANDLER_BODY(HttpServer::Client,onWrite)
{
	int status;
	
	//ldebug(BUFFER_FMT,BUFFER_ARG(response.buffer));
	status=write(response.buffer.ptr,response.buffer.len);
	//ldebug("write %d/%zd mask %d %d %s\n",status,response.buffer.len,mask,errno,strerror(errno));
	if(status<0)
	{
		if(errno==EAGAIN)
		{
			if(mask==0)
				server->nothread->setBackgroundHandling(fd,NOTHREAD_WRITABLE,onWrite_,this);
			return;
		}
		if(mask!=0)
			server->nothread->disableBackgroundHandling(fd);
		bye();
		return;
	}
	response.buffer.ptr+=status;
	response.buffer.len-=status;
	if(response.buffer.len==0)
	{
		if(mask!=0)
			server->nothread->disableBackgroundHandling(fd);
		if(response.fd!=-1)
		{
			onFileRead();
		}
		else
		if(response.then.function)
		{
			response.then.function(response.then.that,this,0);
		}
		else
		{
			waitRequest();
		}
	}
	else
	{
		if(mask==0)
			server->nothread->setBackgroundHandling(fd,NOTHREAD_WRITABLE,onWrite_,this);
	}
	
	return;
}

void HttpServer::Client::Response::init()
{
	buffer.raz();
	fd=-1;
	then.that=NULL;
	then.function=NULL;
}

NOTHREAD_HANDLER_BODY(HttpServer::Client,onFileRead)
{
	Size done;

	response.buffer.raz();
	if(response.buffer.len>response.todo)
		response.buffer.len=response.todo;
	done=::read(response.fd,response.buffer.ptr,response.buffer.len);
	//ldebug("::read %d,%zd/%zd\n", response.fd, done, response.buffer.len);
	if(done==0)
	{
		server->nothread->disableBackgroundHandling(response.fd);
		close(response.fd);
		response.fd=-1;
		if(response.todo)
		{
			ldebug("unexpected end\n");
			bye();
		}
		else
		{
			waitRequest();
		}
	}
	else
	if(done>0)
	{
		response.todo-=done;
		response.buffer.len=done;
		server->nothread->disableBackgroundHandling(response.fd);
		server->nothread->setBackgroundHandling(fd,NOTHREAD_WRITABLE,onWrite_,this);
	}
	else
	{
		if(errno==EAGAIN && 0==server->nothread->setBackgroundHandling(response.fd,NOTHREAD_READABLE,onFileRead_,this))
		{
		}
		else
		{
			ldebug("!read %d\n",errno);
			server->nothread->disableBackgroundHandling(fd);
			bye();
		}
	}
}

void HttpServer::Client::Request::init()
{
	buffer.ptr=buffer.buf;
	buffer.len=0;
	header=buffer;
	method=path=version=Buffer::null;
	args.init();
	options.init();
	data.resize(0);
	remain=data;
}

void unpercent(Buffer &buffer)
{
	Buffer src=buffer;
	Buffer dst=buffer;
	
	while(src.len && dst.len)
	{
		if(*src.ptr=='%' && src.len>=3)
		{
			*dst.ptr=src.slice(1,3).scanU32(16);
			src.ptr+=3;
			src.len-=3;
		}
		else
		{
			*dst.ptr=*src.ptr;
			src.ptr++;
			src.len--;
		}
		dst.ptr++;
		dst.len--;
	}
	buffer.len-=dst.len;
}

int HttpServer::Client::Request::findHead()
{
	for(Size i=header.len;i<=buffer.len-2;i++)
	{
		if(buffer.buf[i]=='\r' && buffer.buf[i+1]=='\n')
		{
			Buffer line(header.end(),i-header.len);
			header.len=i+2;
			//ldebug("line " BUFFER_FMT "\n",BUFFER_ARG(line));
			if(line.len==0)
			{
				return 1;
			}
			if(!version.ptr)
			{
				version=line;
			}
			else
			{
				Arg *option=options.alloc();
				if(option)
				{
					option->value=line;
				}
			}
		}
	}
	return 0;
}

int HttpServer::Client::Request::parse()
{
	version.splitNext(' ',&method);
	version.splitNext(' ',&query);
	query.splitNext('?',&path);
	
	unpercent(path);
	method.terminate();
	path.terminate();
	version.terminate();
	
	Buffer qs=query,q;
	while(qs.splitNext('&',&q))
	{
		Arg *arg=args.alloc();
		if(arg)
		{
			Size p;
			p=q.search('=');
			if(p==Buffer::notFound)
			{
				arg->name=q;
				arg->value=Buffer::null;
			}
			else
			{
				arg->name=q.slice(0,p);
				arg->value=q.slice(p+1);
				unpercent(arg->value);
				arg->value.terminate();
			}
			unpercent(arg->name);
			arg->name.terminate();
			//ldebug("%.*s,%.*s\n",BUFFER_ARG(arg->name),BUFFER_ARG(arg->value));
		}
	}
	//ldebug(BUFFER_FMT "," BUFFER_FMT "," BUFFER_FMT "," BUFFER_FMT "\n",
	//	BUFFER_ARG(method),BUFFER_ARG(path),BUFFER_ARG(query),BUFFER_ARG(version));
	for(Arg *i=options.array,*e=i+options.count;i<e;i++)
	{
		i->value.splitNext(':',&i->name);
		i->name.trim();
		i->value.trim();
		if(i->name=="Content-Length" || i->name=="content-length")
		{
			data.resize(i->value.scanLong(10));
		}
		//ldebug("%.*s,%.*s\n",BUFFER_ARG(i->name),BUFFER_ARG(i->value));
		i->name.terminate();
		i->value.terminate();
	}
	Buffer d(header.end(),buffer.len-header.len);
	remain=data;
	remain.consume(d);
	//ldebug("%d %d %d\n",d.len,data.len,remain.len);
	return 0;
}

int HttpServer::Client::handle()
{
	request.buffer.raz();
	request.remain=request.buffer;
	request.remain.consume(request.header.len);
	inData=0;
	response.then.function=NULL;
	
	//ldebug(BUFFER_FMT " " BUFFER_FMT "\n",BUFFER_ARG(request.method),BUFFER_ARG(request.path));
	foreach(Url,url,server->urls)
	{
		//ldebug("%s %s\n",url->method,url->path);
		if( true
			&& (!url->method || request.method==url->method)
			&& (!url->path   || request.path==url->path)
		)
		{
			if(0==url->onUrl(url->that,this))
				return 0;
		}
	}
	handleError(404,"not found");
	return 0;
}

int HttpServer::Client::handleError(int error,const char*string)
{
	//ldebug("handleError %d %s\n",error,string);
	BufferAlloced<1024> data;
	data.raz();
	data.consume(
		"<html>\n"
		"	<head>\n"
		"		<title> %d - %s </title>\n"
		"	</head>\n"
		"	<body>\n"
		"		<h1>%d %s</h1>\n"
		"	</body>\n"
		"</html>\n"
		,error,string
		,error,string
	);
	data.consumed();

	response.buffer.raz();
	response.buffer.consume("HTTP/1.1 %d %s" CRLF,error,string);
	response.buffer.consume("Content-Type: text/html; charset=UTF-8" CRLF);
	response.buffer.consume("Content-Length: %d" CRLF,data.len);
	response.buffer.consume(CRLF);
	response.buffer.consume(BUFFER_FMT,BUFFER_ARG(data));
	response.buffer.consumed();
	onWrite();
	return 0;
}

/*
printf "GET /..//.../.../../../../../../etc/passwd HTTP/1.0\r\nAuthorization: Basic %s\r\n\r\n" $(echo -ne "admin:" | openssl enc -base64) | openssl s_client -ign_eof -connect 127.0.0.1:8443
*/

struct Miam : public Buffer
{
	int miam(Buffer b);
	int miamMiam(Buffer b);
};

int Miam::miam(Buffer b)
{
	int found=0;
	U8 *d=ptr,*s=ptr,*e=ptr+len-b.len;
	while(d<e)
	{
		if(Buffer(s,b.len)==b)
		{
			found++;
			s+=b.len;
			len-=b.len;
			continue;
		}
		*d=*s;
		s++;
		d++;
	}
	return found;
}

int Miam::miamMiam(Buffer b)
{
	int count=0;
	while(int c=miam(b))
		count+=c;
	return count;
}


int HttpServer::Client::handleFile(Buffer root)
{
	return handleFile(root,root);
}

int HttpServer::Client::handleFile(Buffer root,Buffer dirroot)
{
	int status;
	struct stat filestat;
	
	char filename_[root.len+request.path.len+1];
	BufferCString filename;
	filename.ptr=(U8*)filename_;
	filename.len=sprintf(filename_,BUFFER_FMT BUFFER_FMT,BUFFER_ARG(root),BUFFER_ARG(request.path));
	
	if(((Miam *)&filename)->miamMiam(Buffer("/..")))
		filename.terminate();
	
	memset(&filestat,0,sizeof(filestat));
	status=::stat(filename.cString(),&filestat);
	//ldebug("filename %s %d %zd\n",filename.cString(),status,filestat.st_size);
	if(status!=0)
		return -1;
	
	if(S_ISDIR(filestat.st_mode))
	{
		struct stat indexstat;
		Buffer index("index.html");
		char indexname_[filename.len+1+index.len+1];
		BufferCString indexname;
		indexname.ptr=(U8*)indexname_;
		indexname.len=sprintf(indexname_,BUFFER_FMT "/" BUFFER_FMT, BUFFER_ARG(filename), BUFFER_ARG(index));
		status=::stat(indexname.cString(),&indexstat);
		//ldebug("indexname %s\n",indexname.cString());
		if(status==0)
			return handleFile(indexname,&indexstat);
		
		return handleDir(dirroot,filename,&filestat);
	}
	return handleFile(filename,&filestat);
}

int HttpServer::Client::handleFile(BufferCString filename,struct stat *stat)
{
	struct
	{
		const char*extension;
		const char*mime;
	}*em,ems[]=
	{
		{"js"  ,"application/javascript; charset=UTF-8" },
		{"html","text/html; charset=UTF-8" },
		{"xml" ,"text/xml; charset=UTF-8"  },
		{"css" ,"text/css; charset=UTF-8"  },
		{"png" ,"image/png" },
		{"jpg" ,"image/jpeg" },
		{"jpeg" ,"image/jpeg" },
		{"svg" ,"image/svg+xml" },
		{"txt" ,"text/plain; charset=UTF-8"},
		{NULL  ,"text/plain; charset=UTF-8"},
	};
	
	Buffer filename_(filename);
	Size dot;
	dot=filename_.rsearch('.');
	if(dot==Buffer::notFound)
		em=ems+N_ELEMENT(ems)-1;
	else
	{
		Buffer extension=filename_.slice(dot+1,~0);
		for(em=ems;em->extension;em++)
		{
			if(extension==em->extension)
				break;
		}
	}
	
	int status=200;
	size_t f,t;
	f=0;
	t=stat->st_size;
	for(size_t i=0;i<request.options.count;i++)
	{
		HttpServer::Client::Request::Arg *option=request.options.array+i;
		if(option->name!="Range")
			continue;
		Buffer unit,range;
		option->value.split('=',&unit,&range,NULL);
		if(unit!="bytes")
			continue;
		Buffer bf,bt;
		range.split('-',&bf,&bt,NULL);
		if(bf.len)
		{
			f=bf.scanLong();
			if(bt.len)
			{
				t=bt.scanLong();
				t++;
			}
		}
		else
		{
			f=t-bt.scanLong();
		}
		status=t>f?206:416;
		ldebug("range " BUFFER_FMT "-" BUFFER_FMT " : %zd-%zd\n",BUFFER_ARG(bf),BUFFER_ARG(bt),f,t);
	}
	
	response.todo=t-f;
	
	response.buffer.raz();
	response.buffer.consume(
		"HTTP/1.1 %d OK" CRLF
		"Content-Type: %s" CRLF
		"Content-Length: %d" CRLF
		"Accept-Ranges: bytes" CRLF
		,status
		,em->mime
		,response.todo
	);
	if(status==206)
	response.buffer.consume(
		"Content-Range: bytes %d-%d/%d" CRLF
		,f,t-1,stat->st_size
	);
	else
	if(status==416)
	response.buffer.consume(
		"Content-Range: bytes */%d" CRLF
		,stat->st_size
	);
	response.buffer.consume(
		CRLF
	);
	response.buffer.consumed();
	response.fd=open(filename.cString(),O_NONBLOCK|O_RDONLY);
	lseek(response.fd,f,SEEK_SET);
	//ldebug("handleFile %zd %s %d\n",stat->st_size,filename,response.fd);
	onWrite();
	return 0;
}

struct DirContext
{
	static const char* const plain;
	static const char* const html;
	
	typedef HttpServer::Client Client;
	BufferCString name;
	DIR *dir;
	const char*mime;
	
	int init(BufferCString name,Buffer mime);
	THEN_DECL(onContinue);
};

const char* const DirContext::plain="text/plain";
const char* const DirContext::html="text/html";

int DirContext::init(BufferCString n,Buffer mime)
{
	dir=opendir(n.cString());
	if(!dir)
		return -1;
	
	name.ptr=(U8*)(this+1);
	name.len=n.len;
	strcpy(name.cString(),n.cString());
	//ldebug(BUFFER_FMT "\n",BUFFER_ARG(mime));
	this->mime=plain;
	if(mime==html)
		this->mime=html;
	else
	if(mime==plain)
		this->mime=plain;
	
	return 0;
}

THEN_BODY(DirContext,onContinue)
{
	if(closed)
	{
		closedir(dir);
		return 0;
	}
	struct dirent *entry;
	entry=readdir(dir);
	if(!entry)
	{
		closedir(dir);
		client->response.buffer.raz();
		if(mime==html)
			client->response.buffer.consume(
				"%x" CRLF
				"</table>\n</body>\n</html>\n" CRLF
				,8+      1+7+     1+7+     1
			);
		client->response.buffer.consume(
			"0" CRLF
			CRLF
		);
		client->response.buffer.consumed();
		client->response.then.that=NULL;
		client->response.then.function=NULL;
		client->onWrite();
		return 0;
	}
	
	BufferCString en(entry->d_name);
	U8 fn_[name.len+1+en.len+1];
	BufferCString fn;
	fn.ptr=fn_;
	fn.len=sprintf(fn.cString(),"%s/%s",name.cString(),en.cString());
	//ldebug("%s %s\n",en.cString(),fn.cString());
	struct stat stat;
	lstat(fn.cString(),&stat);
	name.terminate();
	client->response.buffer.raz();
	if(mime==plain)
	{
		client->response.buffer.consume(
			"%x" CRLF
			"%07o %10d %10d \"" BUFFER_FMT "\"\n" CRLF
			,7+  1+10+1+10+1+1+en.len       +1+1
			,stat.st_mode
			,stat.st_size
			,stat.st_mtime
			,BUFFER_ARG(en)
		);
	}
	else
	if(mime==html)
	{
		Buffer end(S_ISDIR(stat.st_mode)?"/":"");
		
		client->response.buffer.consume(
			"%x" CRLF
			"<tr><td>%07o</td><td>%10d</td><td><a href=\"" BUFFER_FMT BUFFER_FMT "\">" BUFFER_FMT BUFFER_FMT "</a></td></tr>\n" CRLF
			,4+  4+  7+  5+   4+  10+ 5+   4+  9+          en.len+    end.len+    2+   en.len+    end.len+    4+  5+   5+   1
			,stat.st_mode
			,stat.st_size
			,BUFFER_ARG(en),BUFFER_ARG(end)
			,BUFFER_ARG(en),BUFFER_ARG(end)
		);
	}
	
	client->response.buffer.consumed();
	client->onWrite();
	return 0;
}

int HttpServer::Client::handleDir(Buffer root,BufferCString filename,struct stat *stat)
{
	Buffer mime;
	mime=request.args.value("mime");
	
	if(mime.isNull())
	{
		Buffer dir("dir.html");
		char buf[root.len+1+dir.len+1];
		BufferCString dir_full;
		dir_full.ptr=(U8*)buf;
		dir_full.len=sprintf(buf,BUFFER_FMT "/" BUFFER_FMT, BUFFER_ARG(root), BUFFER_ARG(dir));
		struct stat stat;
		if(-1==::stat(dir_full.cString(),&stat))
			return -1;
		return handleFile(dir_full,&stat);
	}
	
	DirContext *context=(DirContext*)align(request.header.end(),sizeof(U32));
	
	if(0!=context->init(filename,mime))
		return -1;
	
	response.buffer.raz();
	response.buffer.consume(
		"HTTP/1.1 200 OK" CRLF
		"Content-Type: %s" CRLF
		"Transfer-Encoding: chunked" CRLF
		CRLF
		,context->mime
	);
	if(context->mime==context->html)
	{
		response.buffer.consume(
			"%x" CRLF
			"<html>\n<body>\n<table>\n" CRLF
			,22
		);
	}
	response.buffer.consumed();
	response.then.that=context;
	response.then.function=DirContext::onContinue_;
	onWrite();
	return 0;
}

#ifdef HTTP_WITH_FILE_INSTALL
int HttpServer::Client::handleFileGet(BufferMallocedCString &intern_name,BufferMallocedCString &extern_name)
{
	for(size_t i=0;i<request.args.count;i++)
	{
		HttpServer::Client::Request::Arg *arg=request.args.array+i;
		if(arg->name=="intern")
			intern_name=arg->value;
		else
		if(arg->name=="extern")
			extern_name=arg->value;
	}
	
	intern_name.terminate();
	struct stat stat;
	stat.st_size=0;
	if(0!=::stat((const char*)intern_name.ptr,&stat))
	{
		response.buffer.raz();
		response.buffer.consume(
			"HTTP/1.0 404 Not found" CRLF
			"Cache-control: no-cache" CRLF
			"Pragma: no-cache" CRLF
			"Content-Type: plain/text" CRLF
			"Content-Length: 0" CRLF
			CRLF
		);
		response.buffer.consumed();
		
		onWrite();
		return 0;
	}
	
	response.buffer.raz();
	response.buffer.consume(
		"HTTP/1.0 200 OK" CRLF
		"Cache-control: no-cache" CRLF
		"Pragma: no-cache" CRLF
		"Content-Type: application/x-download" CRLF
		"Content-Disposition:attachment;filename=" BUFFER_FMT CRLF
		"Content-Length: %zd" CRLF
		CRLF
		,BUFFER_ARG(extern_name)
		,stat.st_size
	);
	response.buffer.consumed();
	
	response.fd=open((const char*)intern_name.ptr,O_NONBLOCK);
	response.todo=stat.st_size;
	
	onWrite();
	return 0;
}

int HttpServer::Client::handleFileSet(BufferMallocedCString &intern_name)
{
	const char *ok="ok";
	const char *err=ok;
	int multipart=0,crc32=0,exec=0;
	Buffer boundary,data;
	
	ldebug("" BUFFER_FMT "," BUFFER_FMT "\n",BUFFER_ARG(request.method),BUFFER_ARG(request.path));
	for(size_t i=0;i<request.options.count;i++)
	{
		HttpServer::Client::Request::Arg *option=request.options.array+i;
		ldebug("" BUFFER_FMT "=" BUFFER_FMT "\n",BUFFER_ARG(option->name),BUFFER_ARG(option->value));
		if(option->name=="Content-Type")
		{
			Buffer miam=option->value,name,value;
			while(miam.splitNext(':',&value))
			{
				value.splitNext('=',&name);
				name.trim(' ');
				if(name=="multipart/form-data")
					multipart=1;
				else
				if(name=="boundary")
					boundary=value;
			}
		}
	}
	//ldebug("%d\n",request.data.len);
	//ldebug("\n-----------\n%.*s\n-----------\n",request.data.len,request.data.ptr);
	
	for(size_t i=0;i<request.args.count;i++)
	{
		HttpServer::Client::Request::Arg *arg=request.args.array+i;
		if(arg->name=="intern")
			intern_name=arg->value;
		else
		if(arg->name=="crc32")
			crc32=1;
		else
		if(arg->name=="exec")
			exec=1;
	}
	
	if(multipart)
	{
		Buffer crlf("\r\n");
		Buffer remain=request.data,line;
		
		while(remain.splitNext('\n',&line))
		{
			line.trim_right('\r');
			//ldebug("" BUFFER_FMT "\n",BUFFER_ARG(line));
			if(line.len==0)
				break;
		}
		//remain.slice(~256,~0).dump(stderr);
		data=remain.slice(0,~(crlf.len+2+boundary.len+2+crlf.len));
	}
	else
		data=request.data;
	
	ldebug("file size is %zd\n",data.len);
	
	if(crc32)
	{
		if(::crc32(::crc32(0, Z_NULL, 0), data.ptr, data.len)!=0x2144df1c)
			err="crc32";
	}
	
	if(err==ok)
	{
		int fd;
		fd=open(intern_name.cString(),O_CREAT|O_TRUNC|O_WRONLY,0755);
		if(fd==-1)
		{
			err="open";
		}
		else
		{
			ssize_t done;
			done=::write(fd,data.ptr,data.len);
			if(done!=(ssize_t)data.len)
			{
				err="write";
				ldebug("failed to write %zd bytes (%zd) in %s\n",data.len,done,intern_name.cString());
			}
			close(fd);
		}
	}
	if(err==ok && exec)
	{
		int pid;
		pid=fork();
		if(pid==0)
		{
			int argc=1;
			for(size_t i=0;i<request.args.count;i++)
			{
				HttpServer::Client::Request::Arg *arg=request.args.array+i;
				if(arg->name=="arg")
					argc++;
			}
			char *argv[argc+1];
			char *env[1]={NULL};
			argv[0]=intern_name.cString();
			for(size_t i=0,j=1;i<request.args.count;i++)
			{
				HttpServer::Client::Request::Arg *arg=request.args.array+i;
				if(arg->name=="arg")
				{
					arg->value.ptr[arg->value.len]='\0';
					ldebug("arg %zd %s\n",j,(char*)arg->value.ptr);
					argv[j++]=(char*)arg->value.ptr;
				}
			}
			argv[argc]=NULL;
			// attention au file dup !!
			ldebug("%s %d\n",intern_name.cString(),argc);
			execve(intern_name.cString(),argv,env);
			ldebug("can't exec %s\n",intern_name.cString());
			err="execve";
			::exit(0);
		}
		else
		if(pid<0)
		{
			err="fork";
		}
		else
		{
			int status;
			while(1)
			{
				if(-1==waitpid(pid, &status, WUNTRACED | WCONTINUED))
				{
					err="waitpid";
					break;
				}
				if(WIFEXITED(status))
				{
					if(status)
						err="error";
					break;
				}
				if(WIFSIGNALED(status))
				{
					err="killed";
					break;
				}
				if(WIFSTOPPED(status))
				{
					ldebug("%s stopped\n",intern_name.cString());
				}
				else
				if(WIFCONTINUED(status))
				{
					ldebug("%s continued\n",intern_name.cString());
				}
			}
		}
	}
	
	response.buffer.raz();
	response.buffer.consume(
		"HTTP/1.0 200 OK" CRLF
		"Content-Length: %d" CRLF
		CRLF
		"%s"
		,strlen(err)
		,err
	);
	response.buffer.consumed();
	onWrite();
	return 0;
}
#endif

URL_BODY(HttpServer,testArgs)
{
	BufferAlloced<1024> data;
	data.raz();
	for(size_t i=0;i<client->request.args.count;i++)
	{
		data.consume(
			BUFFER_FMT "=" BUFFER_FMT "\n"
			,BUFFER_ARG(client->request.args.array[i].name)
			,BUFFER_ARG(client->request.args.array[i].value)
		);
	}
	data.consumed();
	client->response.buffer.raz();
	client->response.buffer.consume(
		"HTTP/1.0 200 OK" CRLF
		"Content-Length: %d" CRLF
		CRLF
		BUFFER_FMT
		,data.len
		,BUFFER_ARG(data)
	);
	client->response.buffer.consumed();
	//ldebug("handleList " BUFFER_FMT "\n",BUFFER_ARG(client->request.query));
	client->onWrite();
	return 0;
}

#ifdef HTTP_WITH_LOGIN
int HttpServer::Client::basicAuthorization(BufferMallocedCString *user,BufferMallocedCString *pass)
{
	const Buffer basic("Basic ");
	Buffer *authorization=NULL;
	for(size_t i=0;i<request.options.count;i++)
	{
		Request::Arg *option=request.options.array+i;
		//ldebug("option : " BUFFER_FMT "=" BUFFER_FMT "\n",BUFFER_ARG(option->name),BUFFER_ARG(option->value));
		if(option->name=="Authorization")
			authorization=&option->value;
	}
	//ldebug(BUFFER_FMT " %p\n",BUFFER_ARG(request.path),authorization);
	if(!authorization)
		return -1;
	//ldebug("authorization=" BUFFER_FMT "\n",BUFFER_ARG(*authorization));
	if(!authorization->beginBy(basic))
		return -1;
	
	Buffer encoded,decoded,e,d,u,p;
	encoded=authorization->slice(basic.len,~0);
	U8 decoded_[encoded.decode64Max()];
	decoded.ptr=decoded_;
	decoded.len=sizeof(decoded_);
	e=encoded;
	d=decoded;
	decoded.len=e.decode64(&d);
	//ldebug("encoded=" BUFFER_FMT ",decoded=" BUFFER_FMT ".\n",BUFFER_ARG(encoded),BUFFER_ARG(decoded));
	p=decoded;
	p.splitNext(':',&u);
	if(server->debugLevel&8)
		ldebug("user=" BUFFER_FMT ",password=" BUFFER_FMT ".\n",BUFFER_ARG(u),BUFFER_ARG(p));
	if(user)
		*user=u;
	if(pass)
		*pass=p;
	
	return 0;
}

int HttpServer::Client::session(BufferMallocedCString *user,BufferMallocedCString *pass)
{
	Session *s=session();
	if(!s)
		return -1;
	
	*user=s->user;
	*pass=s->pass;
	return 0;
}

int HttpServer::Client::check(const char *prompt,int mask)
{
	BufferMallocedCString user,pass;
	
	if(server->logins.isEmpty())
		return 0;
	
	foreach(Login,login,server->logins)
	{
		if(login->user.len==0)
		{
			if(login->rights&mask)
				return 0;
		}
	}
	
	if(0==session(&user,&pass) || 0==basicAuthorization(&user,&pass))
	{
		foreach(Login,login,server->logins)
		{
			if(user==login->user && pass==login->pass)
			{
				if(login->rights&mask)
				{
					//logger->add(NtpNow()," type=http method=\"" BUFFER_FMT "\" uri=\"" BUFFER_FMT "\" uri=\"" BUFFER_FMT "\" user=\"" BUFFER_FMT "\" msg=accept",BUFFER_ARG(request.method),BUFFER_ARG(request.path),BUFFER_ARG(user));
					return 0;
				}
			}
		}
	}
	authorizationRequired(prompt);
	//logger->add(NtpNow()," type=http method=\"" BUFFER_FMT "\" uri=\"" BUFFER_FMT "\" uri=\"" BUFFER_FMT "\" user=\"" BUFFER_FMT "\" msg=refused",BUFFER_ARG(request.method),BUFFER_ARG(request.path),BUFFER_ARG(user));
	return -1;
}

int HttpServer::Client::authorizationRequired(const char *prompt)
{
	//ldebug("deny " BUFFER_FMT "\n",BUFFER_ARG(request.path));
	Buffer data(
		"<html>\n"
		"	<head>\n"
		"		<title> Authorization Required </title>\n"
		"	</head>\n"
		"	<body>\n"
		"		<h1>Authorization Required</h1>\n"
		"	</body>\n"
		"</html>\n"
	);
	
	response.buffer.raz();
	response.buffer.consume(
		"HTTP/1.1 401 Authorization Required" CRLF
		"Cache-control: no-cache" CRLF
		"Pragma: no-cache" CRLF
		"WWW-Authenticate: Basic realm=\"%s\"" CRLF
		"Content-Length: %d" CRLF
		CRLF
		BUFFER_FMT
		,prompt
		,data.len
		,BUFFER_ARG(data)
	);
	response.buffer.consumed();
	onWrite();
	return 0;
}

HttpServer::Session* HttpServer::Client::session()
{
	for(size_t i=0;i<request.options.count;i++)
	{
		Request::Arg *option=request.options.array+i;
		//ldebug(BUFFER_FMT "=" BUFFER_FMT ".\n",BUFFER_ARG(option->name),BUFFER_ARG(option->value));
		if(option->name=="Cookie")
		{
			Buffer cookies=option->value,cookie;
			while(cookies.splitNext(';',&cookie))
			{
				Buffer name,value;
				cookie.trim_left(' ');
				cookie.split('=',&name,&value,NULL);
				//ldebug("cookie : " BUFFER_FMT "=" BUFFER_FMT ".\n",BUFFER_ARG(name),BUFFER_ARG(value));
				if(name=="session")
				{
					size_t id=value.strtoU32();
					foreach(Session,session,server->sessions)
					{
						if(id==(size_t)session->id)
							return session;
					}
					//ldebug("%d not found\n",id);
				}
			}
		}
	}
	return NULL;
}

URL_BODY(HttpServer,login)
{
	Buffer *user=NULL;
	Buffer *pass=NULL;
	//ldebug("user : " BUFFER_FMT "\n",BUFFER_ARG(client->request.header));
	for(size_t i=0;i<client->request.args.count;i++)
	{
		HttpServer::Client::Request::Arg *arg=client->request.args.array+i;
		if(arg->name=="user")
			user=&arg->value;
		if(arg->name=="password")
			pass=&arg->value;
	}
	
	Session *session=client->session();
	if(user && pass)
	{
		if(session)
		{
			session->remove();
			delete session;
			session=NULL;
		}
		foreach(Login,login,client->server->logins)
		{
			if(*user==login->user && *pass==login->pass)
			{
				session=new Session();
				session->user=*user;
				session->pass=*pass;
				client->server->sessions.queue(session);
			}
		}
		ldebug(BUFFER_FMT ":" BUFFER_FMT ",%p.\n",BUFFER_ARG(*user),BUFFER_ARG(*pass),session);
	}
	
	BufferAlloced<1024> data;
	data.raz();
	if(session)
	{
		data.consume(
			"{\n"
			"	user:\"" BUFFER_FMT "\",\n"
			//"	rights:\"%d\",\n"
			"}\n"
			,BUFFER_ARG(session->user)
		);
	}
	else
		data.consume("0");
	data.consumed();
	
	client->response.buffer.raz();
	client->response.buffer.consume(
		"HTTP/1.0 200 OK" CRLF
	);
	if(session)
	client->response.buffer.consume(
		"Set-Cookie: session=%d; path=/" CRLF
		,(int)session->id
	);
	client->response.buffer.consume(
		"Content-Type: text/javascript" CRLF
		"Content-Length: %d" CRLF
		CRLF
		BUFFER_FMT
		,data.len
		,BUFFER_ARG(data)
	);
	client->response.buffer.consumed();
	client->onWrite();
	return 0;
}

URL_BODY(HttpServer,logout)
{
	Session *session=client->session();
	int yes=session ? 1:0;
	
	if(session)
	{
		session->remove();
		delete session;
	}
	
	client->response.buffer.raz();
	client->response.buffer.consume(
		"HTTP/1.0 200 OK" CRLF
		"Content-Type: text/javascript" CRLF
		"Content-Length: 1" CRLF
		CRLF
		"%d"
		,yes
	);
	client->response.buffer.consumed();
	client->onWrite();
	return 0;
}
#endif

#ifdef HTTP_WITH_PAM
URL_BODY(HttpServer,pamCheck)
{
	BufferMallocedCString user,pass;
	
	if(0==client->basicAuthorization(&user,&pass))
	foreach(Login,login,logins)
	if(user==login->user)
	{
		int checked=0;
		
		if(login->pass!="" && login->pass==pass)
			checked=1;
		else
		{
			Pam pam;
			//ldebug("%s %s\n",user.cString(),pass.cString());
			pam.check(user.cString(),pass.cString(),&checked);
			if(checked)
			{
				login->pass=pass;
				syslog(LOG_AUTH|LOG_NOTICE, "login succeed, user \"%s\" from \"%s\"", user.cString(), client->peer.cString());
			}
		}
		if(checked)
			return -1;
	}
	syslog(LOG_AUTH|LOG_WARNING, "login failed, user \"%s\" from \"%s\"", user.cString(), client->peer.cString());
	return client->authorizationRequired("bad user");
}

URL_BODY(HttpServer,pamChange)
{
	int status=-1;
	
	BufferCString user(""),curr(""),pass("");
	for(HttpServer::Client::Request::Arg *a=client->request.args.array,*e=a+client->request.args.count;a<e;a++)
	{
		if(0) ;
		else if(a->name=="user") user=a->value;
		else if(a->name=="curr") curr=a->value;
		else if(a->name=="pass") pass=a->value;
	}
	
	Pam pam;
	status=pam.change(user.cString(),curr.cString(),pass.cString());
	ldebug("%s %s %s %d\n",user.cString(),curr.cString(),pass.cString(),status);
	
	FILE *file=client->fopenChunked("text/plain");
	fprintf(file,"%d",status);
	fclose(file);
	return 0;
}
#endif
