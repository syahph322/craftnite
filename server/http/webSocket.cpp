// Copyright 2022 Thierry LARMOIRE
// SPDX short identifier: MIT
#include "webSocket.h"
#include "sha1.h"
#include <errno.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>

WebSocket::WebSocket()
{
}

WebSocket::~WebSocket()
{
}

int WebSocket::init(HttpServer::Client *client)
{
	this->client=client;
	ldebug("%d\n",client->fd);
	
	Buffer upgrade,connection,secWebSocketKey,secWebSocketVersion;
	for(HttpServer::Client::Request::Arg *i=client->request.options.array,*e=i+client->request.options.count;i<e;i++)
	{
		if(0);
		else if(i->name=="Connection")
			connection=i->value;
		else if(i->name=="Upgrade")
			upgrade=i->value;
		else if(i->name=="Sec-WebSocket-Key")
			secWebSocketKey=i->value;
		else if(i->name=="Sec-WebSocket-Version")
			secWebSocketVersion=i->value;
	}
	
	Buffer guid("258EAFA5-E914-47DA-95CA-C5AB0DC85B11");
	
	U8 hash_[Sha1::len];
	Buffer hash(hash_,sizeof(hash_));
	
	Sha1 sha1;
	sha1.init();
	sha1.update(secWebSocketKey);
	sha1.update(guid);
	sha1.exit(hash_);
	
	U8 b64_[hash.encode64Max()];
	Buffer b64(b64_,sizeof(b64_));
	Buffer i=hash,o=b64;
	i.encode64(&o);
	
	client->response.buffer.raz();
	client->response.buffer.consume(
		"HTTP/1.1 101 Switching Protocols" CRLF
		"Date: Tue, 13 Sep 2022 08:35:55 GMT" CRLF
		"Connection: upgrade" CRLF
		"Upgrade: websocket" CRLF
		"Sec-WebSocket-Accept: " BUFFER_FMT CRLF
		"CF-Cache-Status: DYNAMIC" CRLF
		//"Report-To: {\"endpoints\":[{\"url\":\"https:\/\/a.nel.cloudflare.com\/report\/v3?s=0p%2BdW36Fu3Qcl6RqCQwxIBN0um7oefAT%2F9t0IOAyJvYvKc1o9hGoi8LBxqueyMBljcyMp2S4HgzvRkzQ4yZFVQWJ%2BeZabDMa7AkiHUh40pUV3HTSZxnYBjcuJYGeIotmpHa7wQ%3D%3D\"}],\"group\":\"cf-nel\",\"max_age\":604800}" CRLF
		//"NEL: {\"success_fraction\":0,\"report_to\":\"cf-nel\",\"max_age\":604800}" CRLF
		//"Server: cloudflare" CRLF
		//"CF-RAY: 749f8d5caa18d3cc-CDG" CRLF
		//"alt-svc: h3=\":443\"; ma=86400, h3-29=\":443\"; ma=86400" CRLF
		"" CRLF
		,BUFFER_ARG(b64)
	);
	client->response.buffer.consumed();
	
	//client->response.buffer.dump(stderr);
	
	client->response.then.that=this;
	client->response.then.function=graded_;
	
	client->onWrite();
	return 0;
}

int WebSocket::exit()
{
	client->server->nothread->listenerDetach(client->fd);
	client->bye();
	return 0;
}

void WebSocket::graded(bool up)
{
	ldebug("graded %d\n",(int)up);
}

THEN_BODY(WebSocket,graded)
{
	client->response.then.function=NULL;
	ldebug("graded %d\n",(int)!closed);
	if(!closed)
	{
		buffer.resize(4096);
		header=(Header*)buffer.ptr;
		data=buffer;
		got=0;
		client->server->nothread->listenerAttach(client->fd,NOTHREAD_READABLE,ondata_,this);
		graded(true);
	}
	else
	{
		graded(false);
	}
	return 0;
}

int WebSocket::unmask(Buffer data)
{
	U8* p=data.ptr;
	Size l=data.len;
	//ldebug("0x%08x\n",mask);
	//data.dump(stderr);
	while(l && (((long)p)&3))
	{
		*p^=mask;
		mask=(mask>>8)|(mask<<24);
		p++;
		l--;
	}
	while(l>=4)
	{
		*(U32*)p^=mask;
		p+=4;
		l-=4;
	}
	while(l)
	{
		*p^=mask;
		mask=(mask>>8)|(mask<<24);
		p++;
		l--;
	}
	//data.dump(stderr);
	return 0;
}

void WebSocket::onframe(Buffer data)
{
	unmask(data);
	switch(header->opcode)
	{
		case 2:
		{
			ondata(data);
		}
		break;
		case 8:
		{
			U16 code=-1;
			if(data.len>=2)
			{
				code=(data.ptr[0]<<8) | data.ptr[1];
				data.len-=2;
				data.ptr+=2;
				ldebug("close %d %.*s\n",code,(int)data.len,data.ptr);
			}
			else
				ldebug("close bad data\n");
			client->server->nothread->listenerDetach(client->fd);
			client->waitRequest();
			graded(false);
		}
		break;
		default:
			ldebug("unexpected opcode %d\n",header->opcode);
	}
}

NOTHREAD_HANDLER_BODY(WebSocket,ondata)
{
	int status=read(client->fd,data.ptr+got,data.len-got);
	//ldebug("%d\n",status);
	if(status<=0)
	{
		ldebug("!read %d\n",errno);
		client->server->nothread->listenerDetach(client->fd);
		client->bye();
		graded(false);
		return;
	}
	//Buffer(data.ptr+got,status).dump(stderr);
	got+=status;
	
	while(1)
	if(data.ptr==buffer.ptr)
	{
		if(got<2)
			break;
		
		size_t lenlen=0,masklen=0;
		if(0);
		else if(header->len==126)
			lenlen=2;
		else if(header->len==127)
			lenlen=8;
		if(header->mask)
			masklen=4;
		if(got<2+lenlen+masklen)
			break;
		
		size_t datalen;
		if(!lenlen)
		{
			datalen=header->len;
		}
		else
		{
			datalen=0;
			for(U8 *p=header->lens,*e=p+lenlen;p<e;p++)
			{
				datalen<<=8;
				datalen|=*p;
			}
		}
		if(masklen)
		{
			this->mask=0;
			for(U8 *e=header->lens+lenlen,*p=e+masklen;p>e;)
			{
				p--;
				this->mask<<=8;
				this->mask|=*p;
			}
			//ldebug("0x%08x\n",this->mask);
		}
		//Buffer(header,2+lenlen+masklen).dump(stderr);
		//ldebug("lenlen %ld, masklen %ld, datalen %ld\n",lenlen,masklen,datalen);
		got-=2+lenlen+masklen;
		if(got>=datalen)
		{
			Buffer data(header->lens+lenlen+masklen,datalen);
			onframe(data);
			got-=datalen;
			header=(Header*)data.end();
		}
		else
		{
			data.len=datalen;
			data.ptr=(U8*)malloc(datalen);
			if(got)
			{
				memcpy(data.ptr,header->lens+lenlen,got);
			}
			break;
		}
	}
	else
	{
		if(got<data.len)
			break;
		onframe(data);
		free(data.ptr);
		got-=data.len;
		if(got!=0)
		{
			ldebug("unexpected end\n");
			got=0;
		}
		data=buffer;
		header=(Header*)buffer.ptr;
	}
	
	if(data.ptr==buffer.ptr && header!=(Header*)buffer.ptr)
	{
		if(got)
			memmove(buffer.ptr,header,got);
		header=(Header*)buffer.ptr;
	}
}

int WebSocket::send(Buffer buffer)
{
	struct msghdr msg[1];
	memset(msg,0,sizeof(*msg));
	Header header[1];
	memset(header,0,sizeof(*header));
	header->fin=1;
	header->opcode=2;
	header->mask=0;
	size_t lenlen=0;
	if(buffer.len<126)
	{
		header->len=buffer.len;
		lenlen=0;
	}
	else
	if(buffer.len<65536)
	{
		header->len=126;
		lenlen=2;
	}
	else
	{
		header->len=127;
		lenlen=8;
	}
	if(lenlen)
	{
		U64 l=buffer.len;
		for(U8*e=header->lens,*p=e+lenlen;p>e;)
		{
			p--;
			*p=l;
			l>>=8;
		}
	}
	struct iovec ios[2]={
		{ header, 2+lenlen },
		{ buffer.ptr, buffer.len }
	};
	
	//Buffer(ios[0].iov_base,ios[0].iov_len).dump(stderr);
	//Buffer(ios[1].iov_base,ios[1].iov_len).dump(stderr);
	
	msg->msg_iov=ios;
	msg->msg_iovlen=N_ELEMENT(ios);
	int status=sendmsg(client->fd, msg, 0);
	return buffer.len;
}

