// Copyright 2022 Thierry LARMOIRE
// SPDX short identifier: MIT
#ifdef HTTP_WITH_PAM
#include "pam.h"
#include "nothread/debug.h"
#include "nothread/ntp.h"
#include <string.h>
#include <stdlib.h>

int Pam::init()
{
	return 0;
}

int Pam::exit()
{
	return 0;
}

int Pam::check(const char*user, const char*pass, int *checked)
{
	int status;
	pam_handle_t *pam = NULL;
	
	*checked=0;
	conv.conv=func_;
	conv.appdata_ptr=(void*)this;
	
	status=pam_start("http", user, &conv, &pam);
	if(status!=PAM_SUCCESS)
	{
		ldebug("!pam_start %d\n ", status);
		return status;
	}
	
	// avoid blocking on failure in pam_authenticate
	pam_set_item(pam,PAM_FAIL_DELAY,(const void *)delay_);
	pam_set_item(pam, PAM_RUSER, user);
	
	this->pass=pass;
	status=pam_authenticate(pam, 0);
	if(status!=PAM_SUCCESS)
	{
		ldebug("!pam_authenticate %d %s\n", status, pam_strerror(pam,status));
	}
	else
		*checked=1;
	
	status=pam_end(pam, status);
	if(status!=PAM_SUCCESS)
	{
		ldebug("!pam_end %d\n",status);
		return status;
	}
	
	return 0;
}

int Pam::change(const char*user, const char*curr, const char*next)
{
	int status;
	pam_handle_t *pam = NULL;
	
	conv.conv=func_;
	conv.appdata_ptr=(void*)this;
	
	status=pam_start("passwd", user, &conv, &pam);
	if(status!=PAM_SUCCESS)
	{
		ldebug("!pam_start %d\n ", status);
		return status;
	}
	
	// avoid blocking on failure
	pam_set_item(pam,PAM_FAIL_DELAY,(const void *)delay_);
	
	this->pass=curr;
	this->next=next;
	status=pam_chauthtok(pam, 0);
	if(status!=PAM_SUCCESS)
	{
		ldebug("!pam_chauthtok %d %s %s %s %s\n", status, pam_strerror(pam,status),user,curr,next);
	}
	
	status=pam_end(pam, status);
	if(status!=PAM_SUCCESS)
	{
		ldebug("!pam_end %d\n",status);
		return status;
	}
	
	return 0;
}

int Pam::func_(int num_msg, const struct pam_message **msg, struct pam_response **resp, void *appdata_ptr)
{
	return ((Pam*)appdata_ptr)->func(num_msg,msg,resp);
}

int Pam::func(int num_msg, const struct pam_message **msg, struct pam_response **resp)
{
	if(num_msg!=1)
	{
		ldebug("bad num_msg %d\n",num_msg);
		return PAM_CONV_ERR;
	}
	
	switch(msg[0]->msg_style)
	{
		case PAM_PROMPT_ECHO_OFF:
		{
			const char*data=NULL;
			if(0);
			else if(0==strcmp(msg[0]->msg,"Enter new UNIX password: "))
				data=this->next;
			else if(0==strcmp(msg[0]->msg,"Retype new UNIX password: "))
				data=this->next;
			else if(0==strcmp(msg[0]->msg,"Password: "))
				data=this->pass;
			else if(0==strcmp(msg[0]->msg,"Current password: "))
				data=this->pass;
			else if(0==strcmp(msg[0]->msg,"New password: "))
				data=this->next;
			else if(0==strcmp(msg[0]->msg,"Retype new password: "))
				data=this->next;
			else
				ldebug("'%s'\n",msg[0]->msg);
			
			*resp = (struct pam_response*)malloc(sizeof(struct pam_response));
			if(data)
			{
				resp[0]->resp=strdup(data);
				resp[0]->resp_retcode=PAM_SUCCESS;
			}
			else
			{
				resp[0]->resp=NULL;
				resp[0]->resp_retcode=PAM_SUCCESS;
			}
		}
		break;
		case PAM_TEXT_INFO:
			ldebug("info  %s\n",msg[0]->msg);
		break;
		case PAM_ERROR_MSG:
			ldebug("error %s\n",msg[0]->msg);
		break;
		default:
			ldebug("bad msg_style %d\n",msg[0]->msg_style);
			return PAM_CONV_ERR;
	}
	
	return PAM_SUCCESS;
}

void Pam::delay_(int status, int delay, void *appdata_ptr)
{
	((Pam*)appdata_ptr)->delay(status,delay);
}

void Pam::delay(int status, int delay)
{
	if(status)
		ldebug("delay %d %d\n",status,delay);
}
#endif
