// Copyright 2022 Thierry LARMOIRE
// SPDX short identifier: MIT
#ifndef   __config_h__
#define   __config_h__

#include "nothread/var.h"
#include "httpServer.h"
#include "httpClient.h"

struct Config : public Vars
{
	Nothread *nothread;
	HttpServer *httpServer;
	BufferMallocedCString filename;
	int save_on_flash;
	
	int init(Nothread *nothread,HttpServer *httpServer,const char*filename);
	int exit();
	int load(const char*filename=NULL);
	int save(const char*filename=NULL);
	time_t touch(const char*filename=NULL);
	
	typedef HttpServer::Client Client;
	URL_DECL(values);
	URL_DECL(types);
	URL_DECL(events);
};

#endif /* __config_h__ */
