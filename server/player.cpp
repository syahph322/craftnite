// Copyright 2022 Thierry LARMOIRE
// SPDX short identifier: MIT
#include "player.h"

#include "nothread/ntp.h"
#include "unicode_utf8.h"
#include "server.h"

#define GET_PUT(type,type2) \
Player::Buffer& Player::Buffer::get ## type(type *v) \
{ \
	if(len<sizeof(type)) \
	{ \
		ldebug("\n"); \
		return *this; \
	} \
	memcpy(v,ptr,sizeof(type)); \
	ptr+=sizeof(type); \
	len-=sizeof(type); \
	return *this; \
} \
 \
Player::Buffer& Player::Buffer::put ## type(type v) \
{ \
	if(len<sizeof(type)) \
	{ \
		ldebug("\n"); \
		return *this; \
	} \
	memcpy(ptr,&v,sizeof(type)); \
	ptr+=sizeof(type); \
	len-=sizeof(type); \
	return *this; \
} \
 \
inline Player::Buffer& Player::Buffer::get ## type(type2 *v) \
{ \
	type w; \
	get ## type(&w); \
	*v=w; \
	return *this; \
} \
 \
inline Player::Buffer& Player::Buffer::put ## type(type2 v) \
{ \
	return put ## type((type)v); \
} \
/**/

GET_PUT(U8,int)
GET_PUT(U16,int)
GET_PUT(U32,int)
GET_PUT(F32,double)

#undef GET_PUT

Player::Buffer& Player::Buffer::putUnicode(const char *s)
{
	while(*s)
	{
		putU16(*s++);
	}
	return *this;
}


#define CMD(n) \
void Player::n ## _(Player *that, Buffer *data) \
{ \
	that->n(data); \
} \
 \
void Player::n(Buffer *data) \
/**/

Player::Cmd * Player::cmds[]={0};

Player::Player()
{
}

Player::~Player()
{
}

int Player::init(Server *server, HttpServer::Client *client, int id)
{
	ldebug("\n");
	this->server=server;
	this->id=id;
	WebSocket::init(client);
	if(0==Player::cmds[0])
	{
		#define CMD_C_S(i,n) Player::cmds[i]=on ## n ## _;
		#define CMD_S_C(i,n)
		CMDS
		#undef CMD_C_S
		#undef CMD_S_C
	}
	life=100;
	score=0;
	modifying=0;
	return 0;
}

int Player::exit()
{
	WebSocket::exit();
	remove();
	delete this;
	ldebug("\n");
	return 0;
}

void Player::graded(bool up)
{
	if(up)
	{
		ldebug("upgraded\n");
	}
	else
	{
		ldebug("downgraded\n");
		exit();
	}
}

void Player::ondata(::Buffer buffer)
{
	Buffer *data=(Buffer *)&buffer;
	if(!data->len)
	{
		ldebug("empty\n");
		return;
	}
	U8 cmdId;
	data->getU8(&cmdId);
	if(Cmd* cmd=cmdId<cmdMax ? cmds[cmdId]:0)
	{
		cmd(this,data);
	}
	else
	{
		ldebug("%5ld %d\n",data->len,cmdId);
		data->slice(0,16).dump(stderr);
	}
}

void Player::queue(V3 v)
{
	if(fifo.full())
	{
		ldebug("full\n");
		return;
	}
	fifo.push(World::v_i(v));
	modify();
}

void Player::queueAll()
{
	V3 p;
	
	if(server->chunks)
	for(p.x=0;p.x<World::side;p.x++)
	if(server->chunks[p.x])
	for(p.y=0;p.y<World::side;p.y++)
	if(server->chunks[p.x][p.y])
	for(p.z=0;p.z<World::side;p.z++)
	if(server->chunks[p.x][p.y][p.z])
	{
		queue(p);
	}
}

void Player::modify()
{
	if(modifying>0 || fifo.empty())
		return;
	modifying++;
	sendChunk(sendChunkWorker,World::i_v(fifo.pull()));
}


void Player::queueAround()
{
	V3 u(1,1,1);
	V3 cpos=position/(resolution*Chunk::side);
	for(int l=0;l<10;l++)
	{
		V3::Border b(u*(l*2));
		V3 d;
		int count=0;
		int queued=0;
		while(b.get(&d))
		{
			V3 p=d-u*l;
			//ldebug("%d %d " V3_FMT("%d") "\n",l,count,V3_ARG(p));
			count++;
			p=p+cpos;
			if(p.i<0 || p.i>=World::side) continue;
			if(p.j<0 || p.j>=World::side) continue;
			if(p.k<0 || p.k>=World::side) continue;
			if(!server->chunkAt(p)) continue;
			queue(p);
			queued++;
		}
		ldebug("%d => %d %d\n",l,count,queued);
	}
}

CMD(onNewPlayer)
{
	data->getU8(&skin);
	int len=unicode_to_utf8((U16*)data->ptr, data->len/sizeof(U16), NULL, 0);
	BUFFER_ALLOCED(name_server,len)
	//data->dump(stderr);
	unicode_to_utf8((U16*)data->ptr, data->len/sizeof(U16), (char*)name_server.ptr, name_server.len);
	//::Buffer(name,len).dump(stderr);
	Buffer n,s;
	name_server.split("§",2,&n,&s);
	name=n;
	
	ldebug("player id %d skin %d %s\n",id,skin,name.cString());
	
	foreach(Player,p,server->players)
	{
		//binary scan $data c1c1i1a* id skin score unicode
		// send to this other
		{
			int len=utf8_to_unicode((char*)p->name.ptr, p->name.len, NULL, 0);
			BUFFER_ALLOCED(b,7+len*sizeof(U16));
			Buffer x=b;
			x.putU8(sendNewPlayer);
			x.putU8(p->id);
			x.putU8(p->skin);
			x.putU32(p->score);
			utf8_to_unicode((char*)p->name.ptr, p->name.len, (U16*)x.ptr, len);
			send(b);
		}
		
		if(p->id!=id)
		{
			int len=utf8_to_unicode((char*)p->item.ptr, p->item.len, NULL, 0);
			BUFFER_ALLOCED(b,2+len*sizeof(U16));
			Buffer x=b;
			x.putU8(sendPlayerItem);
			x.putU8(id);
			utf8_to_unicode((char*)p->item.ptr, p->item.len, (U16*)x.ptr, len);
			send(b);
		}
		// send to other this
		if(p->id!=id)
		{
			int len=utf8_to_unicode((char*)name.ptr, name.len, NULL, 0);
			BUFFER_ALLOCED(b,7+len*sizeof(U16));
			Buffer x=b;
			x.putU8(sendNewPlayer);
			x.putU8(id);
			x.putU8(skin);
			x.putU32(score);
			utf8_to_unicode((char*)name.ptr, name.len, (U16*)x.ptr, len);
			p->send(b);
		}
	}
	
	{
		struct Item
		{
			U8 count;
			const char *name;
		}items[]={
			{  1,"pickaxe" },
			{ 64,"pistol" }
		};
		int len=1;
		for(Item *i=items,*e=i+N_ELEMENT(items);i<e;i++)
		{
			len+=1+sizeof(U16)*utf8_to_unicode(i->name,strlen(i->name),NULL,0)+1;
		}
		BUFFER_ALLOCED(b,len);
		Buffer x=b;
		x.putU8(sendItems);
		for(Item *i=items,*e=i+N_ELEMENT(items);i<e;i++)
		{
			U8 *l=x.ptr;
			x.putU8(0);
			*l=utf8_to_unicode(i->name,strlen(i->name),(U16*)x.ptr,x.len/sizeof(U16));
			x.ptr+=*l*2;
			x.len-=*l*2;
			x.putU8(i->count);
		}
		send(b);
	}

	//for(int k=16;k<19;k++)
	//for(int j= 3;j< 4;j++)
	//for(int i=24;i<27;i++)
	//	sendChunk(::sendChunk,i,j,k);
	queueAround();
	//queue(server);
}

CMD(onStart)
{
	U8 version;
	data->getU8(&version);
	ldebug("version %d\n",version);
	{
		BUFFER_ALLOCED(b,2);
		Buffer x=b;
		x.putU8(sendPlayerId);
		x.putU8(id);
		send(b);
	}
	{
		BUFFER_ALLOCED(b,9);
		Buffer x=b;
		x.putU8(sendOceanConfig);
		x.putF32(317.);
		x.putF32(100.);
		send(b);
	}
	{
		BUFFER_ALLOCED(b,2);
		Buffer x=b;
		x.putU8(sendCreative);
		x.putU8(0);
		send(b);
	}
	{
		//sendConfig1 000ad7233c0ad7233d000020418fc2753e000020418fc2753e3333b33e0ad7233ccdcccc3dcdcccc3dcdcccc3dcdcccc3d9a99993e0ad7233d000020419a99993e000020419a99993e0ad7233f0ad7233d000020413108ac3d000020413108ac3dcdcc0c3f
		BUFFER_ALLOCED(b,102);
		Buffer x=b;
		x
		.putU8(sendConfig1)
		.putU8 (0) //,flying,,
		.putF32(0.01) //,defaultGravity,,
		.putF32(0.04) //,waterGravity,0.04,
		.putF32(10.) //,waterAddAY,10,
		.putF32(0.24) //,waterMaxAX,0.24,
		.putF32(10.) //,waterMaxAY,10,
		.putF32(0.24) //,waterMaxAZ,0.24,
		.putF32(0.35) //,waterMaxVY,0.35,
		.putF32(0.01) //,airGravity,0.01,
		.putF32(0.1) //,airAddAY,0.1,
		.putF32(0.1) //,airMaxAX,0.1,
		.putF32(0.1) //,airMaxAY,0.1,
		.putF32(0.1) //,airMaxAZ,0.1,
		.putF32(0.3) //,airMaxVY,0.3,
		.putF32(0.04) //,flyGravity,0.04,
		.putF32(10.) //,flyAddAY,10,
		.putF32(0.3) //,flyMaxAX,0.3,
		.putF32(10.) //,flyMaxAY,10,
		.putF32(0.3) //,flyMaxAZ,0.3,
		.putF32(0.64) //,flyMaxVY,0.64,
		.putF32(0.04) //,playerExitWaterGravity,0.04,
		.putF32(10.) //,playerExitWaterAddAY,10,
		.putF32(0.084) //,playerExitWaterMaxAX,0.084,
		.putF32(10.) //,playerExitWaterMaxAY,10,
		.putF32(0.084) //,playerExitWaterMaxAZ,0.084,
		.putF32(0.55) //,playerExitWaterMaxVY,0.55
		;
		if(x.len)
			ldebug("sendConfig1 %ld\n",x.len);
		send(b);
	}
	{
		//sendConfig2 00002eb8ff2eb8ff002eb8ff0000a04100803b454150ce00007ac400004844000082436300680069006c007600610072007900
		const char *worldImage="chilvary";
		int l=utf8_to_unicode(worldImage, strlen(worldImage),NULL,0);
		BUFFER_ALLOCED(b,36+2*l);
		Buffer x=b;
/*
		this.skyVisible = t.getUint8(1),
		this.skyBackground = t.getUint8(2),
		this.skyTopColor = new THREE.Color("rgb(" + t.getUint8(3, !0) + "," + t.getUint8(4, !0) + "," + t.getUint8(5, !0) + ")"),
		this.skyBottomColor = new THREE.Color("rgb(" + t.getUint8(6, !0) + "," + t.getUint8(7, !0) + "," + t.getUint8(8, !0) + ")"),
		this.environmentFog = t.getUint8(9),
		this.environmentFogColor = new THREE.Color("rgb(" + t.getUint8(10, !0) + "," + t.getUint8(11, !0) + "," + t.getUint8(12, !0) +")"),
		this.environmentFogNear = t.getFloat32(13, !0),
		this.environmentFogFar = t.getFloat32(17, !0),
		this.oceanFog = new THREE.Color("rgb(" + t.getUint8(21, !0) + "," + t.getUint8(22, !0) + "," + t.getUint8(23, !0) + ")"),
		this.oceanFogConfig1 = t.getFloat32(24, !0),
		this.oceanFogConfig2 = t.getFloat32(28, !0),
		this.environmentOceanFloorHeight = t.getFloat32(32, !0);
		for (var e = new Uint16Array((t.byteLength - 36) / 2), i = 0, o = 36; o < t.byteLength; o += 2) e[i] = t.getUint16(o, !0), i++;
		this.worldImage = String.fromCharCode.apply(null, e)
*/
		x
		.putU8(sendConfig2)
		// {
		//   "skyVisible": 0,
		.putU8(0)
		//   "skyBackground": 0,
		.putU8(0)
		//   "skyTopColor": {
		//     "r": 0.1803921568627451,
		//     "g": 0.7215686274509804,
		//     "b": 1
		//   },
		.putU8(46).putU8(184).putU8(255)
		//   "skyBottomColor": {
		//     "r": 0.1803921568627451,
		//     "g": 0.7215686274509804,
		//     "b": 1
		//   },
		.putU8(46).putU8(184).putU8(255)
		//   "environmentFog": 0,
		.putU8(0)
		//   "environmentFogColor": {
		//     "r": 0.1803921568627451,
		//     "g": 0.7215686274509804,
		//     "b": 1
		//   },
		.putU8(46).putU8(184).putU8(255)
		//   "environmentFogNear": 20,
		.putF32(20.)
		//   "environmentFogFar": 3000,
		.putF32(3000.)
		//   "oceanFog": {
		//     "r": 0.2549019607843137,
		//     "g": 0.3137254901960784,
		//     "b": 0.807843137254902
		//   },
		.putU8(65).putU8(80).putU8(206)
		//   "oceanFogConfig1": -1000,
		.putF32(-1000.)
		//   "oceanFogConfig2": 800,
		.putF32(800.)
		//   "environmentOceanFloorHeight": 260,
		.putF32(260.);
		//   "worldImage": "chilvary"
		l=utf8_to_unicode(worldImage, strlen(worldImage),(U16*)x.ptr, x.len/sizeof(U16));
		x.ptr+=l*2;
		x.len-=l*2;
		// }
		if(x.len)
			ldebug("sendConfig2 %ld\n",x.len);
		send(b);
	}

	{
		BUFFER_ALLOCED(b,1+sizeof(U32));
		Buffer x=b;
		x.putU8(sendMatchRemainingTime);
		x.putU32(server->remainingTime);
		send(b);
	}
	
	respawn();
	
	{
		BUFFER_ALLOCED(b,1);
		Buffer x=b;
		x.putU8(sendMatchStarted);
		send(b);
	}
	
	{
		BUFFER_ALLOCED(b,1);
		Buffer x=b;
		x.putU8(sendResetChunks);
		send(b);
	}
	
}

CMD(onState)
{
	float x,y,z;
	data->getF32(&x);
	data->getF32(&y);
	data->getF32(&z);
	data->getF32(&time);
	data->getF32(&rotY);
	data->getF32(&rotX);
	data->getU8(&running);
	position.x=x/5.*32;
	position.y=y/5.*32;
	position.z=z/5.*32;
	//ldebug("(%8.2f,%8.2f,%8.2f) (%5.4f,%5.4f) %10.3f %d\n",x,y,z,rotX,rotY,time,running);
	
	{
		//while { 6<=[binary scan $data "c1 f1f1f1 f1f1 f1 c1 a*" _(id) _(x) _(y) _(z) _(rotY) _(rotX) _(time) _(running) data] } {
		int len=1;
		foreach(Player,p,server->players)
		{
			if(p->id==id) continue;
			len+=26;
		}
		BUFFER_ALLOCED(b,len);
		Buffer x=b;
		x.putU8(sendState);
		
		foreach(Player,p,server->players)
		{
			if(p->id==id) continue;
			x.putU8(p->id);
			x.putF32(p->position.x*(5./32));
			x.putF32(p->position.y*(5./32));
			x.putF32(p->position.z*(5./32));
			x.putF32(p->rotY);
			x.putF32(p->rotX);
			x.putF32(p->time);
			x.putU8(p->running);
		}
		if(x.len) ldebug("\n");
		send(b);
	}
}

CMD(onChunkBuffered)
{
	static int skip=0;
	if(0==data->len)
	{
		skip++;
		if(skip<100)
			return;
	}
	if(skip)
		ldebug("%ld %d\n",data->len,skip);
	skip=0;
	U8 i,j,k,enable;
	while(data->len>=4)
	{
		data->getU8(&i);
		data->getU8(&j);
		data->getU8(&k);
		data->getU8(&enable);
		//ldebug("chunkBuffered %d (%d,%d,%d)\n",enable,i,j,k);
		modifying--;
		modify();
	}
}

void Player::sendChunk(int kind, V3 v)
{
	Chunk *c=server->chunkAt(v);
	if(!c)
	{
		ldebug("!" V3_FMT("%d") "\n", V3_ARG(v));
		return;
	}
	//c->borderStone();
	c->send(this,kind);
}

CMD(onPlayerItem)
{
	item.resize(unicode_to_utf8((U16*)data->ptr,data->len/sizeof(U16),NULL,0));
	item.terminate();
	unicode_to_utf8((U16*)data->ptr,data->len/sizeof(U16),(char*)item.ptr,item.len);
	ldebug("item %s\n",item.cString());
	foreach(Player,p,server->players)
	if(p->id!=id)
	{
		int len=utf8_to_unicode((char*)item.ptr, item.len, NULL, 0);
		BUFFER_ALLOCED(b,2+len*sizeof(U16));
		Buffer x=b;
		x.putU8(sendPlayerItem);
		x.putU8(id);
		utf8_to_unicode((char*)item.ptr, item.len, (U16*)x.ptr, len);
		p->send(b);
	}
}

CMD(onTriggerOn)
{
	ldebug("onTriggerOn\n");
}

CMD(onTriggerOff)
{
	ldebug("onTriggerOff\n");
}

CMD(onAlter)
{
	data->len=data->len/12*8; // bug in js
	while(data->len>=8)
	{
		V3 v;
		int o;
		int damage;
		data->getU8(&v.i);
		data->getU8(&v.j);
		data->getU8(&v.k);
		data->getU32(&o);
		data->getU8(&damage);
		Chunk *c=server->chunkAtOrCreate(v);
		if(damage>=c->hard[o])
		{
			ldebug("%d 0x%04x\n",c->data[o],c->data[o]);
			Material *m;
			m=&server->materials[c->data[o]];
			m=&server->materials[m->next];
			c->data[o]=m->id;
			c->hard[o]=m->hard;
		}
		else
			c->hard[o]-=damage;
		foreach(Player,p,server->players)
			p->queue(c->pos);
	}
}

CMD(onChange)
{
	while(data->len>=9)
	{
		// append fmt " c1c1c1i1s1"
		V3 v;
		int o;
		int content;
		data->getU8(&v.i);
		data->getU8(&v.j);
		data->getU8(&v.k);
		data->getU32(&o);
		data->getU16(&content);
		ldebug(V3_FMT("%d") "%d %d\n", V3_ARG(v), o, content);
		Chunk *c=server->chunkAtOrCreate(v);
		c->data[o]=content;
		foreach(Player,p,server->players)
			p->queue(c->pos);
	}
}

CMD(onChat)
{
	char text[unicode_to_utf8((U16*)data->ptr,data->len/sizeof(U16),NULL,0)];
	unicode_to_utf8((U16*)data->ptr,data->len/sizeof(U16),text,sizeof(text));
	ldebug("%s: %.*s\n",name.cString(),(int)sizeof(text),text);
	foreach(Player,p,server->players)
	if(p->id!=id)
	{
		int len=utf8_to_unicode(text, sizeof(text), NULL, 0);
		BUFFER_ALLOCED(b,2+len*sizeof(U16));
		Buffer x=b;
		x.putU8(sendChat);
		x.putU8(id);
		utf8_to_unicode(text, sizeof(text), (U16*)x.ptr, len);
		p->send(b);
	}
}

CMD(onShot)
{
	//data $fd sendShot "cu1 f1f1f1 f1f1f1" $weaponId $orgX $orgY $orgZ $dirX $dirY $dirZ
	double orgX,orgY,orgZ,dirX,dirY,dirZ;
	int weaponId;
	data->getU8(&weaponId);
	data->getF32(&orgX);
	data->getF32(&orgY);
	data->getF32(&orgZ);
	data->getF32(&dirX);
	data->getF32(&dirY);
	data->getF32(&dirZ);
	BUFFER_ALLOCED(b,3+6*sizeof(F32));
	Buffer x=b;
	x.putU8(sendShot);
	x.putU8(id);
	x.putU8(weaponId);
	x.putF32(orgX);
	x.putF32(orgY);
	x.putF32(orgZ);
	x.putF32(dirX);
	x.putF32(dirY);
	x.putF32(dirZ);
	foreach(Player,p,server->players)
	if(p->id!=id)
	{
		p->send(b);
	}
}

CMD(onTouch)
{
// tiers
// recv 26 recvShot
// recv 26 recvShot
// recv 85 recvPlayerScore
// recv 18 recvDeath2

// killed
// recv 26 recvShot
// hp decrease 36
// recv 72 recvHealth
// recv 26 recvShot
// hp decrease 0
// recv 72 recvHealth
// recv 85 recvPlayerScore
// resetItems
// recv 19 recvYouDied
// recv 69 recvResetChunks
// recv 65 recvFirstChunkWorker

// killer
// send 53 sendTriggerOn
// send 25 sendShot
// send 16 sendTouch
// send 55 sendTriggerOff
// send 53 sendTriggerOn
// send 25 sendShot
// send 16 sendTouch
// send 55 sendTriggerOff
// send 5 sendPlayerItem
// send 53 sendTriggerOn
// send 25 sendShot
// send 16 sendTouch
// recv 85 recvPlayerScore
// recv 18 recvDeath2
// send 55 sendTriggerOff

	// data $fd sendTouch "c1 c1" $id $force
	int id;
	int force;
	data->getU8(&id);
	data->getU8(&force);
	
	Player *killer=this;
	Player *killed=NULL;
	foreach(Player,p,server->players)
	if(p->id==id)
		killed=p;
	
	if(!killed)
		return;
	
	if(force<killed->life)
	{
		killed->life-=force;
		{
			// this.HP = t.getUint8(1)
			BUFFER_ALLOCED(b,2);
			Buffer x=b;
			x.putU8(sendHealth);
			x.putU8(killed->life);
			killed->send(b);
		}
	}
	else
	{
		killed->life=0;
		
		score++;
		foreach(Player,p,server->players)
		{
			{
				BUFFER_ALLOCED(b,3);
				Buffer x=b;
				x.putU8(sendPlayerScore);
				x.putU8(killer->id);
				x.putU8(score);
				p->send(b);
			}
			if(p->id!=killed->id)
			{
				// this.id = t.getUint8(1), this.killer = t.getUint8(2)
				BUFFER_ALLOCED(b,3);
				Buffer x=b;
				x.putU8(sendDeath2);
				x.putU8(killed->id);
				x.putU8(killer->id);
				p->send(b);
			}
			else
			{
				{
					BUFFER_ALLOCED(b,2);
					Buffer x=b;
					x.putU8(sendYouDied);
					x.putU8(killer->id);
					killed->send(b);
				}
				{
					BUFFER_ALLOCED(b,1);
					Buffer x=b;
					x.putU8(sendResetChunks);
					killed->send(b);
				}
			}
		}
	}
}

CMD(onRespawn)
{
	respawn();
}

void Player::respawn()
{
	const V3 starts[]= {
		V3(4297, 422,2748)*resolution/js_resolution, // near sea between drakar & church
		V3(4141, 867,2582)*resolution/js_resolution, // church
		V3(3927, 740,2298)*resolution/js_resolution, // near mountain
		V3(4065, 622,2778)*resolution/js_resolution, // near windmill
		V3(3750, 687,2720)*resolution/js_resolution, // below bridge
		V3(3597, 687,2672)*resolution/js_resolution, // front of plane between castle & forest
		V3(3505, 757,3067)*resolution/js_resolution, // castle bottom
		V3(3403,1017,2988)*resolution/js_resolution, // castle top
	};
	
	NtpNow now;
	position=starts[now.integer()%N_ELEMENT(starts)];
	
	ldebug("%s @" V3_FMT("%5d") "\n",name.cString(),V3_ARG(position));
	BUFFER_ALLOCED(b,1+3*sizeof(F32));
	Buffer x=b;
	x.putU8(sendRespawn);
	x.putF32(position.x*(1.*js_resolution/resolution));
	x.putF32(position.y*(1.*js_resolution/resolution));
	x.putF32(position.z*(1.*js_resolution/resolution));
	send(b);
}


#undef DIS_C_S
#undef DIS_S_C

#define DIS_C_S(i,n) CMD(on ## n) { ldebug("\n"); }
#define DIS_S_C(i,n)
#define CMD_C_S(i,n)
#define CMD_S_C(i,n)
CMDS
#undef DIS_C_S
#undef DIS_S_C
#undef CMD_C_S
#undef CMD_S_C
