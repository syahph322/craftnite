// Copyright 2022 Thierry LARMOIRE
// SPDX short identifier: MIT
#include "material.h"
#include <stdlib.h>

#define kinds \
	kind(512,50,768) \
	kind(768,50,0) \
	kind(1024,50,28160) \
	kind(28160,200,0) \
/**/

int Materials::init()
{
	materials=(Material*)calloc(sizeof(Material),len);
	for(int i=0;i<len;i++)
	{
		materials[i].id=i;
		materials[i].hard=50;
		materials[i].next=0;
	}
	#define kind(id,h,n) materials[id].hard=h; materials[id].next=n;
	kinds
	#undef kind
	return 0;
}

int Materials::exit()
{
	free(materials);
	return 0;
}
