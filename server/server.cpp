// Copyright 2022 Thierry LARMOIRE
// SPDX short identifier: MIT
#include "server.h"
#include "player.h"
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>

int Server::init()
{
	Nothread::init();
	Config::init(this,NULL,"server.cfg");
	HttpServer::init(this,this,8081,"http");
	Periodic::init(this,1000*1000,0,tick_,this);
	World::init();
	Materials::init();
	URL_INIT(this,this,file,NULL,NULL);
	URL_INIT(this,this,ws,"GET","/s");
	URL_INIT(this,this,save_restore,"GET","/save");
	URL_INIT(this,this,save_restore,"GET","/restore");
	
	V3 min(2960, 260,2175);
	V3 max(4455,1185,3585);
	World::load("www/saved/world",min/5,(max-min)/5);
	
	Periodic::start();
	return 0;
}

NOTHREAD_TIMEOUT_BODY(Server,tick)
{
	World::eachSeconds();
}

int Server::exit()
{
	Materials::exit();
	World::exit();
	HttpServer::exit();
	Config::exit();
	Nothread::exit();
	return 0;
}

URL_BODY(Server,file)
{
	//ldebug("%.*s\n",(int)client->request.path.len,client->request.path.ptr);
	return client->handleFile(Buffer("www"));
}

URL_BODY(Server,ws)
{
	Player *player=new Player();
	int id=1;
	foreach(Player,p,players)
		if(p->id>=id)
			id=p->id+1;
	player->init(this,client,id);
	players.add(player);
	return 0;
}

URL_BODY(Server,save_restore)
{
	BufferCString name;
	for(HttpServer::Client::Request::Arg *a=client->request.args.array,*e=a+client->request.args.count;a<e;a++)
	{
		if(a->name=="name")
			name=a->value;
	}
	if(name.isNull() || Buffer::notFound!=name.search('.') || Buffer::notFound!=name.search('/'))
		return client->handleError(400,"bad args");
	
	BufferCString path("www/saved");
	char filename_[path.len+1+name.len+1];
	BufferCString filename(filename_,snprintf(filename_,sizeof(filename_),"%s/%s",path.cString(),name.cString()));
	
	if(!client->request.data.len)
	{
		struct stat filestat;
		memset(&filestat,0,sizeof(filestat));
		int status=::stat(filename.cString(),&filestat);
		if(status!=0)
			return client->handleError(404,"Not found");
		return client->handleFile(filename,&filestat);
	}
	else
	{
		int fd;
		fd=open(filename.cString(),O_WRONLY|O_CREAT|O_TRUNC,0664);
		if(-1!=fd)
		{
			write(fd,client->request.data.ptr,client->request.data.len);
			close(fd);
		}
	}
	return 0;
}
