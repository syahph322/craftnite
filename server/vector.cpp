// Copyright 2022 Thierry LARMOIRE
// SPDX short identifier: MIT
#include "vector.h"
#include "nothread/type.h"
#include "nothread/debug.h"

static const V3::Border::Face faces0[]=
{
	{ V3(0,0,0),V3(0,0,0),V3(1,1,1),V3(0,0,0) },
};

static const V3::Border::Face faces[]=
{
	{ V3(0,1,0),V3(0,0,0),V3(1,0,0),V3(0,1,1) },
	{ V3(0,0,1),V3(0,0,0),V3(0,1,0),V3(1,0,1) },
	{ V3(1,0,0),V3(0,0,0),V3(0,0,1),V3(1,1,0) },
	{ V3(0,0,1),V3(1,0,0),V3(1,0,0),V3(0,1,1) },
	{ V3(1,0,0),V3(0,1,0),V3(0,1,0),V3(1,0,1) },
	{ V3(0,1,0),V3(0,0,1),V3(0,0,1),V3(1,1,0) },
	{ V3(0,0,0),V3(0,0,0),V3(1,1,1),V3(0,0,0) },
	{ V3(0,0,0),V3(1,1,1),V3(1,1,1),V3(0,0,0) },
};

V3::Border::Border(V3 size)
{
	this->size=size;
	f=e=0;
	d=V3(0,0,0);
}

bool V3::Border::get(V3 *p)
{
	if(!f)
	{
		if(size.x<0||size.y<0||size.z<0)
			return false;
		if(size.x==0||size.y==0||size.z==0)
		{
			f=e=faces0;
			e+=N_ELEMENT(faces0);
		}
		else
		{
			f=e=faces;
			e+=N_ELEMENT(faces);
		}
		v.x=0;
	}

	if(f>=e)
		return false;

	if(!v.x)
	{
		o=f->o1+f->ol*size;
		v=f->v1+f->vl*size;
		//ldebug("o=" V3_FMT("%d") " v=" V3_FMT("%d") "\n",V3_ARG(o),V3_ARG(v));
	}
	*p=o+d;
	d.x++;
	if(d.x>=v.x)
	{
		d.x=0;
		d.y++;
		if(d.y>=v.y)
		{
			d.y=0;
			d.z++;
			if(d.z>=v.z)
			{
				d.z=0;
				f++;
				v.x=0;
			}
		}
	}
	return true;
}
