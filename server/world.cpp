// Copyright 2022 Thierry LARMOIRE
// SPDX short identifier: MIT
#include "world.h"
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>

int World::init()
{
	chunks=NULL;
	remainingTime=24*60*60;
	return 0;
}

int World::exit()
{
	if(chunks)
	{
		for(size_t i=0;i<side;i++)
		{
			Chunk ***ppp=chunks[i];
			if(!ppp) continue;
			for(size_t j=0;j<side;j++)
			{
				Chunk **pp=ppp[j];
				if(!pp) continue;
				for(size_t k=0;k<side;k++)
				{
					Chunk *p=pp[k];
					if(!p) continue;
					delete p;
				}
				free(pp);
			}
			free(ppp);
		}
		free(chunks);
		chunks=NULL;
	}
	return 0;
}

Chunk *World::chunkAtOrCreate(V3 pos)
{
	pos.x%=side;
	pos.y%=side;
	pos.z%=side;
	
	Chunk *c;
	if(!   chunks)                       chunks                     =(Chunk****)calloc(sizeof(Chunk***),side);
	if(!   chunks[pos.x])                chunks[pos.x]              =(Chunk*** )calloc(sizeof(Chunk** ),side);
	if(!   chunks[pos.x][pos.y])         chunks[pos.x][pos.y]       =(Chunk**  )calloc(sizeof(Chunk*  ),side);
	if(!(c=chunks[pos.x][pos.y][pos.z])) chunks[pos.x][pos.y][pos.z]=c=new Chunk(pos);
	return c;
}

Chunk *World::chunkAt(V3 pos)
{
	return chunks && chunks[pos.x] && chunks[pos.x][pos.y] ? chunks[pos.x][pos.y][pos.z] : NULL;
}

int World::load(const char *file,V3 wb,V3 ws)
{
	int fd=open(file,O_RDONLY);
	if(fd<0)
	{
		ldebug("!open %s\n",file);
		return -1;
	}
	off_t file_size=lseek(fd,0,SEEK_END);
	lseek(fd,0,SEEK_SET);
	U16 *data=(U16*)malloc(file_size);
	read(fd,data,file_size);
	close(fd);
	
	V3 u(1,1,1);
	V3 z(0,0,0);
	V3 we=wb+ws;
	V3 cb=wb/Chunk::side;
	V3 ce=(we-u)/Chunk::side+u;
	V3 cs=ce-cb;
	V3 p;
	ldebug(" beg" V3_FMT("%d") " size" V3_FMT("%d")  " end" V3_FMT("%d") " cbeg" V3_FMT("%d") " csize" V3_FMT("%d") " cend" V3_FMT("%d") "\n", V3_ARG(wb), V3_ARG(ws), V3_ARG(we), V3_ARG(cb), V3_ARG(cs), V3_ARG(ce));
	int count=0;
	for(p.k=cb.k;p.k<ce.k;p.k++)
	for(p.j=cb.j;p.j<ce.j;p.j++)
	for(p.i=cb.i;p.i<ce.i;p.i++)
	{
		Chunk *c=chunkAtOrCreate(p);
		V3 border=p*Chunk::side;
		V3 b=wb-border;
		V3 e=we-border;
		//ldebug("border" V3_FMT("%d") " min" V3_FMT("%d") " max" V3_FMT("%d") "\n", V3_ARG(border), V3_ARG(b), V3_ARG(e));
		b.noless(z);
		e.nomore(u*Chunk::side);
		V3 s,d;
		//ldebug("border" V3_FMT("%d") " min" V3_FMT("%d") " max" V3_FMT("%d") "\n", V3_ARG(border), V3_ARG(b), V3_ARG(e));
		for(d.k=b.k;d.k<e.k;d.k++)
		for(d.j=b.j;d.j<e.j;d.j++)
		for(d.i=b.i;d.i<e.i;d.i++)
		{
			s=border+d-wb;
			c->data[d.x+Chunk::side*(d.y+Chunk::side*d.z)]=data[s.z+ws.z*(s.x+ws.x*s.y)];
		}
		if(c->empty())
		{
			delete(c);
			chunks[p.x][p.y][p.z]=0;
		}
		else
			count++;
	}
	
	free(data);
	ldebug("world is %d chunks\n",count);
	return 0;
}

void World::eachSeconds()
{
	BUFFER_ALLOCED(b,1+sizeof(U32));
	Player::Buffer x=b;
	x.putU8(sendMatchRemainingTime);
	x.putU32(remainingTime);
	foreach(Player,p,players)
	{
		p->send(b);
	}
	remainingTime--;
}