// Copyright 2022 Thierry LARMOIRE
// SPDX short identifier: MIT
#ifndef __server_h__
#define __server_h__

#include "nothread/nothread.h"
#include "nothread/periodic.h"
#include "http/httpServer.h"
#include "http/config.h"
#include "world.h"
#include "material.h"

struct Server : public Nothread, HttpServer, Config, World, Materials, Periodic
{
	struct Client : public HttpServer::Client
	{
	};
	
	int init();
	int exit();
	
	NOTHREAD_TIMEOUT_DECL(tick);
	URL_DECL(file);
	URL_DECL(ws);
	URL_DECL(save_restore);
};

#endif /* __server_h__ */
