// Copyright 2022 Thierry LARMOIRE
// SPDX short identifier: MIT

#include "server.h"

#include <stdlib.h>

int main(int argc, char **argv)
{
	Server server[1];
	server->init();
	server->doEventLoop();
	server->exit();
}
